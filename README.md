# cs280-mini-proj

<!-- TOC -->

- [cs280-mini-proj](#cs280-mini-proj)#
- [Install libraries](#install-libraries)
- [Folder Structure](#folder-structure)

<!-- /TOC -->

# Install libraries
At the root folder
```
pip install -r requirements.txt
```

# Folder Structure

- /data - for datasets/data/etc
    - /raw - original, immutable data dump
    - /interim - data to be used from raw
    - /parsed - parsed data
- /models - models to be used organized by experiment name
- /notes - for scratch/notes
- /reports - for figures etc to be used for reporting
- /results - for storing results by experiment name
- /analysis - for storing results of analysis done
- /src - for source codes
	- /analyze-dataset - for scripts analyzing  the dataset
	- /ANN - for the scripts for training and testing ANN
		- /grid_search_3_tests - for grid search tests for ANN 3 outputs
		- /grid_search_5_tests_v2 - for grid search tests for ANN 3 outputs w/o day and SVM outputs w/o day
    - /dataset-parsing - for initial parsing of the dataset
    - /dataset-processing - for processing the dataset
    - /SVM - scripts for training and testing the SVM


# Prerequisites for running the experiments
The /data folder needs to have the same files as can be found with https://gitlab.com/agathauy/cs280-mini-proj/
After this, the ff. scripts from /src/dataset-processing needs to be ran:
```
python master_standardize_3.py
python master_standardize.py
```

The other scripts would now work properly after this.
