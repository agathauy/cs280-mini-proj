##### Flow Type: entry, Num Set: 0 ####################################

#### Searching optimal hyperparameters for: accuracy
Best parameters set found on development set:
{'hidden_layer_sizes': 3}

Grid scores on development set:
0.352572896296, 0.014300434904 for {'hidden_layer_sizes': 3}


Full performance report:

              precision    recall  f1-score   support

         0.0       0.38      0.33      0.35     10284
         1.0       0.24      0.49      0.32     10270
         2.0       0.30      0.14      0.19     10271
         3.0       0.20      0.11      0.14     10284
         4.0       0.62      0.65      0.63     10283

   micro avg       0.34      0.34      0.34     51392
   macro avg       0.35      0.34      0.33     51392
weighted avg       0.35      0.34      0.33     51392

Elapsed Seconds: 214.329040051
Total Time Elapsed: 00:03:34
