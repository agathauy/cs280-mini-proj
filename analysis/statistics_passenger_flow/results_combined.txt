## Entry
Mean: 1559.4074051
std: 1371.39766849
var: 1880731.56514
min: 0.0
max: 11039.0
0 percentile: 0.0
20 percentile: 458.0
30 percentile: 673.0
40 percentile: 963.0
60 percentile: 1549.0
80 percentile: 2412.0
100 percentile: 11039.0
5 bin unweighted histogram: [    0.   2207.8  4415.6  6623.4  8831.2 11039. ]
## Exit
Mean: 1556.76405872
std: 1216.9846044
var: 1481051.52735
min: 0.0
max: 8537.0
0 percentile: 0.0
20 percentile: 515.0
30 percentile: 727.0
40 percentile: 997.0
60 percentile: 1597.0
80 percentile: 2496.0
100 percentile: 8537.0
5 bin unweighted histogram: [   0.  1707.4 3414.8 5122.2 6829.6 8537. ]


