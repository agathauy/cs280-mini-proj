# Num of nodes: 14, 14 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:05:54, 354.742916107
Training set score: 0.892292
Training set loss: 0.299439
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_entry_4_h1_14_h2_14/ann_0_14_14.pkl

Elapsed Seconds: 354.41759181
Total Time Elapsed: 00:05:54
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.91      0.83      0.87     15427
         1.0       0.87      0.91      0.89     25682
         2.0       0.88      0.88      0.88     10283

   micro avg       0.88      0.88      0.88     51392
   macro avg       0.89      0.88      0.88     51392
weighted avg       0.88      0.88      0.88     51392


Accuracy: 0.882705479452
## 1-kth fold --------------------------------
Training Time Elapsed: 00:11:25, 685.04896307
Training set score: 0.891242
Training set loss: 0.302259
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_entry_4_h1_14_h2_14/ann_1_14_14.pkl

Elapsed Seconds: 330.035109997
Total Time Elapsed: 00:05:30
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.91      0.89      0.90     15427
         1.0       0.89      0.92      0.90     25682
         2.0       0.90      0.87      0.89     10283

   micro avg       0.90      0.90      0.90     51392
   macro avg       0.90      0.89      0.90     51392
weighted avg       0.90      0.90      0.90     51392


Accuracy: 0.9005876401
## 2-kth fold --------------------------------
Training Time Elapsed: 00:16:31, 991.842298031
Training set score: 0.906507
Training set loss: 0.274134
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_entry_4_h1_14_h2_14/ann_2_14_14.pkl

Elapsed Seconds: 306.577866077
Total Time Elapsed: 00:05:06
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.92      0.87      0.89     15427
         1.0       0.89      0.92      0.91     25682
         2.0       0.88      0.88      0.88     10283

   micro avg       0.90      0.90      0.90     51392
   macro avg       0.90      0.89      0.89     51392
weighted avg       0.90      0.90      0.90     51392


Accuracy: 0.89749377335
## 3-kth fold --------------------------------
Training Time Elapsed: 00:21:32, 1292.04983616
Training set score: 0.895401
Training set loss: 0.286569
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_entry_4_h1_14_h2_14/ann_3_14_14.pkl

Elapsed Seconds: 299.928833008
Total Time Elapsed: 00:04:59
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.86      0.88     15426
         1.0       0.84      0.91      0.87     25682
         2.0       0.90      0.75      0.82     10283

   micro avg       0.86      0.86      0.86     51391
   macro avg       0.88      0.84      0.86     51391
weighted avg       0.87      0.86      0.86     51391


Accuracy: 0.864859605768
## 4-kth fold --------------------------------
Training Time Elapsed: 00:26:32, 1592.44219899
Training set score: 0.899410
Training set loss: 0.287114
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_entry_4_h1_14_h2_14/ann_4_14_14.pkl

Elapsed Seconds: 299.919904947
Total Time Elapsed: 00:04:59
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.91      0.87      0.89     15426
         1.0       0.88      0.90      0.89     25682
         2.0       0.86      0.89      0.88     10283

   micro avg       0.89      0.89      0.89     51391
   macro avg       0.89      0.89      0.89     51391
weighted avg       0.89      0.89      0.89     51391


Accuracy: 0.888093245899
Average Training Accuracy for: (14, 14)
0.896970515736

Average Test Accuracy for: (14, 14)
0.886747948914

Elapsed Seconds: 1593.11854696
Total Time Elapsed: 00:26:33
