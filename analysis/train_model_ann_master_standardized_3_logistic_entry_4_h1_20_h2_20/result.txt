# Num of nodes: 20, 20 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:05:55, 355.655508995
Training set score: 0.912145
Training set loss: 0.247514
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_entry_4_h1_20_h2_20/ann_0_20_20.pkl

Elapsed Seconds: 355.518590927
Total Time Elapsed: 00:05:55
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.93      0.88      0.91     15427
         1.0       0.90      0.93      0.91     25682
         2.0       0.90      0.88      0.89     10283

   micro avg       0.91      0.91      0.91     51392
   macro avg       0.91      0.90      0.90     51392
weighted avg       0.91      0.91      0.91     51392


Accuracy: 0.905919209215
## 1-kth fold --------------------------------
Training Time Elapsed: 00:11:30, 690.684890032
Training set score: 0.911230
Training set loss: 0.254085
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_entry_4_h1_20_h2_20/ann_1_20_20.pkl

Elapsed Seconds: 334.611137867
Total Time Elapsed: 00:05:34
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.94      0.88      0.91     15427
         1.0       0.90      0.93      0.92     25682
         2.0       0.91      0.90      0.91     10283

   micro avg       0.91      0.91      0.91     51392
   macro avg       0.92      0.91      0.91     51392
weighted avg       0.91      0.91      0.91     51392


Accuracy: 0.912826899128
## 2-kth fold --------------------------------
Training Time Elapsed: 00:17:35, 1055.64890718
Training set score: 0.897498
Training set loss: 0.279952
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_entry_4_h1_20_h2_20/ann_2_20_20.pkl

Elapsed Seconds: 364.800495863
Total Time Elapsed: 00:06:04
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.91      0.84      0.87     15427
         1.0       0.88      0.92      0.90     25682
         2.0       0.89      0.90      0.89     10283

   micro avg       0.89      0.89      0.89     51392
   macro avg       0.89      0.89      0.89     51392
weighted avg       0.89      0.89      0.89     51392


Accuracy: 0.891189290162
## 3-kth fold --------------------------------
Training Time Elapsed: 00:23:40, 1420.01448417
Training set score: 0.917083
Training set loss: 0.234853
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_entry_4_h1_20_h2_20/ann_3_20_20.pkl

Elapsed Seconds: 363.930966139
Total Time Elapsed: 00:06:03
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.93      0.75      0.83     15426
         1.0       0.83      0.89      0.86     25682
         2.0       0.79      0.89      0.84     10283

   micro avg       0.85      0.85      0.85     51391
   macro avg       0.85      0.84      0.84     51391
weighted avg       0.86      0.85      0.85     51391


Accuracy: 0.848417038003
## 4-kth fold --------------------------------
Training Time Elapsed: 00:29:23, 1763.89506006
Training set score: 0.915560
Training set loss: 0.234792
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_entry_4_h1_20_h2_20/ann_4_20_20.pkl

Elapsed Seconds: 343.576486111
Total Time Elapsed: 00:05:43
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.92      0.88      0.90     15426
         1.0       0.90      0.91      0.90     25682
         2.0       0.87      0.89      0.88     10283

   micro avg       0.90      0.90      0.90     51391
   macro avg       0.89      0.89      0.89     51391
weighted avg       0.90      0.90      0.90     51391


Accuracy: 0.896538304372
Average Training Accuracy for: (20, 20)
0.910703294666

Average Test Accuracy for: (20, 20)
0.890978148176

Elapsed Seconds: 1764.63356113
Total Time Elapsed: 00:29:24
