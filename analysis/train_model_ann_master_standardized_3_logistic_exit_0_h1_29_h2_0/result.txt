# Num of nodes: 29, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:05:00, 300.990040064
Training set score: 0.869317
Training set loss: 0.379725
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 445
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_exit_0_h1_29_h2_0/ann_0_29_0.pkl

Elapsed Seconds: 301.381239891
Total Time Elapsed: 00:05:01
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.92      0.81      0.86     15422
         1.0       0.86      0.88      0.87     25683
         2.0       0.80      0.88      0.84     10287

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.86      0.86      0.86     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.861165161893
## 1-kth fold --------------------------------
Training Time Elapsed: 00:10:56, 656.09090209
Training set score: 0.843744
Training set loss: 0.430147
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_exit_0_h1_29_h2_0/ann_1_29_0.pkl

Elapsed Seconds: 354.691585064
Total Time Elapsed: 00:05:54
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.82      0.85     15422
         1.0       0.84      0.87      0.85     25683
         2.0       0.82      0.85      0.83     10287

   micro avg       0.85      0.85      0.85     51392
   macro avg       0.85      0.84      0.85     51392
weighted avg       0.85      0.85      0.85     51392


Accuracy: 0.849353985056
## 2-kth fold --------------------------------
Training Time Elapsed: 00:15:57, 957.786978006
Training set score: 0.829335
Training set loss: 0.436629
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 430
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_exit_0_h1_29_h2_0/ann_2_29_0.pkl

Elapsed Seconds: 301.404873848
Total Time Elapsed: 00:05:01
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.87      0.71      0.78     15422
         1.0       0.81      0.88      0.84     25683
         2.0       0.82      0.87      0.85     10287

   micro avg       0.83      0.83      0.83     51392
   macro avg       0.83      0.82      0.82     51392
weighted avg       0.83      0.83      0.83     51392


Accuracy: 0.827093711083
## 3-kth fold --------------------------------
Training Time Elapsed: 00:21:55, 1315.44575405
Training set score: 0.829087
Training set loss: 0.424540
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_exit_0_h1_29_h2_0/ann_3_29_0.pkl

Elapsed Seconds: 357.320227861
Total Time Elapsed: 00:05:57
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.77      0.81      0.79     15422
         1.0       0.76      0.84      0.80     25682
         2.0       0.89      0.62      0.73     10287

   micro avg       0.78      0.78      0.78     51391
   macro avg       0.81      0.75      0.77     51391
weighted avg       0.79      0.78      0.78     51391


Accuracy: 0.782997022825
## 4-kth fold --------------------------------
Training Time Elapsed: 00:27:38, 1658.99246597
Training set score: 0.824130
Training set loss: 0.441787
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 490
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_exit_0_h1_29_h2_0/ann_4_29_0.pkl

Elapsed Seconds: 343.271250963
Total Time Elapsed: 00:05:43
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.85      0.73      0.79     15422
         1.0       0.80      0.85      0.82     25682
         2.0       0.81      0.86      0.84     10287

   micro avg       0.82      0.82      0.82     51391
   macro avg       0.82      0.81      0.82     51391
weighted avg       0.82      0.82      0.81     51391


Accuracy: 0.815609737114
Average Training Accuracy for: (29, 0)
0.839122565777

Average Test Accuracy for: (29, 0)
0.827243923594

Elapsed Seconds: 1659.9144001
Total Time Elapsed: 00:27:39
