# Num of nodes: 25, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:04:59, 299.037662983
Training set score: 0.866413
Training set loss: 0.371249
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_exit_1_h1_25_h2_0/ann_0_25_0.pkl

Elapsed Seconds: 299.392343998
Total Time Elapsed: 00:04:59
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.86      0.82      0.84     15427
         1.0       0.85      0.87      0.86     25682
         2.0       0.85      0.86      0.85     10283

   micro avg       0.85      0.85      0.85     51392
   macro avg       0.85      0.85      0.85     51392
weighted avg       0.85      0.85      0.85     51392


Accuracy: 0.853304016189
## 1-kth fold --------------------------------
Training Time Elapsed: 00:10:32, 632.37272501
Training set score: 0.856533
Training set loss: 0.379926
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_exit_1_h1_25_h2_0/ann_1_25_0.pkl

Elapsed Seconds: 333.028712034
Total Time Elapsed: 00:05:33
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.88      0.80      0.84     15427
         1.0       0.84      0.89      0.86     25682
         2.0       0.88      0.84      0.86     10283

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.87      0.85      0.85     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.856028175592
## 2-kth fold --------------------------------
Training Time Elapsed: 00:16:04, 964.478054047
Training set score: 0.871200
Training set loss: 0.367200
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_exit_1_h1_25_h2_0/ann_2_25_0.pkl

Elapsed Seconds: 331.955183983
Total Time Elapsed: 00:05:31
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.91      0.81      0.85     15427
         1.0       0.85      0.91      0.88     25682
         2.0       0.84      0.85      0.85     10283

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.87      0.86      0.86     51392
weighted avg       0.87      0.87      0.86     51392


Accuracy: 0.865368150685
## 3-kth fold --------------------------------
Training Time Elapsed: 00:21:13, 1273.24273086
Training set score: 0.857686
Training set loss: 0.378403
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 455
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_exit_1_h1_25_h2_0/ann_3_25_0.pkl

Elapsed Seconds: 308.176002979
Total Time Elapsed: 00:05:08
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.80      0.84      0.82     15426
         1.0       0.81      0.83      0.82     25682
         2.0       0.84      0.71      0.77     10283

   micro avg       0.81      0.81      0.81     51391
   macro avg       0.82      0.80      0.81     51391
weighted avg       0.81      0.81      0.81     51391


Accuracy: 0.812749314082
## 4-kth fold --------------------------------
Training Time Elapsed: 00:26:42, 1602.8333559
Training set score: 0.870845
Training set loss: 0.362737
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_3_logistic_exit_1_h1_25_h2_0/ann_4_25_0.pkl

Elapsed Seconds: 329.428560019
Total Time Elapsed: 00:05:29
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.83      0.86     15426
         1.0       0.85      0.88      0.87     25682
         2.0       0.85      0.86      0.86     10283

   micro avg       0.86      0.86      0.86     51391
   macro avg       0.86      0.86      0.86     51391
weighted avg       0.86      0.86      0.86     51391


Accuracy: 0.8628942811
Average Training Accuracy for: (25, 0)
0.864535255313

Average Test Accuracy for: (25, 0)
0.85006878753

Elapsed Seconds: 1603.75737596
Total Time Elapsed: 00:26:43
