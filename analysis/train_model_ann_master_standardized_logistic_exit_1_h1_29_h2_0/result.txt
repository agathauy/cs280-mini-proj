# Num of nodes: 29, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:04:30, 270.118920088
Training set score: 0.731900
Training set loss: 0.729001
MLP # layers: 3
MLP # outputs: 5
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2. 3. 4.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_logistic_exit_1_h1_29_h2_0/ann_0_29_0.pkl

Elapsed Seconds: 270.106332064
Total Time Elapsed: 00:04:30
Features in training dataset (80%):
Count: (205565,)
Counter({0.0: 41135, 3.0: 41135, 4.0: 41132, 2.0: 41083, 1.0: 41080})
Features in test dataset (20%):
Count: (51393,)
Counter({0.0: 10284, 3.0: 10284, 4.0: 10283, 1.0: 10271, 2.0: 10271})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.72      0.76      0.74     10284
         1.0       0.66      0.56      0.61     10271
         2.0       0.62      0.59      0.61     10271
         3.0       0.67      0.77      0.71     10284
         4.0       0.88      0.87      0.87     10283

   micro avg       0.71      0.71      0.71     51393
   macro avg       0.71      0.71      0.71     51393
weighted avg       0.71      0.71      0.71     51393


Accuracy: 0.710193995291
## 1-kth fold --------------------------------
Training Time Elapsed: 00:08:51, 531.167623997
Training set score: 0.696973
Training set loss: 0.791319
MLP # layers: 3
MLP # outputs: 5
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2. 3. 4.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_logistic_exit_1_h1_29_h2_0/ann_1_29_0.pkl

Elapsed Seconds: 260.784340858
Total Time Elapsed: 00:04:20
Features in training dataset (80%):
Count: (205566,)
Counter({0.0: 41135, 3.0: 41135, 4.0: 41132, 2.0: 41083, 1.0: 41081})
Features in test dataset (20%):
Count: (51392,)
Counter({0.0: 10284, 3.0: 10284, 4.0: 10283, 2.0: 10271, 1.0: 10270})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.74      0.74      0.74     10284
         1.0       0.61      0.60      0.61     10270
         2.0       0.61      0.57      0.59     10271
         3.0       0.66      0.68      0.67     10284
         4.0       0.83      0.87      0.85     10283

   micro avg       0.69      0.69      0.69     51392
   macro avg       0.69      0.69      0.69     51392
weighted avg       0.69      0.69      0.69     51392


Accuracy: 0.691683530511
## 2-kth fold --------------------------------
Training Time Elapsed: 00:13:26, 806.922498226
Training set score: 0.734995
Training set loss: 0.721245
MLP # layers: 3
MLP # outputs: 5
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2. 3. 4.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_logistic_exit_1_h1_29_h2_0/ann_2_29_0.pkl

Elapsed Seconds: 275.544955969
Total Time Elapsed: 00:04:35
Features in training dataset (80%):
Count: (205566,)
Counter({0.0: 41135, 3.0: 41135, 4.0: 41132, 2.0: 41083, 1.0: 41081})
Features in test dataset (20%):
Count: (51392,)
Counter({0.0: 10284, 3.0: 10284, 4.0: 10283, 2.0: 10271, 1.0: 10270})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.75      0.74      0.74     10284
         1.0       0.71      0.60      0.65     10270
         2.0       0.63      0.67      0.65     10271
         3.0       0.69      0.73      0.71     10284
         4.0       0.86      0.90      0.88     10283

   micro avg       0.73      0.73      0.73     51392
   macro avg       0.73      0.73      0.73     51392
weighted avg       0.73      0.73      0.73     51392


Accuracy: 0.728070516812
## 3-kth fold --------------------------------
Training Time Elapsed: 00:17:26, 1046.66511703
Training set score: 0.667314
Training set loss: 0.826136
MLP # layers: 3
MLP # outputs: 5
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2. 3. 4.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_logistic_exit_1_h1_29_h2_0/ann_3_29_0.pkl

Elapsed Seconds: 239.473503828
Total Time Elapsed: 00:03:59
Features in training dataset (80%):
Count: (205566,)
Counter({0.0: 41135, 3.0: 41135, 4.0: 41132, 2.0: 41083, 1.0: 41081})
Features in test dataset (20%):
Count: (51392,)
Counter({0.0: 10284, 3.0: 10284, 4.0: 10283, 2.0: 10271, 1.0: 10270})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.70      0.68      0.69     10284
         1.0       0.57      0.54      0.56     10270
         2.0       0.52      0.51      0.51     10271
         3.0       0.56      0.62      0.59     10284
         4.0       0.83      0.82      0.82     10283

   micro avg       0.63      0.63      0.63     51392
   macro avg       0.63      0.63      0.63     51392
weighted avg       0.63      0.63      0.63     51392


Accuracy: 0.632588729763
## 4-kth fold --------------------------------
Training Time Elapsed: 00:21:01, 1261.74838018
Training set score: 0.729663
Training set loss: 0.734795
MLP # layers: 3
MLP # outputs: 5
MLP # iterations: 466
MLP out_activation_: softmax
MLP classes: [0. 1. 2. 3. 4.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_logistic_exit_1_h1_29_h2_0/ann_4_29_0.pkl

Elapsed Seconds: 214.765985012
Total Time Elapsed: 00:03:34
Features in training dataset (80%):
Count: (205569,)
Counter({0.0: 41136, 3.0: 41136, 4.0: 41132, 2.0: 41084, 1.0: 41081})
Features in test dataset (20%):
Count: (51389,)
Counter({0.0: 10283, 3.0: 10283, 4.0: 10283, 1.0: 10270, 2.0: 10270})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.76      0.75      0.76     10283
         1.0       0.64      0.61      0.62     10270
         2.0       0.64      0.60      0.62     10270
         3.0       0.67      0.69      0.68     10283
         4.0       0.82      0.89      0.85     10283

   micro avg       0.71      0.71      0.71     51389
   macro avg       0.71      0.71      0.71     51389
weighted avg       0.71      0.71      0.71     51389


Accuracy: 0.708926034755
Average Training Accuracy for: (29, 0)
0.712168882901

Average Test Accuracy for: (29, 0)
0.694292561426

Elapsed Seconds: 1262.39631009
Total Time Elapsed: 00:21:02
