# Num of nodes: 13, 5 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:04:51, 291.191685915
Training set score: 0.629465
Training set loss: 0.912366
MLP # layers: 4
MLP # outputs: 5
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2. 3. 4.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_relu_entry_0_h1_13_h2_5/ann_0_13_5.pkl

Elapsed Seconds: 291.326277971
Total Time Elapsed: 00:04:51
Features in training dataset (80%):
Count: (205565,)
Counter({0.0: 41135, 3.0: 41135, 4.0: 41132, 2.0: 41083, 1.0: 41080})
Features in test dataset (20%):
Count: (51393,)
Counter({0.0: 10284, 3.0: 10284, 4.0: 10283, 1.0: 10271, 2.0: 10271})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.75      0.69      0.72     10284
         1.0       0.58      0.57      0.58     10271
         2.0       0.54      0.43      0.48     10271
         3.0       0.52      0.54      0.53     10284
         4.0       0.73      0.91      0.81     10283

   micro avg       0.63      0.63      0.63     51393
   macro avg       0.62      0.63      0.62     51393
weighted avg       0.62      0.63      0.62     51393


Accuracy: 0.62808164536
## 1-kth fold --------------------------------
Training Time Elapsed: 00:08:34, 514.508503914
Training set score: 0.627190
Training set loss: 0.937858
MLP # layers: 4
MLP # outputs: 5
MLP # iterations: 360
MLP out_activation_: softmax
MLP classes: [0. 1. 2. 3. 4.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_relu_entry_0_h1_13_h2_5/ann_1_13_5.pkl

Elapsed Seconds: 223.078546047
Total Time Elapsed: 00:03:43
Features in training dataset (80%):
Count: (205566,)
Counter({0.0: 41135, 3.0: 41135, 4.0: 41132, 2.0: 41083, 1.0: 41081})
Features in test dataset (20%):
Count: (51392,)
Counter({0.0: 10284, 3.0: 10284, 4.0: 10283, 2.0: 10271, 1.0: 10270})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.69      0.81      0.75     10284
         1.0       0.57      0.49      0.53     10270
         2.0       0.56      0.48      0.51     10271
         3.0       0.54      0.56      0.55     10284
         4.0       0.82      0.87      0.84     10283

   micro avg       0.64      0.64      0.64     51392
   macro avg       0.63      0.64      0.64     51392
weighted avg       0.63      0.64      0.64     51392


Accuracy: 0.64196762142
## 2-kth fold --------------------------------
Training Time Elapsed: 00:10:32, 632.72179389
Training set score: 0.639430
Training set loss: 0.922058
MLP # layers: 4
MLP # outputs: 5
MLP # iterations: 208
MLP out_activation_: softmax
MLP classes: [0. 1. 2. 3. 4.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_relu_entry_0_h1_13_h2_5/ann_2_13_5.pkl

Elapsed Seconds: 117.907044888
Total Time Elapsed: 00:01:57
Features in training dataset (80%):
Count: (205566,)
Counter({0.0: 41135, 3.0: 41135, 4.0: 41132, 2.0: 41083, 1.0: 41081})
Features in test dataset (20%):
Count: (51392,)
Counter({0.0: 10284, 3.0: 10284, 4.0: 10283, 2.0: 10271, 1.0: 10270})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.82      0.65      0.73     10284
         1.0       0.56      0.65      0.60     10270
         2.0       0.53      0.45      0.49     10271
         3.0       0.51      0.58      0.54     10284
         4.0       0.79      0.83      0.81     10283

   micro avg       0.63      0.63      0.63     51392
   macro avg       0.64      0.63      0.63     51392
weighted avg       0.64      0.63      0.63     51392


Accuracy: 0.633522727273
## 3-kth fold --------------------------------
Training Time Elapsed: 00:13:34, 814.188261032
Training set score: 0.692313
Training set loss: 0.819078
MLP # layers: 4
MLP # outputs: 5
MLP # iterations: 267
MLP out_activation_: softmax
MLP classes: [0. 1. 2. 3. 4.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_relu_entry_0_h1_13_h2_5/ann_3_13_5.pkl

Elapsed Seconds: 181.29330492
Total Time Elapsed: 00:03:01
Features in training dataset (80%):
Count: (205566,)
Counter({0.0: 41135, 3.0: 41135, 4.0: 41132, 2.0: 41083, 1.0: 41081})
Features in test dataset (20%):
Count: (51392,)
Counter({0.0: 10284, 3.0: 10284, 4.0: 10283, 2.0: 10271, 1.0: 10270})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.71      0.72      0.71     10284
         1.0       0.56      0.57      0.56     10270
         2.0       0.51      0.61      0.56     10271
         3.0       0.57      0.55      0.56     10284
         4.0       0.87      0.70      0.78     10283

   micro avg       0.63      0.63      0.63     51392
   macro avg       0.64      0.63      0.63     51392
weighted avg       0.64      0.63      0.63     51392


Accuracy: 0.630331569116
## 4-kth fold --------------------------------
Training Time Elapsed: 00:18:35, 1115.30162907
Training set score: 0.589077
Training set loss: 1.018279
MLP # layers: 4
MLP # outputs: 5
MLP # iterations: 444
MLP out_activation_: softmax
MLP classes: [0. 1. 2. 3. 4.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/../../models/ann/train_model_ann_master_standardized_relu_entry_0_h1_13_h2_5/ann_4_13_5.pkl

Elapsed Seconds: 300.890697956
Total Time Elapsed: 00:05:00
Features in training dataset (80%):
Count: (205569,)
Counter({0.0: 41136, 3.0: 41136, 4.0: 41132, 2.0: 41084, 1.0: 41081})
Features in test dataset (20%):
Count: (51389,)
Counter({0.0: 10283, 3.0: 10283, 4.0: 10283, 1.0: 10270, 2.0: 10270})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.71      0.66      0.69     10283
         1.0       0.48      0.35      0.41     10270
         2.0       0.60      0.49      0.54     10270
         3.0       0.43      0.59      0.50     10283
         4.0       0.72      0.83      0.77     10283

   micro avg       0.58      0.58      0.58     51389
   macro avg       0.59      0.58      0.58     51389
weighted avg       0.59      0.58      0.58     51389


Accuracy: 0.584930627177
Average Training Accuracy for: (13, 5)
0.635495035399

Average Test Accuracy for: (13, 5)
0.623766838069

Elapsed Seconds: 1115.89744401
Total Time Elapsed: 00:18:35
