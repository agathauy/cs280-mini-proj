# Num of nodes: 27, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:03:42, 222.09296298
Training set score: 0.889247
Training set loss: 0.273983
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 500
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann/train_model_ann_v2_master_standardized_3_logistic_exit_3_h1_27_h2_0/ann_0_27_0.pkl

Elapsed Seconds: 222.125291824
Total Time Elapsed: 00:03:42
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.83      0.76      0.79     15422
         2.0       0.90      0.93      0.92     35970

   micro avg       0.88      0.88      0.88     51392
   macro avg       0.86      0.85      0.85     51392
weighted avg       0.88      0.88      0.88     51392


Accuracy: 0.880818026152
## 1-kth fold --------------------------------
Training Time Elapsed: 00:07:35, 455.781246901
Training set score: 0.894005
Training set loss: 0.256303
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 500
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann/train_model_ann_v2_master_standardized_3_logistic_exit_3_h1_27_h2_0/ann_1_27_0.pkl

Elapsed Seconds: 233.096349955
Total Time Elapsed: 00:03:53
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.86      0.79      0.82     15422
         2.0       0.91      0.94      0.93     35970

   micro avg       0.90      0.90      0.90     51392
   macro avg       0.88      0.87      0.87     51392
weighted avg       0.90      0.90      0.90     51392


Accuracy: 0.897201899128
## 2-kth fold --------------------------------
Training Time Elapsed: 00:11:25, 685.884018898
Training set score: 0.926369
Training set loss: 0.208782
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 500
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann/train_model_ann_v2_master_standardized_3_logistic_exit_3_h1_27_h2_0/ann_2_27_0.pkl

Elapsed Seconds: 229.871718884
Total Time Elapsed: 00:03:49
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.82      0.85     15422
         2.0       0.92      0.96      0.94     35970

   micro avg       0.92      0.92      0.92     51392
   macro avg       0.91      0.89      0.90     51392
weighted avg       0.92      0.92      0.91     51392


Accuracy: 0.915979140722
## 3-kth fold --------------------------------
Training Time Elapsed: 00:14:59, 899.604920864
Training set score: 0.908458
Training set loss: 0.230683
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 470
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann/train_model_ann_v2_master_standardized_3_logistic_exit_3_h1_27_h2_0/ann_3_27_0.pkl

Elapsed Seconds: 213.358716965
Total Time Elapsed: 00:03:33
Features in training dataset (80%):
Count: (205567,)
Counter({2.0: 143879, 0.0: 61688})
Features in test dataset (20%):
Count: (51391,)
Counter({2.0: 35969, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.81      0.85      0.83     15422
         2.0       0.93      0.92      0.92     35969

   micro avg       0.90      0.90      0.90     51391
   macro avg       0.87      0.88      0.88     51391
weighted avg       0.90      0.90      0.90     51391


Accuracy: 0.895526454048
## 4-kth fold --------------------------------
Training Time Elapsed: 00:17:18, 1038.04727602
Training set score: 0.893748
Training set loss: 0.253215
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 316
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /Users/agatha/Documents/MSEE/CS280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann/train_model_ann_v2_master_standardized_3_logistic_exit_3_h1_27_h2_0/ann_4_27_0.pkl

Elapsed Seconds: 138.189577103
Total Time Elapsed: 00:02:18
Features in training dataset (80%):
Count: (205567,)
Counter({2.0: 143879, 0.0: 61688})
Features in test dataset (20%):
Count: (51391,)
Counter({2.0: 35969, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.85      0.76      0.80     15422
         2.0       0.90      0.94      0.92     35969

   micro avg       0.89      0.89      0.89     51391
   macro avg       0.87      0.85      0.86     51391
weighted avg       0.89      0.89      0.89     51391


Accuracy: 0.886867350314
Average Training Accuracy for: (27, 0)
0.90236536956

Average Test Accuracy for: (27, 0)
0.895278574073

Elapsed Seconds: 1038.76590085
Total Time Elapsed: 00:17:18
