# Num of nodes: 28, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:04:48, 288.942970037
Training set score: 0.715073
Training set loss: 0.797870
MLP # layers: 3
MLP # outputs: 5
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2. 3. 4.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann/train_model_ann_v2_master_standardized_logistic_exit_2_h1_28_h2_0/ann_0_28_0.pkl

Elapsed Seconds: 289.257261992
Total Time Elapsed: 00:04:49
Features in training dataset (80%):
Count: (205565,)
Counter({4.0: 41148, 0.0: 41124, 3.0: 41117, 1.0: 41092, 2.0: 41084})
Features in test dataset (20%):
Count: (51393,)
Counter({4.0: 10287, 0.0: 10281, 3.0: 10280, 1.0: 10274, 2.0: 10271})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.77      0.72      0.74     10281
         1.0       0.72      0.63      0.67     10274
         2.0       0.65      0.61      0.63     10271
         3.0       0.61      0.64      0.63     10280
         4.0       0.77      0.91      0.83     10287

   micro avg       0.70      0.70      0.70     51393
   macro avg       0.70      0.70      0.70     51393
weighted avg       0.70      0.70      0.70     51393


Accuracy: 0.704629035083
## 1-kth fold --------------------------------
Training Time Elapsed: 00:09:40, 580.219402075
Training set score: 0.649572
Training set loss: 0.888599
MLP # layers: 3
MLP # outputs: 5
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2. 3. 4.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann/train_model_ann_v2_master_standardized_logistic_exit_2_h1_28_h2_0/ann_1_28_0.pkl

Elapsed Seconds: 290.967509985
Total Time Elapsed: 00:04:50
Features in training dataset (80%):
Count: (205566,)
Counter({4.0: 41148, 0.0: 41124, 3.0: 41117, 1.0: 41093, 2.0: 41084})
Features in test dataset (20%):
Count: (51392,)
Counter({4.0: 10287, 0.0: 10281, 3.0: 10280, 1.0: 10273, 2.0: 10271})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.77      0.74      0.75     10281
         1.0       0.56      0.57      0.56     10273
         2.0       0.57      0.55      0.56     10271
         3.0       0.54      0.51      0.53     10280
         4.0       0.77      0.86      0.81     10287

   micro avg       0.65      0.65      0.65     51392
   macro avg       0.64      0.65      0.64     51392
weighted avg       0.64      0.65      0.64     51392


Accuracy: 0.646637608966
## 2-kth fold --------------------------------
Training Time Elapsed: 00:14:32, 872.454364061
Training set score: 0.700531
Training set loss: 0.825741
MLP # layers: 3
MLP # outputs: 5
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2. 3. 4.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann/train_model_ann_v2_master_standardized_logistic_exit_2_h1_28_h2_0/ann_2_28_0.pkl

Elapsed Seconds: 292.07901907
Total Time Elapsed: 00:04:52
Features in training dataset (80%):
Count: (205567,)
Counter({4.0: 41148, 0.0: 41124, 3.0: 41118, 1.0: 41093, 2.0: 41084})
Features in test dataset (20%):
Count: (51391,)
Counter({4.0: 10287, 0.0: 10281, 3.0: 10279, 1.0: 10273, 2.0: 10271})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.81      0.69      0.75     10281
         1.0       0.67      0.69      0.68     10273
         2.0       0.62      0.60      0.61     10271
         3.0       0.63      0.66      0.64     10279
         4.0       0.80      0.87      0.84     10287

   micro avg       0.70      0.70      0.70     51391
   macro avg       0.71      0.70      0.70     51391
weighted avg       0.71      0.70      0.70     51391


Accuracy: 0.704072697554
## 3-kth fold --------------------------------
Training Time Elapsed: 00:19:29, 1169.67809105
Training set score: 0.731324
Training set loss: 0.767609
MLP # layers: 3
MLP # outputs: 5
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2. 3. 4.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann/train_model_ann_v2_master_standardized_logistic_exit_2_h1_28_h2_0/ann_3_28_0.pkl

Elapsed Seconds: 297.074491024
Total Time Elapsed: 00:04:57
Features in training dataset (80%):
Count: (205567,)
Counter({4.0: 41148, 0.0: 41124, 3.0: 41118, 1.0: 41093, 2.0: 41084})
Features in test dataset (20%):
Count: (51391,)
Counter({4.0: 10287, 0.0: 10281, 3.0: 10279, 1.0: 10273, 2.0: 10271})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.75      0.76      0.76     10281
         1.0       0.63      0.67      0.65     10273
         2.0       0.58      0.60      0.59     10271
         3.0       0.57      0.61      0.59     10279
         4.0       0.86      0.70      0.77     10287

   micro avg       0.67      0.67      0.67     51391
   macro avg       0.68      0.67      0.67     51391
weighted avg       0.68      0.67      0.67     51391


Accuracy: 0.668424432294
## 4-kth fold --------------------------------
Training Time Elapsed: 00:24:30, 1470.73241711
Training set score: 0.713422
Training set loss: 0.794688
MLP # layers: 3
MLP # outputs: 5
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2. 3. 4.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann/train_model_ann_v2_master_standardized_logistic_exit_2_h1_28_h2_0/ann_4_28_0.pkl

Elapsed Seconds: 300.706212044
Total Time Elapsed: 00:05:00
Features in training dataset (80%):
Count: (205567,)
Counter({4.0: 41148, 0.0: 41124, 3.0: 41118, 1.0: 41093, 2.0: 41084})
Features in test dataset (20%):
Count: (51391,)
Counter({4.0: 10287, 0.0: 10281, 3.0: 10279, 1.0: 10273, 2.0: 10271})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.85      0.72      0.78     10281
         1.0       0.67      0.72      0.69     10273
         2.0       0.63      0.59      0.61     10271
         3.0       0.64      0.62      0.62     10279
         4.0       0.76      0.90      0.82     10287

   micro avg       0.71      0.71      0.71     51391
   macro avg       0.71      0.71      0.71     51391
weighted avg       0.71      0.71      0.71     51391


Accuracy: 0.708626024012
Average Training Accuracy for: (28, 0)
0.701984345463

Average Test Accuracy for: (28, 0)
0.686477959582

Elapsed Seconds: 1471.55785298
Total Time Elapsed: 00:24:31
