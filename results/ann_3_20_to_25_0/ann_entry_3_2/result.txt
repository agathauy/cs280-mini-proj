# ann_entry_3_2
['station', 'month', 'day', 'day_of_the_week', 'hour', 'num_trains', 'avg_air_pressure', 'temperature']
# Num of nodes: 20, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:03:29, 209.119684219
Training set score: 0.862419
Training set loss: 0.382051
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_0_20_0.pkl

Elapsed Seconds: 210.605663061
Total Time Elapsed: 00:03:30
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.88      0.81      0.84     15427
         1.0       0.85      0.89      0.87     25682
         2.0       0.86      0.85      0.85     10283

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.86      0.85      0.85     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.857176214197
## 1-kth fold --------------------------------
Training Time Elapsed: 00:03:22, 202.832268953
Training set score: 0.859447
Training set loss: 0.385979
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 469
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_1_20_0.pkl

Elapsed Seconds: 204.189417839
Total Time Elapsed: 00:03:24
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.88      0.83      0.85     15427
         1.0       0.84      0.89      0.86     25682
         2.0       0.89      0.81      0.84     10283

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.87      0.84      0.85     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.857974003736
## 2-kth fold --------------------------------
Training Time Elapsed: 00:02:38, 158.727813005
Training set score: 0.836680
Training set loss: 0.427920
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 352
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_2_20_0.pkl

Elapsed Seconds: 160.193206072
Total Time Elapsed: 00:02:40
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.85      0.77      0.81     15427
         1.0       0.81      0.88      0.85     25682
         2.0       0.86      0.81      0.84     10283

   micro avg       0.83      0.83      0.83     51392
   macro avg       0.84      0.82      0.83     51392
weighted avg       0.84      0.83      0.83     51392


Accuracy: 0.83337873599
## 3-kth fold --------------------------------
Training Time Elapsed: 00:03:38, 218.172346115
Training set score: 0.858460
Training set loss: 0.376691
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 497
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_3_20_0.pkl

Elapsed Seconds: 219.552893877
Total Time Elapsed: 00:03:39
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.88      0.76      0.82     15426
         1.0       0.81      0.90      0.85     25682
         2.0       0.85      0.79      0.82     10283

   micro avg       0.83      0.83      0.83     51391
   macro avg       0.85      0.82      0.83     51391
weighted avg       0.84      0.83      0.83     51391


Accuracy: 0.83499056255
## 4-kth fold --------------------------------
Training Time Elapsed: 00:03:27, 207.106045961
Training set score: 0.853941
Training set loss: 0.393709
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 460
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_4_20_0.pkl

Elapsed Seconds: 208.484124899
Total Time Elapsed: 00:03:28
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.88      0.81      0.84     15426
         1.0       0.84      0.87      0.85     25682
         2.0       0.83      0.87      0.85     10283

   micro avg       0.85      0.85      0.85     51391
   macro avg       0.85      0.85      0.85     51391
weighted avg       0.85      0.85      0.85     51391


Accuracy: 0.849039715125
Sorted List of Temp Training Scores: 
[]

Sorted List of Temp Test Accuracies: 
[]

Average Training Accuracy for: (20, 0)
0.854189202105

Average Test Accuracy for: (20, 0)
0.84651184632

Elapsed Seconds: 1004.23523593
Total Time Elapsed: 00:16:44
# Num of nodes: 21, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:02:12, 132.641223907
Training set score: 0.834092
Training set loss: 0.426182
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 295
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_0_21_0.pkl

Elapsed Seconds: 134.03983593
Total Time Elapsed: 00:02:14
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.83      0.78      0.81     15427
         1.0       0.82      0.87      0.84     25682
         2.0       0.87      0.80      0.83     10283

   micro avg       0.83      0.83      0.83     51392
   macro avg       0.84      0.82      0.83     51392
weighted avg       0.83      0.83      0.83     51392


Accuracy: 0.830070828144
## 1-kth fold --------------------------------
Training Time Elapsed: 00:03:22, 202.155596972
Training set score: 0.830629
Training set loss: 0.434102
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 429
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_1_21_0.pkl

Elapsed Seconds: 203.578466892
Total Time Elapsed: 00:03:23
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.83      0.81      0.82     15427
         1.0       0.82      0.86      0.84     25682
         2.0       0.86      0.79      0.83     10283

   micro avg       0.83      0.83      0.83     51392
   macro avg       0.84      0.82      0.83     51392
weighted avg       0.83      0.83      0.83     51392


Accuracy: 0.828339041096
## 2-kth fold --------------------------------
Training Time Elapsed: 00:03:31, 211.827424049
Training set score: 0.854679
Training set loss: 0.405202
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 447
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_2_21_0.pkl

Elapsed Seconds: 213.200084209
Total Time Elapsed: 00:03:33
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.88      0.80      0.84     15427
         1.0       0.84      0.89      0.86     25682
         2.0       0.85      0.83      0.84     10283

   micro avg       0.85      0.85      0.85     51392
   macro avg       0.85      0.84      0.85     51392
weighted avg       0.85      0.85      0.85     51392


Accuracy: 0.850171232877
## 3-kth fold --------------------------------
Training Time Elapsed: 00:02:43, 163.393356085
Training set score: 0.842231
Training set loss: 0.401887
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 334
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_3_21_0.pkl

Elapsed Seconds: 164.913547039
Total Time Elapsed: 00:02:44
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.75      0.81      0.78     15426
         1.0       0.79      0.80      0.79     25682
         2.0       0.82      0.69      0.75     10283

   micro avg       0.78      0.78      0.78     51391
   macro avg       0.79      0.77      0.77     51391
weighted avg       0.78      0.78      0.78     51391


Accuracy: 0.780156058454
## 4-kth fold --------------------------------
Training Time Elapsed: 00:03:12, 192.155522108
Training set score: 0.831028
Training set loss: 0.431315
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 429
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_4_21_0.pkl

Elapsed Seconds: 193.510978937
Total Time Elapsed: 00:03:13
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.85      0.78      0.81     15426
         1.0       0.81      0.86      0.84     25682
         2.0       0.84      0.83      0.83     10283

   micro avg       0.83      0.83      0.83     51391
   macro avg       0.83      0.82      0.83     51391
weighted avg       0.83      0.83      0.83     51391


Accuracy: 0.827927068942
Sorted List of Temp Training Scores: 
[[(20, 0), 0.854189202105079]]

Sorted List of Temp Test Accuracies: 
[[(20, 0), 0.8465118463195738]]

Average Training Accuracy for: (21, 0)
0.83853198169

Average Test Accuracy for: (21, 0)
0.823332845903

Elapsed Seconds: 910.413233042
Total Time Elapsed: 00:15:10
# Num of nodes: 22, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:04:02, 242.336250067
Training set score: 0.849041
Training set loss: 0.395086
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_0_22_0.pkl

Elapsed Seconds: 243.790473938
Total Time Elapsed: 00:04:03
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.85      0.81      0.83     15427
         1.0       0.83      0.88      0.85     25682
         2.0       0.86      0.81      0.83     10283

   micro avg       0.84      0.84      0.84     51392
   macro avg       0.85      0.83      0.84     51392
weighted avg       0.84      0.84      0.84     51392


Accuracy: 0.844333748443
## 1-kth fold --------------------------------
Training Time Elapsed: 00:02:46, 166.207312822
Training set score: 0.850379
Training set loss: 0.393962
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 337
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_1_22_0.pkl

Elapsed Seconds: 167.821736097
Total Time Elapsed: 00:02:47
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.83      0.86     15427
         1.0       0.84      0.89      0.86     25682
         2.0       0.87      0.81      0.84     10283

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.86      0.84      0.85     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.856436799502
## 2-kth fold --------------------------------
Training Time Elapsed: 00:03:24, 204.463677883
Training set score: 0.869502
Training set loss: 0.371263
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 428
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_2_22_0.pkl

Elapsed Seconds: 206.049068928
Total Time Elapsed: 00:03:26
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.81      0.85     15427
         1.0       0.85      0.91      0.88     25682
         2.0       0.88      0.84      0.86     10283

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.87      0.85      0.86     51392
weighted avg       0.87      0.87      0.87     51392


Accuracy: 0.866341064757
## 3-kth fold --------------------------------
Training Time Elapsed: 00:04:13, 253.564477921
Training set score: 0.858124
Training set loss: 0.376224
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_3_22_0.pkl

Elapsed Seconds: 255.110994816
Total Time Elapsed: 00:04:15
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.85      0.80      0.82     15426
         1.0       0.82      0.88      0.85     25682
         2.0       0.87      0.77      0.82     10283

   micro avg       0.83      0.83      0.83     51391
   macro avg       0.84      0.82      0.83     51391
weighted avg       0.84      0.83      0.83     51391


Accuracy: 0.833608997684
## 4-kth fold --------------------------------
Training Time Elapsed: 00:03:34, 214.069031
Training set score: 0.854072
Training set loss: 0.388363
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 427
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_4_22_0.pkl

Elapsed Seconds: 215.682342052
Total Time Elapsed: 00:03:35
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.85      0.80      0.82     15426
         1.0       0.83      0.85      0.84     25682
         2.0       0.82      0.86      0.84     10283

   micro avg       0.84      0.84      0.84     51391
   macro avg       0.84      0.83      0.83     51391
weighted avg       0.84      0.84      0.84     51391


Accuracy: 0.835768908953
Sorted List of Temp Training Scores: 
[[(20, 0), 0.854189202105079], [(21, 0), 0.8385319816897608]]

Sorted List of Temp Test Accuracies: 
[[(20, 0), 0.8465118463195738], [(21, 0), 0.8233328459025817]]

Average Training Accuracy for: (22, 0)
0.856223585422

Average Test Accuracy for: (22, 0)
0.847297903868

Elapsed Seconds: 1089.79781508
Total Time Elapsed: 00:18:09
# Num of nodes: 23, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:04:16, 256.429035902
Training set score: 0.863343
Training set loss: 0.386378
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_0_23_0.pkl

Elapsed Seconds: 259.06796217
Total Time Elapsed: 00:04:19
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.87      0.81      0.84     15427
         1.0       0.85      0.89      0.87     25682
         2.0       0.86      0.85      0.85     10283

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.86      0.85      0.85     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.855522260274
## 1-kth fold --------------------------------
Training Time Elapsed: 00:03:58, 238.064776897
Training set score: 0.834900
Training set loss: 0.419888
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 477
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_1_23_0.pkl

Elapsed Seconds: 239.545099974
Total Time Elapsed: 00:03:59
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.84      0.79      0.81     15427
         1.0       0.82      0.86      0.84     25682
         2.0       0.86      0.82      0.84     10283

   micro avg       0.83      0.83      0.83     51392
   macro avg       0.84      0.82      0.83     51392
weighted avg       0.83      0.83      0.83     51392


Accuracy: 0.832289072229
## 2-kth fold --------------------------------
Training Time Elapsed: 00:04:02, 242.692955971
Training set score: 0.864598
Training set loss: 0.368779
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 479
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_2_23_0.pkl

Elapsed Seconds: 244.162725925
Total Time Elapsed: 00:04:04
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.88      0.82      0.85     15427
         1.0       0.85      0.90      0.87     25682
         2.0       0.88      0.83      0.85     10283

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.87      0.85      0.86     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.86071762142
## 3-kth fold --------------------------------
Training Time Elapsed: 00:04:10, 250.061490059
Training set score: 0.853153
Training set loss: 0.378125
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_3_23_0.pkl

Elapsed Seconds: 251.613918066
Total Time Elapsed: 00:04:11
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.82      0.79      0.81     15426
         1.0       0.79      0.87      0.83     25682
         2.0       0.86      0.71      0.77     10283

   micro avg       0.81      0.81      0.81     51391
   macro avg       0.82      0.79      0.80     51391
weighted avg       0.81      0.81      0.81     51391


Accuracy: 0.811212079936
## 4-kth fold --------------------------------
Training Time Elapsed: 00:03:28, 208.031445026
Training set score: 0.836418
Training set loss: 0.413057
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 425
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_4_23_0.pkl

Elapsed Seconds: 209.691527843
Total Time Elapsed: 00:03:29
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.79      0.76      0.78     15426
         1.0       0.81      0.81      0.81     25682
         2.0       0.81      0.84      0.82     10283

   micro avg       0.80      0.80      0.80     51391
   macro avg       0.80      0.80      0.80     51391
weighted avg       0.80      0.80      0.80     51391


Accuracy: 0.800451440914
Sorted List of Temp Training Scores: 
[[(22, 0), 0.8562235854216906], [(20, 0), 0.854189202105079], [(21, 0), 0.8385319816897608]]

Sorted List of Temp Test Accuracies: 
[[(22, 0), 0.847297903867943], [(20, 0), 0.8465118463195738], [(21, 0), 0.8233328459025817]]

Average Training Accuracy for: (23, 0)
0.850482385637

Average Test Accuracy for: (23, 0)
0.832038494955

Elapsed Seconds: 1206.96060205
Total Time Elapsed: 00:20:06
# Num of nodes: 24, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:03:18, 198.37490797
Training set score: 0.852592
Training set loss: 0.390972
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 394
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_0_24_0.pkl

Elapsed Seconds: 199.974955082
Total Time Elapsed: 00:03:19
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.86      0.81      0.83     15427
         1.0       0.83      0.89      0.86     25682
         2.0       0.87      0.79      0.83     10283

   micro avg       0.84      0.84      0.84     51392
   macro avg       0.85      0.83      0.84     51392
weighted avg       0.85      0.84      0.84     51392


Accuracy: 0.844586706102
## 1-kth fold --------------------------------
Training Time Elapsed: 00:04:11, 251.596361876
Training set score: 0.873544
Training set loss: 0.353816
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_1_24_0.pkl

Elapsed Seconds: 253.28621006
Total Time Elapsed: 00:04:13
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.83      0.87     15427
         1.0       0.85      0.91      0.88     25682
         2.0       0.89      0.84      0.86     10283

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.88      0.86      0.87     51392
weighted avg       0.87      0.87      0.87     51392


Accuracy: 0.871205635118
## 2-kth fold --------------------------------
Training Time Elapsed: 01:40:01, 6001.8190999
Training set score: 0.870397
Training set loss: 0.366215
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_2_24_0.pkl

Elapsed Seconds: 6003.43228793
Total Time Elapsed: 01:40:03
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.81      0.85     15427
         1.0       0.85      0.90      0.88     25682
         2.0       0.85      0.87      0.86     10283

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.87      0.86      0.86     51392
weighted avg       0.87      0.86      0.86     51392


Accuracy: 0.86490115193
## 3-kth fold --------------------------------
Training Time Elapsed: 00:02:24, 144.528571844
Training set score: 0.838345
Training set loss: 0.410169
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 288
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_3_24_0.pkl

Elapsed Seconds: 146.09081912
Total Time Elapsed: 00:02:26
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.75      0.81      0.78     15426
         1.0       0.79      0.80      0.80     25682
         2.0       0.84      0.70      0.76     10283

   micro avg       0.79      0.79      0.79     51391
   macro avg       0.79      0.77      0.78     51391
weighted avg       0.79      0.79      0.79     51391


Accuracy: 0.785721235236
## 4-kth fold --------------------------------
Training Time Elapsed: 00:02:54, 174.227263927
Training set score: 0.853843
Training set loss: 0.389829
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 342
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_4_24_0.pkl

Elapsed Seconds: 175.722471952
Total Time Elapsed: 00:02:55
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.82      0.85     15426
         1.0       0.84      0.88      0.86     25682
         2.0       0.83      0.81      0.82     10283

   micro avg       0.85      0.85      0.85     51391
   macro avg       0.85      0.84      0.84     51391
weighted avg       0.85      0.85      0.85     51391


Accuracy: 0.849545640287
Sorted List of Temp Training Scores: 
[[(22, 0), 0.8562235854216906], [(20, 0), 0.854189202105079], [(23, 0), 0.8504823856369275], [(21, 0), 0.8385319816897608]]

Sorted List of Temp Test Accuracies: 
[[(22, 0), 0.847297903867943], [(20, 0), 0.8465118463195738], [(23, 0), 0.8320384949545488], [(21, 0), 0.8233328459025817]]

Average Training Accuracy for: (24, 0)
0.857744284378

Average Test Accuracy for: (24, 0)
0.843192073735

Elapsed Seconds: 6781.26583385
Total Time Elapsed: 01:53:01
# Num of nodes: 25, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:02:44, 164.339709044
Training set score: 0.836787
Training set loss: 0.416960
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 320
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_0_25_0.pkl

Elapsed Seconds: 165.80023694
Total Time Elapsed: 00:02:45
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.83      0.79      0.81     15427
         1.0       0.82      0.87      0.84     25682
         2.0       0.88      0.81      0.84     10283

   micro avg       0.83      0.83      0.83     51392
   macro avg       0.84      0.82      0.83     51392
weighted avg       0.84      0.83      0.83     51392


Accuracy: 0.833865193026
## 1-kth fold --------------------------------
Training Time Elapsed: 00:04:09, 249.666028976
Training set score: 0.855764
Training set loss: 0.388051
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 487
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_1_25_0.pkl

Elapsed Seconds: 251.204806089
Total Time Elapsed: 00:04:11
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.88      0.82      0.85     15427
         1.0       0.84      0.89      0.86     25682
         2.0       0.88      0.82      0.85     10283

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.86      0.84      0.85     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.855288760897
## 2-kth fold --------------------------------
Training Time Elapsed: 00:03:12, 192.790386915
Training set score: 0.869093
Training set loss: 0.369620
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 356
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_2_25_0.pkl

Elapsed Seconds: 195.046303034
Total Time Elapsed: 00:03:15
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.80      0.85     15427
         1.0       0.85      0.91      0.88     25682
         2.0       0.86      0.85      0.86     10283

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.87      0.85      0.86     51392
weighted avg       0.87      0.86      0.86     51392


Accuracy: 0.864317403487
## 3-kth fold --------------------------------
Training Time Elapsed: 00:03:32, 212.878988028
Training set score: 0.870315
Training set loss: 0.348237
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 389
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_3_25_0.pkl

Elapsed Seconds: 214.670238972
Total Time Elapsed: 00:03:34
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.82      0.83      0.82     15426
         1.0       0.81      0.86      0.83     25682
         2.0       0.87      0.73      0.80     10283

   micro avg       0.82      0.82      0.82     51391
   macro avg       0.83      0.81      0.82     51391
weighted avg       0.83      0.82      0.82     51391


Accuracy: 0.824191006207
## 4-kth fold --------------------------------
Training Time Elapsed: 00:30:46, 1846.42785215
Training set score: 0.851567
Training set loss: 0.379946
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 437
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_entry_3_2/ann_4_25_0.pkl

Elapsed Seconds: 1848.06168413
Total Time Elapsed: 00:30:48
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.85      0.84      0.84     15426
         1.0       0.84      0.85      0.84     25682
         2.0       0.82      0.81      0.81     10283

   micro avg       0.84      0.84      0.84     51391
   macro avg       0.83      0.83      0.83     51391
weighted avg       0.84      0.84      0.84     51391


Accuracy: 0.836060788854
Sorted List of Temp Training Scores: 
[[(24, 0), 0.8577442843778169], [(22, 0), 0.8562235854216906], [(20, 0), 0.854189202105079], [(23, 0), 0.8504823856369275], [(21, 0), 0.8385319816897608]]

Sorted List of Temp Test Accuracies: 
[[(22, 0), 0.847297903867943], [(20, 0), 0.8465118463195738], [(24, 0), 0.8431920737347273], [(23, 0), 0.8320384949545488], [(21, 0), 0.8233328459025817]]

Average Training Accuracy for: (25, 0)
0.85670517315

Average Test Accuracy for: (25, 0)
0.842744630494

Elapsed Seconds: 2678.0810051
Total Time Elapsed: 00:44:38
List of Training Scores: 
[[(20, 0), 0.854189202105079], [(21, 0), 0.8385319816897608], [(22, 0), 0.8562235854216906], [(23, 0), 0.8504823856369275], [(24, 0), 0.8577442843778169], [(25, 0), 0.8567051731498102]]

List of Test Accuracies: 
[[(20, 0), 0.8465118463195738], [(21, 0), 0.8233328459025817], [(22, 0), 0.847297903867943], [(23, 0), 0.8320384949545488], [(24, 0), 0.8431920737347273], [(25, 0), 0.8427446304942212]]

Sorted List of Training Scores: 
[[(24, 0), 0.8577442843778169], [(25, 0), 0.8567051731498102], [(22, 0), 0.8562235854216906], [(20, 0), 0.854189202105079], [(23, 0), 0.8504823856369275], [(21, 0), 0.8385319816897608]]

Sorted List of Test Accuracies: 
[[(22, 0), 0.847297903867943], [(20, 0), 0.8465118463195738], [(24, 0), 0.8431920737347273], [(25, 0), 0.8427446304942212], [(23, 0), 0.8320384949545488], [(21, 0), 0.8233328459025817]]

Elapsed Seconds: 13670.7543001
Total Time Elapsed: 03:47:50
