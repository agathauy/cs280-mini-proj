# ann_exit_3_3
['station', 'month', 'day', 'day_of_the_week', 'hour', 'num_trains', 'avg_air_pressure', 'temperature', 'holiday_type', 'payday_type']
# Num of nodes: 20, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:01:44, 104.697618008
Training set score: 0.819279
Training set loss: 0.362584
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 224
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_0_20_0.pkl

Elapsed Seconds: 106.444972992
Total Time Elapsed: 00:01:46
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.71      0.62      0.66     15422
         2.0       0.84      0.89      0.87     35970

   micro avg       0.81      0.81      0.81     51392
   macro avg       0.78      0.75      0.76     51392
weighted avg       0.80      0.81      0.81     51392


Accuracy: 0.810184464508
## 1-kth fold --------------------------------
Training Time Elapsed: 00:01:53, 113.931818008
Training set score: 0.824392
Training set loss: 0.367781
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 239
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_1_20_0.pkl

Elapsed Seconds: 115.635392904
Total Time Elapsed: 00:01:55
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.73      0.67      0.69     15422
         2.0       0.86      0.89      0.88     35970

   micro avg       0.82      0.82      0.82     51392
   macro avg       0.79      0.78      0.79     51392
weighted avg       0.82      0.82      0.82     51392


Accuracy: 0.824019302615
## 2-kth fold --------------------------------
Training Time Elapsed: 00:02:17, 137.643228054
Training set score: 0.825915
Training set loss: 0.363384
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 292
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_2_20_0.pkl

Elapsed Seconds: 139.326004028
Total Time Elapsed: 00:02:19
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.72      0.65      0.68     15422
         2.0       0.86      0.89      0.87     35970

   micro avg       0.82      0.82      0.82     51392
   macro avg       0.79      0.77      0.78     51392
weighted avg       0.82      0.82      0.82     51392


Accuracy: 0.820730853051
## 3-kth fold --------------------------------
Training Time Elapsed: 00:01:58, 118.891732931
Training set score: 0.818940
Training set loss: 0.362836
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 251
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_3_20_0.pkl

Elapsed Seconds: 120.574997187
Total Time Elapsed: 00:02:00
Features in training dataset (80%):
Count: (205567,)
Counter({2.0: 143879, 0.0: 61688})
Features in test dataset (20%):
Count: (51391,)
Counter({2.0: 35969, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.70      0.67      0.68     15422
         2.0       0.86      0.88      0.87     35969

   micro avg       0.81      0.81      0.81     51391
   macro avg       0.78      0.77      0.77     51391
weighted avg       0.81      0.81      0.81     51391


Accuracy: 0.813119028624
## 4-kth fold --------------------------------
Training Time Elapsed: 00:01:20, 80.9787769318
Training set score: 0.816848
Training set loss: 0.372713
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 164
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_4_20_0.pkl

Elapsed Seconds: 82.6741600037
Total Time Elapsed: 00:01:22
Features in training dataset (80%):
Count: (205567,)
Counter({2.0: 143879, 0.0: 61688})
Features in test dataset (20%):
Count: (51391,)
Counter({2.0: 35969, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.72      0.59      0.65     15422
         2.0       0.84      0.90      0.87     35969

   micro avg       0.81      0.81      0.81     51391
   macro avg       0.78      0.75      0.76     51391
weighted avg       0.80      0.81      0.80     51391


Accuracy: 0.809986184351
Sorted List of Temp Training Scores: 
[]

Sorted List of Temp Test Accuracies: 
[]

Average Training Accuracy for: (20, 0)
0.821074851106

Average Test Accuracy for: (20, 0)
0.81560796663

Elapsed Seconds: 565.956386089
Total Time Elapsed: 00:09:25
# Num of nodes: 21, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:04:14, 254.070909023
Training set score: 0.902839
Training set loss: 0.257470
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 500
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_0_21_0.pkl

Elapsed Seconds: 255.745979071
Total Time Elapsed: 00:04:15
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.87      0.77      0.82     15422
         2.0       0.91      0.95      0.93     35970

   micro avg       0.90      0.90      0.90     51392
   macro avg       0.89      0.86      0.87     51392
weighted avg       0.90      0.90      0.90     51392


Accuracy: 0.897746731009
## 1-kth fold --------------------------------
Training Time Elapsed: 00:02:03, 123.183551788
Training set score: 0.825516
Training set loss: 0.363550
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 241
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_1_21_0.pkl

Elapsed Seconds: 125.104699135
Total Time Elapsed: 00:02:05
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.73      0.67      0.70     15422
         2.0       0.86      0.89      0.88     35970

   micro avg       0.83      0.83      0.83     51392
   macro avg       0.80      0.78      0.79     51392
weighted avg       0.82      0.83      0.82     51392


Accuracy: 0.826081880448
## 2-kth fold --------------------------------
Training Time Elapsed: 00:02:53, 173.841472864
Training set score: 0.834953
Training set loss: 0.349785
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 341
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_2_21_0.pkl

Elapsed Seconds: 176.024667025
Total Time Elapsed: 00:02:56
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.74      0.66      0.70     15422
         2.0       0.86      0.90      0.88     35970

   micro avg       0.83      0.83      0.83     51392
   macro avg       0.80      0.78      0.79     51392
weighted avg       0.82      0.83      0.82     51392


Accuracy: 0.827424501868
## 3-kth fold --------------------------------
Training Time Elapsed: 00:03:19, 199.10912919
Training set score: 0.885624
Training set loss: 0.259843
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 390
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_3_21_0.pkl

Elapsed Seconds: 200.76693511
Total Time Elapsed: 00:03:20
Features in training dataset (80%):
Count: (205567,)
Counter({2.0: 143879, 0.0: 61688})
Features in test dataset (20%):
Count: (51391,)
Counter({2.0: 35969, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.79      0.79      0.79     15422
         2.0       0.91      0.91      0.91     35969

   micro avg       0.87      0.87      0.87     51391
   macro avg       0.85      0.85      0.85     51391
weighted avg       0.87      0.87      0.87     51391


Accuracy: 0.871864723395
## 4-kth fold --------------------------------
Training Time Elapsed: 00:01:53, 113.943794012
Training set score: 0.820793
Training set loss: 0.369486
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 225
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_4_21_0.pkl

Elapsed Seconds: 115.914304018
Total Time Elapsed: 00:01:55
Features in training dataset (80%):
Count: (205567,)
Counter({2.0: 143879, 0.0: 61688})
Features in test dataset (20%):
Count: (51391,)
Counter({2.0: 35969, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.74      0.60      0.66     15422
         2.0       0.84      0.91      0.87     35969

   micro avg       0.82      0.82      0.82     51391
   macro avg       0.79      0.76      0.77     51391
weighted avg       0.81      0.82      0.81     51391


Accuracy: 0.816991301979
Sorted List of Temp Training Scores: 
[[(20, 0), 0.821074851105878]]

Sorted List of Temp Test Accuracies: 
[[(20, 0), 0.8156079666298762]]

Average Training Accuracy for: (21, 0)
0.853945004118

Average Test Accuracy for: (21, 0)
0.84802182774

Elapsed Seconds: 875.106665134
Total Time Elapsed: 00:14:35
# Num of nodes: 22, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:38:22, 2302.87372208
Training set score: 0.832297
Training set loss: 0.354374
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 265
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_0_22_0.pkl

Elapsed Seconds: 2304.55191803
Total Time Elapsed: 00:38:24
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.72      0.67      0.69     15422
         2.0       0.86      0.89      0.87     35970

   micro avg       0.82      0.82      0.82     51392
   macro avg       0.79      0.78      0.78     51392
weighted avg       0.82      0.82      0.82     51392


Accuracy: 0.822073474471
## 1-kth fold --------------------------------
Training Time Elapsed: 00:04:16, 256.863590956
Training set score: 0.914879
Training set loss: 0.227270
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 500
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_1_22_0.pkl

Elapsed Seconds: 258.688961029
Total Time Elapsed: 00:04:18
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.88      0.84      0.86     15422
         2.0       0.93      0.95      0.94     35970

   micro avg       0.92      0.92      0.92     51392
   macro avg       0.91      0.90      0.90     51392
weighted avg       0.92      0.92      0.92     51392


Accuracy: 0.919034090909
## 2-kth fold --------------------------------
Training Time Elapsed: 00:02:22, 142.632642031
Training set score: 0.875305
Training set loss: 0.284499
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 284
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_2_22_0.pkl

Elapsed Seconds: 144.234432936
Total Time Elapsed: 00:02:24
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.79      0.77      0.78     15422
         2.0       0.90      0.91      0.91     35970

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.85      0.84      0.84     51392
weighted avg       0.87      0.87      0.87     51392


Accuracy: 0.868345267746
## 3-kth fold --------------------------------
Training Time Elapsed: 00:03:05, 185.084021807
Training set score: 0.885487
Training set loss: 0.268248
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 375
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_3_22_0.pkl

Elapsed Seconds: 186.711354017
Total Time Elapsed: 00:03:06
Features in training dataset (80%):
Count: (205567,)
Counter({2.0: 143879, 0.0: 61688})
Features in test dataset (20%):
Count: (51391,)
Counter({2.0: 35969, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.80      0.77      0.78     15422
         2.0       0.90      0.92      0.91     35969

   micro avg       0.87      0.87      0.87     51391
   macro avg       0.85      0.84      0.85     51391
weighted avg       0.87      0.87      0.87     51391


Accuracy: 0.873324122901
## 4-kth fold --------------------------------
Training Time Elapsed: 00:29:45, 1785.26111698
Training set score: 0.904542
Training set loss: 0.238050
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 500
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_4_22_0.pkl

Elapsed Seconds: 1786.96219015
Total Time Elapsed: 00:29:46
Features in training dataset (80%):
Count: (205567,)
Counter({2.0: 143879, 0.0: 61688})
Features in test dataset (20%):
Count: (51391,)
Counter({2.0: 35969, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.88      0.77      0.82     15422
         2.0       0.91      0.96      0.93     35969

   micro avg       0.90      0.90      0.90     51391
   macro avg       0.89      0.86      0.88     51391
weighted avg       0.90      0.90      0.90     51391


Accuracy: 0.90099433753
Sorted List of Temp Training Scores: 
[[(21, 0), 0.8539450041184964], [(20, 0), 0.821074851105878]]

Sorted List of Temp Test Accuracies: 
[[(21, 0), 0.8480218277398247], [(20, 0), 0.8156079666298762]]

Average Training Accuracy for: (22, 0)
0.882502174455

Average Test Accuracy for: (22, 0)
0.876754258711

Elapsed Seconds: 4682.41687989
Total Time Elapsed: 01:18:02
# Num of nodes: 23, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:04:09, 249.043689013
Training set score: 0.918630
Training set loss: 0.224947
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 500
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_0_23_0.pkl

Elapsed Seconds: 250.721593857
Total Time Elapsed: 00:04:10
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.79      0.84     15422
         2.0       0.91      0.96      0.94     35970

   micro avg       0.91      0.91      0.91     51392
   macro avg       0.91      0.88      0.89     51392
weighted avg       0.91      0.91      0.91     51392


Accuracy: 0.911834526775
## 1-kth fold --------------------------------
Training Time Elapsed: 00:02:53, 173.000868797
Training set score: 0.888600
Training set loss: 0.267909
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 350
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_1_23_0.pkl

Elapsed Seconds: 174.612436056
Total Time Elapsed: 00:02:54
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.86      0.77      0.81     15422
         2.0       0.90      0.95      0.92     35970

   micro avg       0.89      0.89      0.89     51392
   macro avg       0.88      0.86      0.87     51392
weighted avg       0.89      0.89      0.89     51392


Accuracy: 0.89116983188
## 2-kth fold --------------------------------
Training Time Elapsed: 00:02:40, 160.138588905
Training set score: 0.893825
Training set loss: 0.257873
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 325
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_2_23_0.pkl

Elapsed Seconds: 161.879061937
Total Time Elapsed: 00:02:41
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.84      0.77      0.80     15422
         2.0       0.90      0.94      0.92     35970

   micro avg       0.89      0.89      0.89     51392
   macro avg       0.87      0.85      0.86     51392
weighted avg       0.89      0.89      0.89     51392


Accuracy: 0.888056506849
## 3-kth fold --------------------------------
Training Time Elapsed: 00:02:37, 157.505659819
Training set score: 0.895212
Training set loss: 0.258385
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 318
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_3_23_0.pkl

Elapsed Seconds: 159.151501179
Total Time Elapsed: 00:02:39
Features in training dataset (80%):
Count: (205567,)
Counter({2.0: 143879, 0.0: 61688})
Features in test dataset (20%):
Count: (51391,)
Counter({2.0: 35969, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.80      0.80      0.80     15422
         2.0       0.91      0.91      0.91     35969

   micro avg       0.88      0.88      0.88     51391
   macro avg       0.86      0.86      0.86     51391
weighted avg       0.88      0.88      0.88     51391


Accuracy: 0.879103344944
## 4-kth fold --------------------------------
Training Time Elapsed: 00:03:01, 181.959779024
Training set score: 0.890975
Training set loss: 0.258118
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 369
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_4_23_0.pkl

Elapsed Seconds: 183.598402023
Total Time Elapsed: 00:03:03
Features in training dataset (80%):
Count: (205567,)
Counter({2.0: 143879, 0.0: 61688})
Features in test dataset (20%):
Count: (51391,)
Counter({2.0: 35969, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.87      0.74      0.80     15422
         2.0       0.89      0.95      0.92     35969

   micro avg       0.89      0.89      0.89     51391
   macro avg       0.88      0.84      0.86     51391
weighted avg       0.89      0.89      0.88     51391


Accuracy: 0.887918117958
Sorted List of Temp Training Scores: 
[[(22, 0), 0.8825021744553787], [(21, 0), 0.8539450041184964], [(20, 0), 0.821074851105878]]

Sorted List of Temp Test Accuracies: 
[[(22, 0), 0.8767542587113185], [(21, 0), 0.8480218277398247], [(20, 0), 0.8156079666298762]]

Average Training Accuracy for: (23, 0)
0.897448229584

Average Test Accuracy for: (23, 0)
0.891616465681

Elapsed Seconds: 931.3020401
Total Time Elapsed: 00:15:31
# Num of nodes: 24, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:02:49, 169.219929934
Training set score: 0.889588
Training set loss: 0.259272
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 342
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_0_24_0.pkl

Elapsed Seconds: 170.854604959
Total Time Elapsed: 00:02:50
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.84      0.75      0.79     15422
         2.0       0.90      0.94      0.92     35970

   micro avg       0.88      0.88      0.88     51392
   macro avg       0.87      0.84      0.85     51392
weighted avg       0.88      0.88      0.88     51392


Accuracy: 0.879611612702
## 1-kth fold --------------------------------
Training Time Elapsed: 00:04:08, 248.181138039
Training set score: 0.919958
Training set loss: 0.227118
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 500
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_1_24_0.pkl

Elapsed Seconds: 249.825407982
Total Time Elapsed: 00:04:09
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.85      0.87     15422
         2.0       0.94      0.96      0.95     35970

   micro avg       0.92      0.92      0.92     51392
   macro avg       0.91      0.90      0.91     51392
weighted avg       0.92      0.92      0.92     51392


Accuracy: 0.924229452055
## 2-kth fold --------------------------------
Training Time Elapsed: 00:19:45, 1185.16145802
Training set score: 0.929375
Training set loss: 0.205418
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 500
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_2_24_0.pkl

Elapsed Seconds: 1186.84192014
Total Time Elapsed: 00:19:46
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.82      0.86     15422
         2.0       0.92      0.96      0.94     35970

   micro avg       0.92      0.92      0.92     51392
   macro avg       0.91      0.89      0.90     51392
weighted avg       0.92      0.92      0.92     51392


Accuracy: 0.918139009963
## 3-kth fold --------------------------------
Training Time Elapsed: 00:02:13, 133.553164959
Training set score: 0.893227
Training set loss: 0.257113
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 269
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_3_24_0.pkl

Elapsed Seconds: 135.288376093
Total Time Elapsed: 00:02:15
Features in training dataset (80%):
Count: (205567,)
Counter({2.0: 143879, 0.0: 61688})
Features in test dataset (20%):
Count: (51391,)
Counter({2.0: 35969, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.79      0.80      0.79     15422
         2.0       0.91      0.91      0.91     35969

   micro avg       0.87      0.87      0.87     51391
   macro avg       0.85      0.85      0.85     51391
weighted avg       0.87      0.87      0.87     51391


Accuracy: 0.873946800023
## 4-kth fold --------------------------------
Training Time Elapsed: 00:03:55, 235.347511053
Training set score: 0.915196
Training set loss: 0.226191
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 464
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_4_24_0.pkl

Elapsed Seconds: 237.055729151
Total Time Elapsed: 00:03:57
Features in training dataset (80%):
Count: (205567,)
Counter({2.0: 143879, 0.0: 61688})
Features in test dataset (20%):
Count: (51391,)
Counter({2.0: 35969, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.81      0.85     15422
         2.0       0.92      0.96      0.94     35969

   micro avg       0.91      0.91      0.91     51391
   macro avg       0.91      0.88      0.89     51391
weighted avg       0.91      0.91      0.91     51391


Accuracy: 0.912864120177
Sorted List of Temp Training Scores: 
[[(23, 0), 0.8974482295841737], [(22, 0), 0.8825021744553787], [(21, 0), 0.8539450041184964], [(20, 0), 0.821074851105878]]

Sorted List of Temp Test Accuracies: 
[[(23, 0), 0.8916164656812846], [(22, 0), 0.8767542587113185], [(21, 0), 0.8480218277398247], [(20, 0), 0.8156079666298762]]

Average Training Accuracy for: (24, 0)
0.909468678261

Average Test Accuracy for: (24, 0)
0.901758198984

Elapsed Seconds: 1981.1224792
Total Time Elapsed: 00:33:01
# Num of nodes: 25, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:02:56, 176.882914066
Training set score: 0.893625
Training set loss: 0.252210
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 351
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_0_25_0.pkl

Elapsed Seconds: 178.563555956
Total Time Elapsed: 00:02:58
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.84      0.75      0.79     15422
         2.0       0.90      0.94      0.92     35970

   micro avg       0.88      0.88      0.88     51392
   macro avg       0.87      0.85      0.85     51392
weighted avg       0.88      0.88      0.88     51392


Accuracy: 0.881674190535
## 1-kth fold --------------------------------
Training Time Elapsed: 00:02:30, 150.609411001
Training set score: 0.893377
Training set loss: 0.257569
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 297
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_1_25_0.pkl

Elapsed Seconds: 152.190086842
Total Time Elapsed: 00:02:32
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.85      0.80      0.82     15422
         2.0       0.92      0.94      0.93     35970

   micro avg       0.90      0.90      0.90     51392
   macro avg       0.88      0.87      0.87     51392
weighted avg       0.89      0.90      0.90     51392


Accuracy: 0.896112235367
## 2-kth fold --------------------------------
Training Time Elapsed: 00:02:35, 155.310548067
Training set score: 0.894462
Training set loss: 0.248461
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 299
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_2_25_0.pkl

Elapsed Seconds: 157.049057007
Total Time Elapsed: 00:02:37
Features in training dataset (80%):
Count: (205566,)
Counter({2.0: 143878, 0.0: 61688})
Features in test dataset (20%):
Count: (51392,)
Counter({2.0: 35970, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.85      0.77      0.81     15422
         2.0       0.90      0.94      0.92     35970

   micro avg       0.89      0.89      0.89     51392
   macro avg       0.88      0.85      0.86     51392
weighted avg       0.89      0.89      0.89     51392


Accuracy: 0.889885585305
## 3-kth fold --------------------------------
Training Time Elapsed: 00:02:43, 163.195034981
Training set score: 0.895110
Training set loss: 0.249836
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 320
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_3_25_0.pkl

Elapsed Seconds: 164.913041115
Total Time Elapsed: 00:02:44
Features in training dataset (80%):
Count: (205567,)
Counter({2.0: 143879, 0.0: 61688})
Features in test dataset (20%):
Count: (51391,)
Counter({2.0: 35969, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.78      0.80      0.79     15422
         2.0       0.92      0.90      0.91     35969

   micro avg       0.87      0.87      0.87     51391
   macro avg       0.85      0.85      0.85     51391
weighted avg       0.88      0.87      0.87     51391


Accuracy: 0.874472183845
## 4-kth fold --------------------------------
Training Time Elapsed: 00:02:33, 153.647210121
Training set score: 0.889155
Training set loss: 0.259116
MLP # layers: 3
MLP # outputs: 1
MLP # iterations: 302
MLP out_activation_: logistic
MLP classes: [0. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_0/ann_exit_3_3/ann_4_25_0.pkl

Elapsed Seconds: 155.379822969
Total Time Elapsed: 00:02:35
Features in training dataset (80%):
Count: (205567,)
Counter({2.0: 143879, 0.0: 61688})
Features in test dataset (20%):
Count: (51391,)
Counter({2.0: 35969, 0.0: 15422})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.85      0.76      0.80     15422
         2.0       0.90      0.94      0.92     35969

   micro avg       0.89      0.89      0.89     51391
   macro avg       0.87      0.85      0.86     51391
weighted avg       0.88      0.89      0.88     51391


Accuracy: 0.886614387733
Sorted List of Temp Training Scores: 
[[(24, 0), 0.9094686782614263], [(23, 0), 0.8974482295841737], [(22, 0), 0.8825021744553787], [(21, 0), 0.8539450041184964], [(20, 0), 0.821074851105878]]

Sorted List of Temp Test Accuracies: 
[[(24, 0), 0.9017581989839671], [(23, 0), 0.8916164656812846], [(22, 0), 0.8767542587113185], [(21, 0), 0.8480218277398247], [(20, 0), 0.8156079666298762]]

Average Training Accuracy for: (25, 0)
0.893145963569

Average Test Accuracy for: (25, 0)
0.885751716557

Elapsed Seconds: 809.575130939
Total Time Elapsed: 00:13:29
List of Training Scores: 
[[(20, 0), 0.821074851105878], [(21, 0), 0.8539450041184964], [(22, 0), 0.8825021744553787], [(23, 0), 0.8974482295841737], [(24, 0), 0.9094686782614263], [(25, 0), 0.8931459635688936]]

List of Test Accuracies: 
[[(20, 0), 0.8156079666298762], [(21, 0), 0.8480218277398247], [(22, 0), 0.8767542587113185], [(23, 0), 0.8916164656812846], [(24, 0), 0.9017581989839671], [(25, 0), 0.8857517165573302]]

Sorted List of Training Scores: 
[[(24, 0), 0.9094686782614263], [(23, 0), 0.8974482295841737], [(25, 0), 0.8931459635688936], [(22, 0), 0.8825021744553787], [(21, 0), 0.8539450041184964], [(20, 0), 0.821074851105878]]

Sorted List of Test Accuracies: 
[[(24, 0), 0.9017581989839671], [(23, 0), 0.8916164656812846], [(25, 0), 0.8857517165573302], [(22, 0), 0.8767542587113185], [(21, 0), 0.8480218277398247], [(20, 0), 0.8156079666298762]]

Elapsed Seconds: 9845.48013902
Total Time Elapsed: 02:44:05
