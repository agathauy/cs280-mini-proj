# ann_entry_3_8
['station', 'month', 'day', 'day_of_the_week', 'hour', 'traffic_type']
# Num of nodes: 20, 20 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:07:24, 444.091518164
Training set score: 0.892443
Training set loss: 0.302491
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 494
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_0_20_20.pkl

Elapsed Seconds: 446.864398003
Total Time Elapsed: 00:07:26
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.95      0.81      0.87     15427
         1.0       0.88      0.90      0.89     25682
         2.0       0.80      0.95      0.87     10283

   micro avg       0.88      0.88      0.88     51392
   macro avg       0.88      0.88      0.88     51392
weighted avg       0.89      0.88      0.88     51392


Accuracy: 0.881226650062
## 1-kth fold --------------------------------
Training Time Elapsed: 00:07:33, 453.958063126
Training set score: 0.891013
Training set loss: 0.305541
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_1_20_20.pkl

Elapsed Seconds: 456.008096933
Total Time Elapsed: 00:07:36
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.93      0.89      0.91     15427
         1.0       0.89      0.93      0.91     25682
         2.0       0.90      0.88      0.89     10283

   micro avg       0.91      0.91      0.91     51392
   macro avg       0.91      0.90      0.90     51392
weighted avg       0.91      0.91      0.91     51392


Accuracy: 0.905899750934
## 2-kth fold --------------------------------
Training Time Elapsed: 00:07:26, 446.909116983
Training set score: 0.889923
Training set loss: 0.311736
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_2_20_20.pkl

Elapsed Seconds: 449.394862175
Total Time Elapsed: 00:07:29
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.96      0.80      0.87     15427
         1.0       0.86      0.94      0.90     25682
         2.0       0.85      0.89      0.87     10283

   micro avg       0.88      0.88      0.88     51392
   macro avg       0.89      0.87      0.88     51392
weighted avg       0.89      0.88      0.88     51392


Accuracy: 0.884067559153
## 3-kth fold --------------------------------
Training Time Elapsed: 00:07:06, 426.819874048
Training set score: 0.905452
Training set loss: 0.265703
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_3_20_20.pkl

Elapsed Seconds: 1121.10185599
Total Time Elapsed: 00:18:41
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.84      0.87      0.86     15426
         1.0       0.83      0.89      0.86     25682
         2.0       0.91      0.69      0.79     10283

   micro avg       0.85      0.85      0.85     51391
   macro avg       0.86      0.82      0.83     51391
weighted avg       0.85      0.85      0.84     51391


Accuracy: 0.845673366932
## 4-kth fold --------------------------------
Training Time Elapsed: 00:07:15, 435.586276054
Training set score: 0.886290
Training set loss: 0.309297
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_4_20_20.pkl

Elapsed Seconds: 437.527472019
Total Time Elapsed: 00:07:17
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.93      0.84      0.88     15426
         1.0       0.87      0.89      0.88     25682
         2.0       0.83      0.89      0.86     10283

   micro avg       0.88      0.88      0.88     51391
   macro avg       0.88      0.88      0.88     51391
weighted avg       0.88      0.88      0.88     51391


Accuracy: 0.877974742659
Sorted List of Temp Training Scores: 
[]

Sorted List of Temp Test Accuracies: 
[]

Average Training Accuracy for: (20, 20)
0.893024340852

Average Test Accuracy for: (20, 20)
0.878968413948

Elapsed Seconds: 2913.00507092
Total Time Elapsed: 00:48:33
# Num of nodes: 21, 21 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:07:31, 451.324902058
Training set score: 0.902907
Training set loss: 0.285130
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_0_21_21.pkl

Elapsed Seconds: 453.242329121
Total Time Elapsed: 00:07:33
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.94      0.84      0.89     15427
         1.0       0.89      0.93      0.91     25682
         2.0       0.86      0.91      0.89     10283

   micro avg       0.90      0.90      0.90     51392
   macro avg       0.90      0.89      0.89     51392
weighted avg       0.90      0.90      0.90     51392


Accuracy: 0.895937110834
## 1-kth fold --------------------------------
Training Time Elapsed: 00:07:02, 422.682454824
Training set score: 0.898972
Training set loss: 0.297598
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_1_21_21.pkl

Elapsed Seconds: 424.735164881
Total Time Elapsed: 00:07:04
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.93      0.89      0.91     15427
         1.0       0.90      0.92      0.91     25682
         2.0       0.90      0.89      0.89     10283

   micro avg       0.91      0.91      0.91     51392
   macro avg       0.91      0.90      0.90     51392
weighted avg       0.91      0.91      0.91     51392


Accuracy: 0.905335460772
## 2-kth fold --------------------------------
Training Time Elapsed: 00:33:46, 2026.88998008
Training set score: 0.900246
Training set loss: 0.296027
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_2_21_21.pkl

Elapsed Seconds: 2028.76474309
Total Time Elapsed: 00:33:48
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.95      0.83      0.89     15427
         1.0       0.88      0.93      0.91     25682
         2.0       0.87      0.90      0.89     10283

   micro avg       0.90      0.90      0.90     51392
   macro avg       0.90      0.89      0.89     51392
weighted avg       0.90      0.90      0.90     51392


Accuracy: 0.897415940224
## 3-kth fold --------------------------------
Training Time Elapsed: 00:07:26, 446.705514908
Training set score: 0.899337
Training set loss: 0.278908
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_3_21_21.pkl

Elapsed Seconds: 448.821135998
Total Time Elapsed: 00:07:28
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.85      0.88     15426
         1.0       0.86      0.91      0.88     25682
         2.0       0.86      0.82      0.84     10283

   micro avg       0.87      0.87      0.87     51391
   macro avg       0.88      0.86      0.87     51391
weighted avg       0.87      0.87      0.87     51391


Accuracy: 0.873830048063
## 4-kth fold --------------------------------
Training Time Elapsed: 00:07:26, 446.52632618
Training set score: 0.897771
Training set loss: 0.292394
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_4_21_21.pkl

Elapsed Seconds: 448.606899977
Total Time Elapsed: 00:07:28
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.92      0.87      0.89     15426
         1.0       0.89      0.90      0.89     25682
         2.0       0.85      0.90      0.87     10283

   micro avg       0.89      0.89      0.89     51391
   macro avg       0.88      0.89      0.88     51391
weighted avg       0.89      0.89      0.89     51391


Accuracy: 0.887198147536
Sorted List of Temp Training Scores: 
[[(20, 20), 0.8930243408522177]]

Sorted List of Temp Test Accuracies: 
[[(20, 20), 0.8789684139481226]]

Average Training Accuracy for: (21, 21)
0.89984647548

Average Test Accuracy for: (21, 21)
0.891943341486

Elapsed Seconds: 3805.76230693
Total Time Elapsed: 01:03:25
# Num of nodes: 22, 22 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:18:26, 1106.00237203
Training set score: 0.898874
Training set loss: 0.289409
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_0_22_22.pkl

Elapsed Seconds: 1108.10555506
Total Time Elapsed: 00:18:28
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.94      0.83      0.88     15427
         1.0       0.87      0.93      0.90     25682
         2.0       0.88      0.89      0.89     10283

   micro avg       0.89      0.89      0.89     51392
   macro avg       0.90      0.88      0.89     51392
weighted avg       0.89      0.89      0.89     51392


Accuracy: 0.891422789539
## 1-kth fold --------------------------------
Training Time Elapsed: 00:07:54, 474.874778986
Training set score: 0.900810
Training set loss: 0.272811
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_1_22_22.pkl

Elapsed Seconds: 476.969154835
Total Time Elapsed: 00:07:56
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.92      0.89      0.90     15427
         1.0       0.90      0.92      0.91     25682
         2.0       0.89      0.91      0.90     10283

   micro avg       0.91      0.91      0.91     51392
   macro avg       0.90      0.90      0.90     51392
weighted avg       0.91      0.91      0.91     51392


Accuracy: 0.905568960149
## 2-kth fold --------------------------------
Training Time Elapsed: 00:07:58, 478.527014971
Training set score: 0.895873
Training set loss: 0.290650
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_2_22_22.pkl

Elapsed Seconds: 480.813686132
Total Time Elapsed: 00:08:00
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.93      0.83      0.88     15427
         1.0       0.87      0.93      0.90     25682
         2.0       0.88      0.87      0.88     10283

   micro avg       0.89      0.89      0.89     51392
   macro avg       0.90      0.88      0.89     51392
weighted avg       0.89      0.89      0.89     51392


Accuracy: 0.889866127024
## 3-kth fold --------------------------------
Training Time Elapsed: 00:07:59, 479.959815025
Training set score: 0.902912
Training set loss: 0.267910
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_3_22_22.pkl

Elapsed Seconds: 482.10285306
Total Time Elapsed: 00:08:02
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.86      0.86      0.86     15426
         1.0       0.84      0.90      0.87     25682
         2.0       0.88      0.75      0.81     10283

   micro avg       0.86      0.86      0.86     51391
   macro avg       0.86      0.84      0.85     51391
weighted avg       0.86      0.86      0.85     51391


Accuracy: 0.85530540367
## 4-kth fold --------------------------------
Training Time Elapsed: 00:07:58, 478.197427988
Training set score: 0.900368
Training set loss: 0.272177
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_4_22_22.pkl

Elapsed Seconds: 480.500638962
Total Time Elapsed: 00:08:00
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.94      0.79      0.85     15426
         1.0       0.86      0.87      0.86     25682
         2.0       0.76      0.91      0.83     10283

   micro avg       0.85      0.85      0.85     51391
   macro avg       0.85      0.86      0.85     51391
weighted avg       0.86      0.85      0.85     51391


Accuracy: 0.854060049425
Sorted List of Temp Training Scores: 
[[(21, 21), 0.8998464754798825], [(20, 20), 0.8930243408522177]]

Sorted List of Temp Test Accuracies: 
[[(21, 21), 0.8919433414858172], [(20, 20), 0.8789684139481226]]

Average Training Accuracy for: (22, 22)
0.899767662667

Average Test Accuracy for: (22, 22)
0.879244665961

Elapsed Seconds: 3030.171067
Total Time Elapsed: 00:50:30
# Num of nodes: 23, 23 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:07:38, 458.802830935
Training set score: 0.922657
Training set loss: 0.219703
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_0_23_23.pkl

Elapsed Seconds: 460.955425024
Total Time Elapsed: 00:07:40
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.93      0.89      0.91     15427
         1.0       0.91      0.93      0.92     25682
         2.0       0.91      0.90      0.90     10283

   micro avg       0.91      0.91      0.91     51392
   macro avg       0.92      0.91      0.91     51392
weighted avg       0.91      0.91      0.91     51392


Accuracy: 0.914208437111
## 1-kth fold --------------------------------
Training Time Elapsed: 00:06:56, 416.288455009
Training set score: 0.916630
Training set loss: 0.232592
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_1_23_23.pkl

Elapsed Seconds: 418.102473021
Total Time Elapsed: 00:06:58
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.94      0.91      0.92     15427
         1.0       0.92      0.93      0.92     25682
         2.0       0.91      0.91      0.91     10283

   micro avg       0.92      0.92      0.92     51392
   macro avg       0.92      0.92      0.92     51392
weighted avg       0.92      0.92      0.92     51392


Accuracy: 0.920688044832
## 2-kth fold --------------------------------
Training Time Elapsed: 00:06:51, 411.62336421
Training set score: 0.919656
Training set loss: 0.237262
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_2_23_23.pkl

Elapsed Seconds: 414.459981918
Total Time Elapsed: 00:06:54
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.95      0.86      0.90     15427
         1.0       0.89      0.94      0.92     25682
         2.0       0.89      0.90      0.90     10283

   micro avg       0.91      0.91      0.91     51392
   macro avg       0.91      0.90      0.90     51392
weighted avg       0.91      0.91      0.91     51392


Accuracy: 0.907145080946
## 3-kth fold --------------------------------
Training Time Elapsed: 00:06:54, 414.373460054
Training set score: 0.921665
Training set loss: 0.214863
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_3_23_23.pkl

Elapsed Seconds: 417.000069857
Total Time Elapsed: 00:06:57
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.78      0.91      0.84     15426
         1.0       0.87      0.86      0.86     25682
         2.0       0.91      0.72      0.80     10283

   micro avg       0.84      0.84      0.84     51391
   macro avg       0.85      0.83      0.83     51391
weighted avg       0.85      0.84      0.84     51391


Accuracy: 0.843396703703
## 4-kth fold --------------------------------
Training Time Elapsed: 00:06:50, 410.846763134
Training set score: 0.926647
Training set loss: 0.203829
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_4_23_23.pkl

Elapsed Seconds: 412.931696892
Total Time Elapsed: 00:06:52
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.87      0.89     15426
         1.0       0.90      0.89      0.89     25682
         2.0       0.85      0.90      0.87     10283

   micro avg       0.89      0.89      0.89     51391
   macro avg       0.88      0.89      0.88     51391
weighted avg       0.89      0.89      0.89     51391


Accuracy: 0.886341966492
Sorted List of Temp Training Scores: 
[[(21, 21), 0.8998464754798825], [(22, 22), 0.8997676626672872], [(20, 20), 0.8930243408522177]]

Sorted List of Temp Test Accuracies: 
[[(21, 21), 0.8919433414858172], [(22, 22), 0.8792446659614457], [(20, 20), 0.8789684139481226]]

Average Training Accuracy for: (23, 23)
0.921451165745

Average Test Accuracy for: (23, 23)
0.894356046617

Elapsed Seconds: 2125.60874701
Total Time Elapsed: 00:35:25
# Num of nodes: 24, 24 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:07:01, 421.774224997
Training set score: 0.918902
Training set loss: 0.234669
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_0_24_24.pkl

Elapsed Seconds: 423.902303934
Total Time Elapsed: 00:07:03
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.93      0.88      0.91     15427
         1.0       0.90      0.93      0.92     25682
         2.0       0.89      0.91      0.90     10283

   micro avg       0.91      0.91      0.91     51392
   macro avg       0.91      0.91      0.91     51392
weighted avg       0.91      0.91      0.91     51392


Accuracy: 0.910394613948
## 1-kth fold --------------------------------
Training Time Elapsed: 00:07:17, 437.073904037
Training set score: 0.919043
Training set loss: 0.228956
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_1_24_24.pkl

Elapsed Seconds: 439.294737101
Total Time Elapsed: 00:07:19
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.93      0.92      0.93     15427
         1.0       0.92      0.93      0.92     25682
         2.0       0.92      0.90      0.91     10283

   micro avg       0.92      0.92      0.92     51392
   macro avg       0.92      0.92      0.92     51392
weighted avg       0.92      0.92      0.92     51392


Accuracy: 0.921777708593
## 2-kth fold --------------------------------
Training Time Elapsed: 00:06:51, 411.997845888
Training set score: 0.920434
Training set loss: 0.229950
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_2_24_24.pkl

Elapsed Seconds: 414.195927858
Total Time Elapsed: 00:06:54
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.94      0.88      0.91     15427
         1.0       0.91      0.93      0.92     25682
         2.0       0.89      0.91      0.90     10283

   micro avg       0.91      0.91      0.91     51392
   macro avg       0.91      0.91      0.91     51392
weighted avg       0.91      0.91      0.91     51392


Accuracy: 0.911659402242
## 3-kth fold --------------------------------
Training Time Elapsed: 00:07:04, 424.136692047
Training set score: 0.921836
Training set loss: 0.223487
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_3_24_24.pkl

Elapsed Seconds: 426.074615002
Total Time Elapsed: 00:07:06
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.79      0.89      0.84     15426
         1.0       0.83      0.86      0.84     25682
         2.0       0.92      0.65      0.76     10283

   micro avg       0.83      0.83      0.83     51391
   macro avg       0.85      0.80      0.82     51391
weighted avg       0.84      0.83      0.83     51391


Accuracy: 0.828705415345
## 4-kth fold --------------------------------
Training Time Elapsed: 00:07:01, 421.716258049
Training set score: 0.930076
Training set loss: 0.198024
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_4_24_24.pkl

Elapsed Seconds: 423.977066994
Total Time Elapsed: 00:07:03
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.91      0.87      0.89     15426
         1.0       0.90      0.88      0.89     25682
         2.0       0.83      0.92      0.87     10283

   micro avg       0.89      0.89      0.89     51391
   macro avg       0.88      0.89      0.88     51391
weighted avg       0.89      0.89      0.89     51391


Accuracy: 0.886711681034
Sorted List of Temp Training Scores: 
[[(23, 23), 0.9214511657450852], [(21, 21), 0.8998464754798825], [(22, 22), 0.8997676626672872], [(20, 20), 0.8930243408522177]]

Sorted List of Temp Test Accuracies: 
[[(23, 23), 0.8943560466168672], [(21, 21), 0.8919433414858172], [(22, 22), 0.8792446659614457], [(20, 20), 0.8789684139481226]]

Average Training Accuracy for: (24, 24)
0.92205826653

Average Test Accuracy for: (24, 24)
0.891849764232

Elapsed Seconds: 2129.23470402
Total Time Elapsed: 00:35:29
# Num of nodes: 25, 25 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:07:08, 428.993113041
Training set score: 0.917812
Training set loss: 0.229759
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_0_25_25.pkl

Elapsed Seconds: 430.908104181
Total Time Elapsed: 00:07:10
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.96      0.85      0.90     15427
         1.0       0.88      0.95      0.92     25682
         2.0       0.90      0.90      0.90     10283

   micro avg       0.91      0.91      0.91     51392
   macro avg       0.91      0.90      0.90     51392
weighted avg       0.91      0.91      0.91     51392


Accuracy: 0.906736457036
## 1-kth fold --------------------------------
Training Time Elapsed: 00:07:09, 429.582132816
Training set score: 0.912111
Training set loss: 0.242093
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_1_25_25.pkl

Elapsed Seconds: 431.349570036
Total Time Elapsed: 00:07:11
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.96      0.87      0.91     15427
         1.0       0.90      0.94      0.92     25682
         2.0       0.91      0.91      0.91     10283

   micro avg       0.92      0.92      0.92     51392
   macro avg       0.92      0.91      0.91     51392
weighted avg       0.92      0.92      0.91     51392


Accuracy: 0.915025684932
## 2-kth fold --------------------------------
Training Time Elapsed: 00:07:13, 433.751890898
Training set score: 0.914830
Training set loss: 0.237120
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_2_25_25.pkl

Elapsed Seconds: 435.691742897
Total Time Elapsed: 00:07:15
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.96      0.84      0.89     15427
         1.0       0.88      0.95      0.91     25682
         2.0       0.89      0.90      0.89     10283

   micro avg       0.90      0.90      0.90     51392
   macro avg       0.91      0.89      0.90     51392
weighted avg       0.91      0.90      0.90     51392


Accuracy: 0.904265255293
## 3-kth fold --------------------------------
Training Time Elapsed: 00:07:15, 435.190828085
Training set score: 0.920527
Training set loss: 0.228005
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_3_25_25.pkl

Elapsed Seconds: 437.257097006
Total Time Elapsed: 00:07:17
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.85      0.89      0.87     15426
         1.0       0.86      0.89      0.87     25682
         2.0       0.91      0.76      0.83     10283

   micro avg       0.86      0.86      0.86     51391
   macro avg       0.87      0.85      0.86     51391
weighted avg       0.87      0.86      0.86     51391


Accuracy: 0.864412056586
## 4-kth fold --------------------------------
Training Time Elapsed: 00:07:08, 428.207783937
Training set score: 0.918897
Training set loss: 0.231355
MLP # layers: 4
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_20_to_25_s/ann_entry_3_8/ann_4_25_25.pkl

Elapsed Seconds: 431.048797846
Total Time Elapsed: 00:07:11
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.93      0.87      0.90     15426
         1.0       0.90      0.90      0.90     25682
         2.0       0.83      0.93      0.88     10283

   micro avg       0.89      0.89      0.89     51391
   macro avg       0.89      0.90      0.89     51391
weighted avg       0.90      0.89      0.89     51391


Accuracy: 0.89352221206
Sorted List of Temp Training Scores: 
[[(24, 24), 0.9220582665304295], [(23, 23), 0.9214511657450852], [(21, 21), 0.8998464754798825], [(22, 22), 0.8997676626672872], [(20, 20), 0.8930243408522177]]

Sorted List of Temp Test Accuracies: 
[[(23, 23), 0.8943560466168672], [(21, 21), 0.8919433414858172], [(24, 24), 0.8918497642321622], [(22, 22), 0.8792446659614457], [(20, 20), 0.8789684139481226]]

Average Training Accuracy for: (25, 25)
0.916835625128

Average Test Accuracy for: (25, 25)
0.896792333181

Elapsed Seconds: 2168.04340386
Total Time Elapsed: 00:36:08
List of Training Scores: 
[[(20, 20), 0.8930243408522177], [(21, 21), 0.8998464754798825], [(22, 22), 0.8997676626672872], [(23, 23), 0.9214511657450852], [(24, 24), 0.9220582665304295], [(25, 25), 0.91683562512807]]

List of Test Accuracies: 
[[(20, 20), 0.8789684139481226], [(21, 21), 0.8919433414858172], [(22, 22), 0.8792446659614457], [(23, 23), 0.8943560466168672], [(24, 24), 0.8918497642321622], [(25, 25), 0.8967923331813068]]

Sorted List of Training Scores: 
[[(24, 24), 0.9220582665304295], [(23, 23), 0.9214511657450852], [(25, 25), 0.91683562512807], [(21, 21), 0.8998464754798825], [(22, 22), 0.8997676626672872], [(20, 20), 0.8930243408522177]]

Sorted List of Test Accuracies: 
[[(25, 25), 0.8967923331813068], [(23, 23), 0.8943560466168672], [(21, 21), 0.8919433414858172], [(24, 24), 0.8918497642321622], [(22, 22), 0.8792446659614457], [(20, 20), 0.8789684139481226]]

Elapsed Seconds: 16171.825912
Total Time Elapsed: 04:29:31
