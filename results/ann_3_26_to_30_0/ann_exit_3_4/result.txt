# ann_exit_3_4
['station', 'month', 'day_of_the_week', 'hour', 'num_trains', 'avg_air_pressure', 'temperature', 'holiday_type', 'payday_type']
# Num of nodes: 26, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:04:45, 285.815829992
Training set score: 0.874245
Training set loss: 0.350699
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_0_26_0.pkl

Elapsed Seconds: 287.606086969
Total Time Elapsed: 00:04:47
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.91      0.82      0.86     15422
         1.0       0.86      0.88      0.87     25683
         2.0       0.82      0.89      0.85     10287

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.86      0.86      0.86     51392
weighted avg       0.87      0.86      0.86     51392


Accuracy: 0.864609277709
## 1-kth fold --------------------------------
Training Time Elapsed: 00:05:44, 344.475265026
Training set score: 0.859602
Training set loss: 0.375523
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_1_26_0.pkl

Elapsed Seconds: 346.718502998
Total Time Elapsed: 00:05:46
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.82      0.86     15422
         1.0       0.84      0.89      0.87     25683
         2.0       0.86      0.84      0.85     10287

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.87      0.85      0.86     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.859997665006
## 2-kth fold --------------------------------
Training Time Elapsed: 00:05:45, 345.483975887
Training set score: 0.867138
Training set loss: 0.359976
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_2_26_0.pkl

Elapsed Seconds: 347.544179916
Total Time Elapsed: 00:05:47
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.82      0.85     15422
         1.0       0.85      0.89      0.87     25683
         2.0       0.84      0.84      0.84     10287

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.86      0.85      0.86     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.859355541719
## 3-kth fold --------------------------------
Training Time Elapsed: 00:05:22, 322.455091
Training set score: 0.873871
Training set loss: 0.345874
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 466
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_3_26_0.pkl

Elapsed Seconds: 324.557965994
Total Time Elapsed: 00:05:24
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.86      0.87      0.86     15422
         1.0       0.82      0.89      0.86     25682
         2.0       0.90      0.70      0.79     10287

   micro avg       0.85      0.85      0.85     51391
   macro avg       0.86      0.82      0.84     51391
weighted avg       0.85      0.85      0.84     51391


Accuracy: 0.845887412193
## 4-kth fold --------------------------------
Training Time Elapsed: 00:05:41, 341.985516071
Training set score: 0.861072
Training set loss: 0.370789
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_4_26_0.pkl

Elapsed Seconds: 343.964759827
Total Time Elapsed: 00:05:43
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.82      0.86     15422
         1.0       0.84      0.89      0.86     25682
         2.0       0.85      0.83      0.84     10287

   micro avg       0.86      0.86      0.86     51391
   macro avg       0.86      0.85      0.85     51391
weighted avg       0.86      0.86      0.86     51391


Accuracy: 0.856998307097
Sorted List of Temp Training Scores: 
[]

Sorted List of Temp Test Accuracies: 
[]

Average Training Accuracy for: (26, 0)
0.867185492793

Average Test Accuracy for: (26, 0)
0.857369640745

Elapsed Seconds: 1652.22442698
Total Time Elapsed: 00:27:32
# Num of nodes: 27, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:05:59, 359.039544106
Training set score: 0.876911
Training set loss: 0.348540
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_0_27_0.pkl

Elapsed Seconds: 361.015349865
Total Time Elapsed: 00:06:01
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.85      0.87     15422
         1.0       0.87      0.88      0.87     25683
         2.0       0.83      0.88      0.85     10287

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.87      0.87      0.87     51392
weighted avg       0.87      0.87      0.87     51392


Accuracy: 0.869571139477
## 1-kth fold --------------------------------
Training Time Elapsed: 00:05:42, 342.409894943
Training set score: 0.858075
Training set loss: 0.387270
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_1_27_0.pkl

Elapsed Seconds: 344.364238977
Total Time Elapsed: 00:05:44
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.86      0.86      0.86     15422
         1.0       0.85      0.87      0.86     25683
         2.0       0.85      0.83      0.84     10287

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.86      0.85      0.85     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.856670298879
## 2-kth fold --------------------------------
Training Time Elapsed: 00:04:16, 256.060193062
Training set score: 0.865610
Training set loss: 0.357949
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 480
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_2_27_0.pkl

Elapsed Seconds: 257.717432976
Total Time Elapsed: 00:04:17
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.88      0.82      0.85     15422
         1.0       0.85      0.88      0.86     25683
         2.0       0.83      0.86      0.84     10287

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.85      0.85      0.85     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.855288760897
## 3-kth fold --------------------------------
Training Time Elapsed: 00:04:16, 256.280878067
Training set score: 0.865027
Training set loss: 0.360785
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 480
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_3_27_0.pkl

Elapsed Seconds: 258.019525051
Total Time Elapsed: 00:04:18
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.82      0.86      0.84     15422
         1.0       0.81      0.86      0.84     25682
         2.0       0.89      0.68      0.77     10287

   micro avg       0.82      0.82      0.82     51391
   macro avg       0.84      0.80      0.81     51391
weighted avg       0.83      0.82      0.82     51391


Accuracy: 0.82489151797
## 4-kth fold --------------------------------
Training Time Elapsed: 00:04:25, 265.930124998
Training set score: 0.878088
Training set loss: 0.344985
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_4_27_0.pkl

Elapsed Seconds: 268.515617132
Total Time Elapsed: 00:04:28
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.93      0.85      0.88     15422
         1.0       0.87      0.90      0.88     25682
         2.0       0.84      0.87      0.86     10287

   micro avg       0.88      0.88      0.88     51391
   macro avg       0.88      0.87      0.87     51391
weighted avg       0.88      0.88      0.88     51391


Accuracy: 0.877196396256
Sorted List of Temp Training Scores: 
[[(26, 0), 0.8671854927927134]]

Sorted List of Temp Test Accuracies: 
[[(26, 0), 0.8573696407445489]]

Average Training Accuracy for: (27, 0)
0.868742162502

Average Test Accuracy for: (27, 0)
0.856723622696

Elapsed Seconds: 1491.21236992
Total Time Elapsed: 00:24:51
# Num of nodes: 28, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:03:54, 234.160501003
Training set score: 0.863222
Training set loss: 0.373777
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 460
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_0_28_0.pkl

Elapsed Seconds: 235.655705929
Total Time Elapsed: 00:03:55
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.82      0.85     15422
         1.0       0.85      0.88      0.86     25683
         2.0       0.83      0.87      0.85     10287

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.86      0.85      0.85     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.857507004981
## 1-kth fold --------------------------------
Training Time Elapsed: 00:04:11, 251.931754112
Training set score: 0.867191
Training set loss: 0.366527
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_1_28_0.pkl

Elapsed Seconds: 253.493535042
Total Time Elapsed: 00:04:13
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.87      0.88     15422
         1.0       0.87      0.88      0.88     25683
         2.0       0.86      0.86      0.86     10287

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.87      0.87      0.87     51392
weighted avg       0.87      0.87      0.87     51392


Accuracy: 0.874104919054
## 2-kth fold --------------------------------
Training Time Elapsed: 00:03:42, 222.803792
Training set score: 0.870460
Training set loss: 0.363976
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 462
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_2_28_0.pkl

Elapsed Seconds: 224.229560137
Total Time Elapsed: 00:03:44
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.81      0.86     15422
         1.0       0.85      0.89      0.87     25683
         2.0       0.82      0.87      0.84     10287

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.86      0.86      0.86     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.860523038605
## 3-kth fold --------------------------------
Training Time Elapsed: 00:04:07, 247.019629002
Training set score: 0.867581
Training set loss: 0.362320
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_3_28_0.pkl

Elapsed Seconds: 248.582273006
Total Time Elapsed: 00:04:08
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.82      0.88      0.85     15422
         1.0       0.82      0.87      0.84     25682
         2.0       0.91      0.67      0.77     10287

   micro avg       0.83      0.83      0.83     51391
   macro avg       0.85      0.81      0.82     51391
weighted avg       0.84      0.83      0.83     51391


Accuracy: 0.831059913214
## 4-kth fold --------------------------------
Training Time Elapsed: 00:04:05, 245.65267086
Training set score: 0.860795
Training set loss: 0.380863
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_4_28_0.pkl

Elapsed Seconds: 247.11886096
Total Time Elapsed: 00:04:07
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.88      0.82      0.85     15422
         1.0       0.85      0.86      0.85     25682
         2.0       0.82      0.87      0.85     10287

   micro avg       0.85      0.85      0.85     51391
   macro avg       0.85      0.85      0.85     51391
weighted avg       0.85      0.85      0.85     51391


Accuracy: 0.850109941429
Sorted List of Temp Training Scores: 
[[(27, 0), 0.868742162502181], [(26, 0), 0.8671854927927134]]

Sorted List of Temp Test Accuracies: 
[[(26, 0), 0.8573696407445489], [(27, 0), 0.8567236226958057]]

Average Training Accuracy for: (28, 0)
0.865849675165

Average Test Accuracy for: (28, 0)
0.854660963457

Elapsed Seconds: 1210.22739291
Total Time Elapsed: 00:20:10
# Num of nodes: 29, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:04:05, 245.346452951
Training set score: 0.872610
Training set loss: 0.360574
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_0_29_0.pkl

Elapsed Seconds: 246.76665616
Total Time Elapsed: 00:04:06
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.83      0.85     15422
         1.0       0.86      0.87      0.86     25683
         2.0       0.82      0.89      0.85     10287

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.85      0.86      0.86     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.858538293898
## 1-kth fold --------------------------------
Training Time Elapsed: 00:03:10, 190.360258102
Training set score: 0.869949
Training set loss: 0.362717
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 386
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_1_29_0.pkl

Elapsed Seconds: 191.816351891
Total Time Elapsed: 00:03:11
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.88      0.89     15422
         1.0       0.88      0.88      0.88     25683
         2.0       0.84      0.86      0.85     10287

   micro avg       0.88      0.88      0.88     51392
   macro avg       0.87      0.87      0.87     51392
weighted avg       0.88      0.88      0.88     51392


Accuracy: 0.875428082192
## 2-kth fold --------------------------------
Training Time Elapsed: 00:04:00, 240.961709023
Training set score: 0.859257
Training set loss: 0.381301
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_2_29_0.pkl

Elapsed Seconds: 242.459630013
Total Time Elapsed: 00:04:02
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.88      0.82      0.85     15422
         1.0       0.84      0.88      0.86     25683
         2.0       0.84      0.83      0.83     10287

   micro avg       0.85      0.85      0.85     51392
   macro avg       0.85      0.84      0.85     51392
weighted avg       0.85      0.85      0.85     51392


Accuracy: 0.853342932752
## 3-kth fold --------------------------------
Training Time Elapsed: 00:04:05, 245.023581982
Training set score: 0.871215
Training set loss: 0.353673
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_3_29_0.pkl

Elapsed Seconds: 246.482965946
Total Time Elapsed: 00:04:06
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.84      0.87      0.86     15422
         1.0       0.82      0.88      0.85     25682
         2.0       0.92      0.69      0.78     10287

   micro avg       0.84      0.84      0.84     51391
   macro avg       0.86      0.81      0.83     51391
weighted avg       0.85      0.84      0.84     51391


Accuracy: 0.840653032632
## 4-kth fold --------------------------------
Training Time Elapsed: 00:04:05, 245.018922091
Training set score: 0.861417
Training set loss: 0.369163
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_4_29_0.pkl

Elapsed Seconds: 246.550302029
Total Time Elapsed: 00:04:06
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.83      0.86     15422
         1.0       0.85      0.87      0.86     25682
         2.0       0.83      0.85      0.84     10287

   micro avg       0.86      0.86      0.86     51391
   macro avg       0.86      0.85      0.85     51391
weighted avg       0.86      0.86      0.86     51391


Accuracy: 0.856317253994
Sorted List of Temp Training Scores: 
[[(27, 0), 0.868742162502181], [(26, 0), 0.8671854927927134], [(28, 0), 0.8658496751645084]]

Sorted List of Temp Test Accuracies: 
[[(26, 0), 0.8573696407445489], [(27, 0), 0.8567236226958057], [(28, 0), 0.8546609634567817]]

Average Training Accuracy for: (29, 0)
0.866889726285

Average Test Accuracy for: (29, 0)
0.856855919094

Elapsed Seconds: 1175.26085806
Total Time Elapsed: 00:19:35
# Num of nodes: 30, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:04:11, 251.546115875
Training set score: 0.878239
Training set loss: 0.344790
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_0_30_0.pkl

Elapsed Seconds: 253.011723995
Total Time Elapsed: 00:04:13
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.91      0.82      0.86     15422
         1.0       0.87      0.88      0.87     25683
         2.0       0.81      0.90      0.85     10287

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.86      0.87      0.86     51392
weighted avg       0.87      0.87      0.87     51392


Accuracy: 0.865231942715
## 1-kth fold --------------------------------
Training Time Elapsed: 00:03:30, 210.675921202
Training set score: 0.880817
Training set loss: 0.341925
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 433
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_1_30_0.pkl

Elapsed Seconds: 212.148520947
Total Time Elapsed: 00:03:32
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.91      0.88      0.89     15422
         1.0       0.88      0.90      0.89     25683
         2.0       0.87      0.86      0.86     10287

   micro avg       0.88      0.88      0.88     51392
   macro avg       0.88      0.88      0.88     51392
weighted avg       0.89      0.88      0.88     51392


Accuracy: 0.88482643213
## 2-kth fold --------------------------------
Training Time Elapsed: 00:04:02, 242.216777086
Training set score: 0.875490
Training set loss: 0.352891
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_2_30_0.pkl

Elapsed Seconds: 243.620456934
Total Time Elapsed: 00:04:03
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.82      0.86     15422
         1.0       0.86      0.89      0.87     25683
         2.0       0.83      0.88      0.85     10287

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.86      0.86      0.86     51392
weighted avg       0.87      0.87      0.87     51392


Accuracy: 0.865251400996
## 3-kth fold --------------------------------
Training Time Elapsed: 00:04:09, 249.091051817
Training set score: 0.872810
Training set loss: 0.353336
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_3_30_0.pkl

Elapsed Seconds: 250.522644043
Total Time Elapsed: 00:04:10
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.85      0.87      0.86     15422
         1.0       0.82      0.89      0.85     25682
         2.0       0.90      0.68      0.78     10287

   micro avg       0.84      0.84      0.84     51391
   macro avg       0.86      0.81      0.83     51391
weighted avg       0.85      0.84      0.84     51391


Accuracy: 0.84269619194
## 4-kth fold --------------------------------
Training Time Elapsed: 00:03:55, 235.646038055
Training set score: 0.870217
Training set loss: 0.361340
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 486
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_3_tests/../../../models/ann_entry_3_26_to_30_0/ann_exit_3_4/ann_4_30_0.pkl

Elapsed Seconds: 237.077740908
Total Time Elapsed: 00:03:57
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.83      0.86     15422
         1.0       0.86      0.88      0.87     25682
         2.0       0.82      0.87      0.85     10287

   micro avg       0.86      0.86      0.86     51391
   macro avg       0.86      0.86      0.86     51391
weighted avg       0.86      0.86      0.86     51391


Accuracy: 0.862271603977
Sorted List of Temp Training Scores: 
[[(27, 0), 0.868742162502181], [(26, 0), 0.8671854927927134], [(29, 0), 0.866889726285297], [(28, 0), 0.8658496751645084]]

Sorted List of Temp Test Accuracies: 
[[(26, 0), 0.8573696407445489], [(29, 0), 0.8568559190935812], [(27, 0), 0.8567236226958057], [(28, 0), 0.8546609634567817]]

Average Training Accuracy for: (30, 0)
0.875514683335

Average Test Accuracy for: (30, 0)
0.864055514352

Elapsed Seconds: 1197.55377412
Total Time Elapsed: 00:19:57
List of Training Scores: 
[[(26, 0), 0.8671854927927134], [(27, 0), 0.868742162502181], [(28, 0), 0.8658496751645084], [(29, 0), 0.866889726285297], [(30, 0), 0.8755146833349672]]

List of Test Accuracies: 
[[(26, 0), 0.8573696407445489], [(27, 0), 0.8567236226958057], [(28, 0), 0.8546609634567817], [(29, 0), 0.8568559190935812], [(30, 0), 0.8640555143516341]]

Sorted List of Training Scores: 
[[(30, 0), 0.8755146833349672], [(27, 0), 0.868742162502181], [(26, 0), 0.8671854927927134], [(29, 0), 0.866889726285297], [(28, 0), 0.8658496751645084]]

Sorted List of Test Accuracies: 
[[(30, 0), 0.8640555143516341], [(26, 0), 0.8573696407445489], [(29, 0), 0.8568559190935812], [(27, 0), 0.8567236226958057], [(28, 0), 0.8546609634567817]]

Elapsed Seconds: 6726.4797039
Total Time Elapsed: 01:52:06
