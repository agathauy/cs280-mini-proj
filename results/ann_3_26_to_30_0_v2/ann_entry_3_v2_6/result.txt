# ann_entry_3_v2_6
['station', 'month', 'day_of_the_week', 'hour', 'avg_air_pressure', 'temperature']
# Num of nodes: 26, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:04:07, 247.898878098
Training set score: 0.861154
Training set loss: 0.391393
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_0_26_0.pkl

Elapsed Seconds: 249.465586901
Total Time Elapsed: 00:04:09
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.78      0.84     15427
         1.0       0.85      0.89      0.87     25682
         2.0       0.83      0.89      0.86     10283

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.86      0.85      0.86     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.858518835616
## 1-kth fold --------------------------------
Training Time Elapsed: 00:04:06, 246.085561037
Training set score: 0.859301
Training set loss: 0.387297
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_1_26_0.pkl

Elapsed Seconds: 247.719150066
Total Time Elapsed: 00:04:07
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.85      0.87     15427
         1.0       0.86      0.89      0.88     25682
         2.0       0.86      0.84      0.85     10283

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.87      0.86      0.87     51392
weighted avg       0.87      0.87      0.87     51392


Accuracy: 0.869629514321
## 2-kth fold --------------------------------
Training Time Elapsed: 00:03:25, 205.710494041
Training set score: 0.861709
Training set loss: 0.384489
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 406
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_2_26_0.pkl

Elapsed Seconds: 207.356089115
Total Time Elapsed: 00:03:27
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.88      0.80      0.84     15427
         1.0       0.86      0.88      0.87     25682
         2.0       0.83      0.89      0.86     10283

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.86      0.86      0.86     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.859219333748
## 3-kth fold --------------------------------
Training Time Elapsed: 00:04:15, 255.703109026
Training set score: 0.872553
Training set loss: 0.361721
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_3_26_0.pkl

Elapsed Seconds: 257.177717924
Total Time Elapsed: 00:04:17
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.84      0.84      0.84     15426
         1.0       0.80      0.88      0.84     25682
         2.0       0.88      0.68      0.77     10283

   micro avg       0.83      0.83      0.83     51391
   macro avg       0.84      0.80      0.82     51391
weighted avg       0.83      0.83      0.82     51391


Accuracy: 0.826448210776
## 4-kth fold --------------------------------
Training Time Elapsed: 00:04:08, 248.001649857
Training set score: 0.870884
Training set loss: 0.369594
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_4_26_0.pkl

Elapsed Seconds: 249.749850035
Total Time Elapsed: 00:04:09
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.84      0.87     15426
         1.0       0.86      0.87      0.87     25682
         2.0       0.81      0.88      0.85     10283

   micro avg       0.86      0.86      0.86     51391
   macro avg       0.86      0.86      0.86     51391
weighted avg       0.86      0.86      0.86     51391


Accuracy: 0.863419664922
Sorted List of Temp Training Scores: 
[]

Sorted List of Temp Test Accuracies: 
[]

Average Training Accuracy for: (26, 0)
0.865119967858

Average Test Accuracy for: (26, 0)
0.855447111877

Elapsed Seconds: 1212.76221418
Total Time Elapsed: 00:20:12
# Num of nodes: 27, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:03:40, 220.503532887
Training set score: 0.859223
Training set loss: 0.387369
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 437
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_0_27_0.pkl

Elapsed Seconds: 222.199915886
Total Time Elapsed: 00:03:42
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.87      0.80      0.83     15427
         1.0       0.85      0.87      0.86     25682
         2.0       0.83      0.88      0.86     10283

   micro avg       0.85      0.85      0.85     51392
   macro avg       0.85      0.85      0.85     51392
weighted avg       0.85      0.85      0.85     51392


Accuracy: 0.852798100872
## 1-kth fold --------------------------------
Training Time Elapsed: 00:04:02, 242.182360888
Training set score: 0.860590
Training set loss: 0.390978
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 487
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_1_27_0.pkl

Elapsed Seconds: 243.811566114
Total Time Elapsed: 00:04:03
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.84      0.86     15427
         1.0       0.86      0.89      0.87     25682
         2.0       0.87      0.85      0.86     10283

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.87      0.86      0.87     51392
weighted avg       0.87      0.87      0.87     51392


Accuracy: 0.868656600249
## 2-kth fold --------------------------------
Training Time Elapsed: 00:04:25, 265.305287123
Training set score: 0.874707
Training set loss: 0.372340
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_2_27_0.pkl

Elapsed Seconds: 266.975969076
Total Time Elapsed: 00:04:26
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.91      0.82      0.86     15427
         1.0       0.87      0.91      0.89     25682
         2.0       0.86      0.88      0.87     10283

   micro avg       0.88      0.88      0.88     51392
   macro avg       0.88      0.87      0.87     51392
weighted avg       0.88      0.88      0.88     51392


Accuracy: 0.87554483188
## 3-kth fold --------------------------------
Training Time Elapsed: 00:03:08, 188.859401941
Training set score: 0.871268
Training set loss: 0.361651
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 372
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_3_27_0.pkl

Elapsed Seconds: 190.622391939
Total Time Elapsed: 00:03:10
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.81      0.86      0.84     15426
         1.0       0.80      0.86      0.83     25682
         2.0       0.89      0.66      0.76     10283

   micro avg       0.82      0.82      0.82     51391
   macro avg       0.84      0.79      0.81     51391
weighted avg       0.82      0.82      0.82     51391


Accuracy: 0.81973497305
## 4-kth fold --------------------------------
Training Time Elapsed: 00:04:08, 248.727617979
Training set score: 0.858761
Training set loss: 0.388617
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_4_27_0.pkl

Elapsed Seconds: 250.409222841
Total Time Elapsed: 00:04:10
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.82      0.85     15426
         1.0       0.86      0.86      0.86     25682
         2.0       0.81      0.90      0.85     10283

   micro avg       0.86      0.86      0.86     51391
   macro avg       0.85      0.86      0.85     51391
weighted avg       0.86      0.86      0.86     51391


Accuracy: 0.85524702769
Sorted List of Temp Training Scores: 
[[(26, 0), 0.8651199678580636]]

Sorted List of Temp Test Accuracies: 
[[(26, 0), 0.8554471118768513]]

Average Training Accuracy for: (27, 0)
0.864909829418

Average Test Accuracy for: (27, 0)
0.854396306748

Elapsed Seconds: 1176.70954108
Total Time Elapsed: 00:19:36
# Num of nodes: 28, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:04:11, 251.811647892
Training set score: 0.874999
Training set loss: 0.362758
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 417
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_0_28_0.pkl

Elapsed Seconds: 253.341532946
Total Time Elapsed: 00:04:13
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.91      0.80      0.85     15427
         1.0       0.86      0.90      0.88     25682
         2.0       0.85      0.90      0.87     10283

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.87      0.87      0.87     51392
weighted avg       0.87      0.87      0.87     51392


Accuracy: 0.871302926526
## 1-kth fold --------------------------------
Training Time Elapsed: 00:04:02, 242.386869907
Training set score: 0.873831
Training set loss: 0.365315
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_1_28_0.pkl

Elapsed Seconds: 243.884792805
Total Time Elapsed: 00:04:03
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.91      0.85      0.88     15427
         1.0       0.87      0.90      0.88     25682
         2.0       0.86      0.87      0.87     10283

   micro avg       0.88      0.88      0.88     51392
   macro avg       0.88      0.87      0.88     51392
weighted avg       0.88      0.88      0.88     51392


Accuracy: 0.879241905355
## 2-kth fold --------------------------------
Training Time Elapsed: 00:04:13, 253.951576948
Training set score: 0.870256
Training set loss: 0.375399
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_2_28_0.pkl

Elapsed Seconds: 255.483937025
Total Time Elapsed: 00:04:15
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.91      0.80      0.85     15427
         1.0       0.86      0.91      0.88     25682
         2.0       0.86      0.87      0.87     10283

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.88      0.86      0.87     51392
weighted avg       0.87      0.87      0.87     51392


Accuracy: 0.870446762142
## 3-kth fold --------------------------------
Training Time Elapsed: 00:03:00, 180.208257914
Training set score: 0.847033
Training set loss: 0.403208
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 363
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_3_28_0.pkl

Elapsed Seconds: 181.780181885
Total Time Elapsed: 00:03:01
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.80      0.81      0.81     15426
         1.0       0.79      0.85      0.82     25682
         2.0       0.86      0.68      0.76     10283

   micro avg       0.80      0.80      0.80     51391
   macro avg       0.82      0.78      0.80     51391
weighted avg       0.81      0.80      0.80     51391


Accuracy: 0.804829639431
## 4-kth fold --------------------------------
Training Time Elapsed: 00:07:42, 462.040061951
Training set score: 0.856966
Training set loss: 0.397209
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_4_28_0.pkl

Elapsed Seconds: 463.52481389
Total Time Elapsed: 00:07:43
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.80      0.84     15426
         1.0       0.85      0.87      0.86     25682
         2.0       0.82      0.89      0.86     10283

   micro avg       0.85      0.85      0.85     51391
   macro avg       0.85      0.86      0.85     51391
weighted avg       0.86      0.85      0.85     51391


Accuracy: 0.854682726547
Sorted List of Temp Training Scores: 
[[(26, 0), 0.8651199678580636], [(27, 0), 0.8649098294176806]]

Sorted List of Temp Test Accuracies: 
[[(26, 0), 0.8554471118768513], [(27, 0), 0.8543963067481348]]

Average Training Accuracy for: (28, 0)
0.864617004758

Average Test Accuracy for: (28, 0)
0.856100792

Elapsed Seconds: 1400.83776116
Total Time Elapsed: 00:23:20
# Num of nodes: 29, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:04:23, 263.778577089
Training set score: 0.883483
Training set loss: 0.349565
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_0_29_0.pkl

Elapsed Seconds: 265.389318943
Total Time Elapsed: 00:04:25
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.93      0.80      0.86     15427
         1.0       0.87      0.91      0.89     25682
         2.0       0.85      0.91      0.87     10283

   micro avg       0.88      0.88      0.88     51392
   macro avg       0.88      0.87      0.87     51392
weighted avg       0.88      0.88      0.88     51392


Accuracy: 0.877724159402
## 1-kth fold --------------------------------
Training Time Elapsed: 00:03:36, 216.482978106
Training set score: 0.867157
Training set loss: 0.376904
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 436
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_1_29_0.pkl

Elapsed Seconds: 218.053281069
Total Time Elapsed: 00:03:38
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.91      0.84      0.88     15427
         1.0       0.85      0.91      0.88     25682
         2.0       0.88      0.83      0.86     10283

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.88      0.86      0.87     51392
weighted avg       0.88      0.87      0.87     51392


Accuracy: 0.874785958904
## 2-kth fold --------------------------------
Training Time Elapsed: 00:03:20, 200.801592827
Training set score: 0.869575
Training set loss: 0.370232
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 387
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_2_29_0.pkl

Elapsed Seconds: 203.387757063
Total Time Elapsed: 00:03:23
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.92      0.79      0.85     15427
         1.0       0.85      0.92      0.88     25682
         2.0       0.87      0.85      0.86     10283

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.88      0.86      0.87     51392
weighted avg       0.87      0.87      0.87     51392


Accuracy: 0.870621886675
## 3-kth fold --------------------------------
Training Time Elapsed: 00:09:22, 562.867758989
Training set score: 0.863616
Training set loss: 0.382208
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 403
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_3_29_0.pkl

Elapsed Seconds: 564.452053785
Total Time Elapsed: 00:09:24
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.83      0.83      0.83     15426
         1.0       0.80      0.87      0.83     25682
         2.0       0.86      0.66      0.75     10283

   micro avg       0.82      0.82      0.82     51391
   macro avg       0.83      0.79      0.80     51391
weighted avg       0.82      0.82      0.81     51391


Accuracy: 0.816971843319
## 4-kth fold --------------------------------
Training Time Elapsed: 00:04:12, 252.978782177
Training set score: 0.868632
Training set loss: 0.383826
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_4_29_0.pkl

Elapsed Seconds: 254.519721985
Total Time Elapsed: 00:04:14
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.84      0.86     15426
         1.0       0.87      0.87      0.87     25682
         2.0       0.83      0.89      0.86     10283

   micro avg       0.86      0.86      0.86     51391
   macro avg       0.86      0.87      0.86     51391
weighted avg       0.87      0.86      0.86     51391


Accuracy: 0.864820688447
Sorted List of Temp Training Scores: 
[[(26, 0), 0.8651199678580636], [(27, 0), 0.8649098294176806], [(28, 0), 0.8646170047583913]]

Sorted List of Temp Test Accuracies: 
[[(28, 0), 0.8561007920001791], [(26, 0), 0.8554471118768513], [(27, 0), 0.8543963067481348]]

Average Training Accuracy for: (29, 0)
0.87049246252

Average Test Accuracy for: (29, 0)
0.86098490735

Elapsed Seconds: 1508.68014383
Total Time Elapsed: 00:25:08
# Num of nodes: 30, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:04:19, 259.01454401
Training set score: 0.863046
Training set loss: 0.378163
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_0_30_0.pkl

Elapsed Seconds: 260.649500847
Total Time Elapsed: 00:04:20
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.78      0.83     15427
         1.0       0.84      0.89      0.87     25682
         2.0       0.83      0.88      0.86     10283

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.86      0.85      0.85     51392
weighted avg       0.86      0.86      0.85     51392


Accuracy: 0.85546388543
## 1-kth fold --------------------------------
Training Time Elapsed: 00:03:58, 238.170378208
Training set score: 0.862292
Training set loss: 0.378095
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 468
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_1_30_0.pkl

Elapsed Seconds: 239.763604164
Total Time Elapsed: 00:03:59
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.91      0.83      0.86     15427
         1.0       0.85      0.90      0.87     25682
         2.0       0.86      0.85      0.85     10283

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.87      0.86      0.86     51392
weighted avg       0.87      0.87      0.87     51392


Accuracy: 0.866341064757
## 2-kth fold --------------------------------
Training Time Elapsed: 00:03:17, 197.619618893
Training set score: 0.868543
Training set loss: 0.370323
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 380
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_2_30_0.pkl

Elapsed Seconds: 199.085938931
Total Time Elapsed: 00:03:19
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102728, 0.0: 61706, 2.0: 41132})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25682, 0.0: 15427, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.91      0.81      0.86     15427
         1.0       0.86      0.91      0.88     25682
         2.0       0.86      0.86      0.86     10283

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.87      0.86      0.87     51392
weighted avg       0.87      0.87      0.87     51392


Accuracy: 0.869687889166
## 3-kth fold --------------------------------
Training Time Elapsed: 00:04:08, 248.964033127
Training set score: 0.880633
Training set loss: 0.342654
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 495
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_3_30_0.pkl

Elapsed Seconds: 251.355028868
Total Time Elapsed: 00:04:11
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.83      0.86      0.84     15426
         1.0       0.81      0.87      0.84     25682
         2.0       0.89      0.68      0.77     10283

   micro avg       0.83      0.83      0.83     51391
   macro avg       0.84      0.80      0.82     51391
weighted avg       0.83      0.83      0.83     51391


Accuracy: 0.828608122045
## 4-kth fold --------------------------------
Training Time Elapsed: 00:21:04, 1264.24552989
Training set score: 0.857754
Training set loss: 0.386656
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 476
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_entry_3_v2_6/ann_4_30_0.pkl

Elapsed Seconds: 1265.86376405
Total Time Elapsed: 00:21:05
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102728, 0.0: 61707, 2.0: 41132})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15426, 2.0: 10283})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.83      0.86     15426
         1.0       0.85      0.87      0.86     25682
         2.0       0.82      0.87      0.84     10283

   micro avg       0.86      0.86      0.86     51391
   macro avg       0.86      0.86      0.86     51391
weighted avg       0.86      0.86      0.86     51391


Accuracy: 0.858087992061
Sorted List of Temp Training Scores: 
[[(29, 0), 0.870492462520183], [(26, 0), 0.8651199678580636], [(27, 0), 0.8649098294176806], [(28, 0), 0.8646170047583913]]

Sorted List of Temp Test Accuracies: 
[[(29, 0), 0.8609849073495166], [(28, 0), 0.8561007920001791], [(26, 0), 0.8554471118768513], [(27, 0), 0.8543963067481348]]

Average Training Accuracy for: (30, 0)
0.866453850941

Average Test Accuracy for: (30, 0)
0.855637790692

Elapsed Seconds: 2219.45406103
Total Time Elapsed: 00:36:59
List of Training Scores: 
[[(26, 0), 0.8651199678580636], [(27, 0), 0.8649098294176806], [(28, 0), 0.8646170047583913], [(29, 0), 0.870492462520183], [(30, 0), 0.8664538509413056]]

List of Test Accuracies: 
[[(26, 0), 0.8554471118768513], [(27, 0), 0.8543963067481348], [(28, 0), 0.8561007920001791], [(29, 0), 0.8609849073495166], [(30, 0), 0.8556377906916023]]

Sorted List of Training Scores: 
[[(29, 0), 0.870492462520183], [(30, 0), 0.8664538509413056], [(26, 0), 0.8651199678580636], [(27, 0), 0.8649098294176806], [(28, 0), 0.8646170047583913]]

Sorted List of Test Accuracies: 
[[(29, 0), 0.8609849073495166], [(28, 0), 0.8561007920001791], [(30, 0), 0.8556377906916023], [(26, 0), 0.8554471118768513], [(27, 0), 0.8543963067481348]]

Elapsed Seconds: 7518.44415808
Total Time Elapsed: 02:05:18
