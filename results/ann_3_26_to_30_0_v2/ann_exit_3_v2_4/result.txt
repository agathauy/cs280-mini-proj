# ann_exit_3_v2_4
['station', 'month', 'day_of_the_week', 'hour', 'num_trains', 'avg_air_pressure', 'temperature', 'holiday_type', 'payday_type']
# Num of nodes: 26, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:05:01, 301.550180912
Training set score: 0.874245
Training set loss: 0.350699
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_0_26_0.pkl

Elapsed Seconds: 303.445795059
Total Time Elapsed: 00:05:03
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.91      0.82      0.86     15422
         1.0       0.86      0.88      0.87     25683
         2.0       0.82      0.89      0.85     10287

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.86      0.86      0.86     51392
weighted avg       0.87      0.86      0.86     51392


Accuracy: 0.864609277709
## 1-kth fold --------------------------------
Training Time Elapsed: 00:05:43, 343.558691978
Training set score: 0.859602
Training set loss: 0.375523
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_1_26_0.pkl

Elapsed Seconds: 345.643082142
Total Time Elapsed: 00:05:45
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.82      0.86     15422
         1.0       0.84      0.89      0.87     25683
         2.0       0.86      0.84      0.85     10287

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.87      0.85      0.86     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.859997665006
## 2-kth fold --------------------------------
Training Time Elapsed: 00:05:38, 338.339449167
Training set score: 0.867138
Training set loss: 0.359976
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_2_26_0.pkl

Elapsed Seconds: 340.453005075
Total Time Elapsed: 00:05:40
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.82      0.85     15422
         1.0       0.85      0.89      0.87     25683
         2.0       0.84      0.84      0.84     10287

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.86      0.85      0.86     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.859355541719
## 3-kth fold --------------------------------
Training Time Elapsed: 00:05:23, 323.596389055
Training set score: 0.873871
Training set loss: 0.345874
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 466
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_3_26_0.pkl

Elapsed Seconds: 326.048529863
Total Time Elapsed: 00:05:26
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.86      0.87      0.86     15422
         1.0       0.82      0.89      0.86     25682
         2.0       0.90      0.70      0.79     10287

   micro avg       0.85      0.85      0.85     51391
   macro avg       0.86      0.82      0.84     51391
weighted avg       0.85      0.85      0.84     51391


Accuracy: 0.845887412193
## 4-kth fold --------------------------------
Training Time Elapsed: 00:05:44, 344.654155016
Training set score: 0.861072
Training set loss: 0.370789
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_4_26_0.pkl

Elapsed Seconds: 346.703990936
Total Time Elapsed: 00:05:46
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.82      0.86     15422
         1.0       0.84      0.89      0.86     25682
         2.0       0.85      0.83      0.84     10287

   micro avg       0.86      0.86      0.86     51391
   macro avg       0.86      0.85      0.85     51391
weighted avg       0.86      0.86      0.86     51391


Accuracy: 0.856998307097
Sorted List of Temp Training Scores: 
[]

Sorted List of Temp Test Accuracies: 
[]

Average Training Accuracy for: (26, 0)
0.867185492793

Average Test Accuracy for: (26, 0)
0.857369640745

Elapsed Seconds: 1664.10127401
Total Time Elapsed: 00:27:44
# Num of nodes: 27, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:05:50, 350.409346819
Training set score: 0.876911
Training set loss: 0.348540
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_0_27_0.pkl

Elapsed Seconds: 352.862229824
Total Time Elapsed: 00:05:52
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.85      0.87     15422
         1.0       0.87      0.88      0.87     25683
         2.0       0.83      0.88      0.85     10287

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.87      0.87      0.87     51392
weighted avg       0.87      0.87      0.87     51392


Accuracy: 0.869571139477
## 1-kth fold --------------------------------
Training Time Elapsed: 00:05:27, 327.526468039
Training set score: 0.858075
Training set loss: 0.387270
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_1_27_0.pkl

Elapsed Seconds: 329.393560886
Total Time Elapsed: 00:05:29
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.86      0.86      0.86     15422
         1.0       0.85      0.87      0.86     25683
         2.0       0.85      0.83      0.84     10287

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.86      0.85      0.85     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.856670298879
## 2-kth fold --------------------------------
Training Time Elapsed: 00:04:13, 253.8005898
Training set score: 0.865610
Training set loss: 0.357949
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 480
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_2_27_0.pkl

Elapsed Seconds: 255.362719059
Total Time Elapsed: 00:04:15
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.88      0.82      0.85     15422
         1.0       0.85      0.88      0.86     25683
         2.0       0.83      0.86      0.84     10287

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.85      0.85      0.85     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.855288760897
## 3-kth fold --------------------------------
Training Time Elapsed: 00:04:10, 250.827433109
Training set score: 0.865027
Training set loss: 0.360785
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 480
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_3_27_0.pkl

Elapsed Seconds: 252.363854885
Total Time Elapsed: 00:04:12
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.82      0.86      0.84     15422
         1.0       0.81      0.86      0.84     25682
         2.0       0.89      0.68      0.77     10287

   micro avg       0.82      0.82      0.82     51391
   macro avg       0.84      0.80      0.81     51391
weighted avg       0.83      0.82      0.82     51391


Accuracy: 0.82489151797
## 4-kth fold --------------------------------
Training Time Elapsed: 00:04:22, 262.349956036
Training set score: 0.878088
Training set loss: 0.344985
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_4_27_0.pkl

Elapsed Seconds: 264.012477875
Total Time Elapsed: 00:04:24
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.93      0.85      0.88     15422
         1.0       0.87      0.90      0.88     25682
         2.0       0.84      0.87      0.86     10287

   micro avg       0.88      0.88      0.88     51391
   macro avg       0.88      0.87      0.87     51391
weighted avg       0.88      0.88      0.88     51391


Accuracy: 0.877196396256
Sorted List of Temp Training Scores: 
[[(26, 0), 0.8671854927927134]]

Sorted List of Temp Test Accuracies: 
[[(26, 0), 0.8573696407445489]]

Average Training Accuracy for: (27, 0)
0.868742162502

Average Test Accuracy for: (27, 0)
0.856723622696

Elapsed Seconds: 1455.39349484
Total Time Elapsed: 00:24:15
# Num of nodes: 28, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:03:51, 231.476345778
Training set score: 0.863222
Training set loss: 0.373777
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 460
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_0_28_0.pkl

Elapsed Seconds: 232.962768078
Total Time Elapsed: 00:03:52
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.82      0.85     15422
         1.0       0.85      0.88      0.86     25683
         2.0       0.83      0.87      0.85     10287

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.86      0.85      0.85     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.857507004981
## 1-kth fold --------------------------------
Training Time Elapsed: 00:04:04, 244.572407961
Training set score: 0.867191
Training set loss: 0.366527
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_1_28_0.pkl

Elapsed Seconds: 246.176964998
Total Time Elapsed: 00:04:06
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.87      0.88     15422
         1.0       0.87      0.88      0.88     25683
         2.0       0.86      0.86      0.86     10287

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.87      0.87      0.87     51392
weighted avg       0.87      0.87      0.87     51392


Accuracy: 0.874104919054
## 2-kth fold --------------------------------
Training Time Elapsed: 00:03:52, 232.048311949
Training set score: 0.870460
Training set loss: 0.363976
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 462
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_2_28_0.pkl

Elapsed Seconds: 233.524328947
Total Time Elapsed: 00:03:53
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.81      0.86     15422
         1.0       0.85      0.89      0.87     25683
         2.0       0.82      0.87      0.84     10287

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.86      0.86      0.86     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.860523038605
## 3-kth fold --------------------------------
Training Time Elapsed: 00:04:02, 242.352706194
Training set score: 0.867581
Training set loss: 0.362320
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_3_28_0.pkl

Elapsed Seconds: 243.847934961
Total Time Elapsed: 00:04:03
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.82      0.88      0.85     15422
         1.0       0.82      0.87      0.84     25682
         2.0       0.91      0.67      0.77     10287

   micro avg       0.83      0.83      0.83     51391
   macro avg       0.85      0.81      0.82     51391
weighted avg       0.84      0.83      0.83     51391


Accuracy: 0.831059913214
## 4-kth fold --------------------------------
Training Time Elapsed: 00:04:00, 240.048962831
Training set score: 0.860795
Training set loss: 0.380863
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_4_28_0.pkl

Elapsed Seconds: 241.526321888
Total Time Elapsed: 00:04:01
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.88      0.82      0.85     15422
         1.0       0.85      0.86      0.85     25682
         2.0       0.82      0.87      0.85     10287

   micro avg       0.85      0.85      0.85     51391
   macro avg       0.85      0.85      0.85     51391
weighted avg       0.85      0.85      0.85     51391


Accuracy: 0.850109941429
Sorted List of Temp Training Scores: 
[[(27, 0), 0.868742162502181], [(26, 0), 0.8671854927927134]]

Sorted List of Temp Test Accuracies: 
[[(26, 0), 0.8573696407445489], [(27, 0), 0.8567236226958057]]

Average Training Accuracy for: (28, 0)
0.865849675165

Average Test Accuracy for: (28, 0)
0.854660963457

Elapsed Seconds: 1199.17094803
Total Time Elapsed: 00:19:59
# Num of nodes: 29, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:04:03, 243.180890083
Training set score: 0.872610
Training set loss: 0.360574
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_0_29_0.pkl

Elapsed Seconds: 244.614805222
Total Time Elapsed: 00:04:04
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.83      0.85     15422
         1.0       0.86      0.87      0.86     25683
         2.0       0.82      0.89      0.85     10287

   micro avg       0.86      0.86      0.86     51392
   macro avg       0.85      0.86      0.86     51392
weighted avg       0.86      0.86      0.86     51392


Accuracy: 0.858538293898
## 1-kth fold --------------------------------
Training Time Elapsed: 00:03:06, 186.170777082
Training set score: 0.869949
Training set loss: 0.362717
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 386
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_1_29_0.pkl

Elapsed Seconds: 187.654835939
Total Time Elapsed: 00:03:07
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.88      0.89     15422
         1.0       0.88      0.88      0.88     25683
         2.0       0.84      0.86      0.85     10287

   micro avg       0.88      0.88      0.88     51392
   macro avg       0.87      0.87      0.87     51392
weighted avg       0.88      0.88      0.88     51392


Accuracy: 0.875428082192
## 2-kth fold --------------------------------
Training Time Elapsed: 00:03:58, 238.146410942
Training set score: 0.859257
Training set loss: 0.381301
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_2_29_0.pkl

Elapsed Seconds: 239.590527058
Total Time Elapsed: 00:03:59
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.88      0.82      0.85     15422
         1.0       0.84      0.88      0.86     25683
         2.0       0.84      0.83      0.83     10287

   micro avg       0.85      0.85      0.85     51392
   macro avg       0.85      0.84      0.85     51392
weighted avg       0.85      0.85      0.85     51392


Accuracy: 0.853342932752
## 3-kth fold --------------------------------
Training Time Elapsed: 00:04:04, 244.812486887
Training set score: 0.871215
Training set loss: 0.353673
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_3_29_0.pkl

Elapsed Seconds: 246.20549798
Total Time Elapsed: 00:04:06
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.84      0.87      0.86     15422
         1.0       0.82      0.88      0.85     25682
         2.0       0.92      0.69      0.78     10287

   micro avg       0.84      0.84      0.84     51391
   macro avg       0.86      0.81      0.83     51391
weighted avg       0.85      0.84      0.84     51391


Accuracy: 0.840653032632
## 4-kth fold --------------------------------
Training Time Elapsed: 00:03:56, 236.747193098
Training set score: 0.861417
Training set loss: 0.369163
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_4_29_0.pkl

Elapsed Seconds: 238.140468836
Total Time Elapsed: 00:03:58
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.89      0.83      0.86     15422
         1.0       0.85      0.87      0.86     25682
         2.0       0.83      0.85      0.84     10287

   micro avg       0.86      0.86      0.86     51391
   macro avg       0.86      0.85      0.85     51391
weighted avg       0.86      0.86      0.86     51391


Accuracy: 0.856317253994
Sorted List of Temp Training Scores: 
[[(27, 0), 0.868742162502181], [(26, 0), 0.8671854927927134], [(28, 0), 0.8658496751645084]]

Sorted List of Temp Test Accuracies: 
[[(26, 0), 0.8573696407445489], [(27, 0), 0.8567236226958057], [(28, 0), 0.8546609634567817]]

Average Training Accuracy for: (29, 0)
0.866889726285

Average Test Accuracy for: (29, 0)
0.856855919094

Elapsed Seconds: 1157.30992699
Total Time Elapsed: 00:19:17
# Num of nodes: 30, 0 -----------------------------------------------*****
## 0-kth fold --------------------------------
Training Time Elapsed: 00:04:05, 245.721485138
Training set score: 0.878239
Training set loss: 0.344790
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_0_30_0.pkl

Elapsed Seconds: 247.196883917
Total Time Elapsed: 00:04:07
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.91      0.82      0.86     15422
         1.0       0.87      0.88      0.87     25683
         2.0       0.81      0.90      0.85     10287

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.86      0.87      0.86     51392
weighted avg       0.87      0.87      0.87     51392


Accuracy: 0.865231942715
## 1-kth fold --------------------------------
Training Time Elapsed: 00:03:27, 207.807758093
Training set score: 0.880817
Training set loss: 0.341925
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 433
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_1_30_0.pkl

Elapsed Seconds: 209.260793924
Total Time Elapsed: 00:03:29
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.91      0.88      0.89     15422
         1.0       0.88      0.90      0.89     25683
         2.0       0.87      0.86      0.86     10287

   micro avg       0.88      0.88      0.88     51392
   macro avg       0.88      0.88      0.88     51392
weighted avg       0.89      0.88      0.88     51392


Accuracy: 0.88482643213
## 2-kth fold --------------------------------
Training Time Elapsed: 00:04:00, 240.202529907
Training set score: 0.875490
Training set loss: 0.352891
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_2_30_0.pkl

Elapsed Seconds: 241.653509855
Total Time Elapsed: 00:04:01
Features in training dataset (80%):
Count: (205566,)
Counter({1.0: 102730, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51392,)
Counter({1.0: 25683, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.82      0.86     15422
         1.0       0.86      0.89      0.87     25683
         2.0       0.83      0.88      0.85     10287

   micro avg       0.87      0.87      0.87     51392
   macro avg       0.86      0.86      0.86     51392
weighted avg       0.87      0.87      0.87     51392


Accuracy: 0.865251400996
## 3-kth fold --------------------------------
Training Time Elapsed: 00:04:04, 244.067451954
Training set score: 0.872810
Training set loss: 0.353336
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 500
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_3_30_0.pkl

Elapsed Seconds: 245.471553087
Total Time Elapsed: 00:04:05
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.85      0.87      0.86     15422
         1.0       0.82      0.89      0.85     25682
         2.0       0.90      0.68      0.78     10287

   micro avg       0.84      0.84      0.84     51391
   macro avg       0.86      0.81      0.83     51391
weighted avg       0.85      0.84      0.84     51391


Accuracy: 0.84269619194
## 4-kth fold --------------------------------
Training Time Elapsed: 00:03:52, 232.450227022
Training set score: 0.870217
Training set loss: 0.361340
MLP # layers: 3
MLP # outputs: 3
MLP # iterations: 486
MLP out_activation_: softmax
MLP classes: [0. 1. 2.]
Classifier saved to /home/paperspace/MSEE/cs280/cs280-mini-proj/src/ANN/_old_grid_search_5_tests_v2/../../../models/ann_entry_3_26_to_30_0_v2/ann_exit_3_v2_4/ann_4_30_0.pkl

Elapsed Seconds: 233.867277861
Total Time Elapsed: 00:03:53
Features in training dataset (80%):
Count: (205567,)
Counter({1.0: 102731, 0.0: 61688, 2.0: 41148})
Features in test dataset (20%):
Count: (51391,)
Counter({1.0: 25682, 0.0: 15422, 2.0: 10287})
Classification Report for Test Data (20%):
              precision    recall  f1-score   support

         0.0       0.90      0.83      0.86     15422
         1.0       0.86      0.88      0.87     25682
         2.0       0.82      0.87      0.85     10287

   micro avg       0.86      0.86      0.86     51391
   macro avg       0.86      0.86      0.86     51391
weighted avg       0.86      0.86      0.86     51391


Accuracy: 0.862271603977
Sorted List of Temp Training Scores: 
[[(27, 0), 0.868742162502181], [(26, 0), 0.8671854927927134], [(29, 0), 0.866889726285297], [(28, 0), 0.8658496751645084]]

Sorted List of Temp Test Accuracies: 
[[(26, 0), 0.8573696407445489], [(29, 0), 0.8568559190935812], [(27, 0), 0.8567236226958057], [(28, 0), 0.8546609634567817]]

Average Training Accuracy for: (30, 0)
0.875514683335

Average Test Accuracy for: (30, 0)
0.864055514352

Elapsed Seconds: 1178.55903101
Total Time Elapsed: 00:19:38
List of Training Scores: 
[[(26, 0), 0.8671854927927134], [(27, 0), 0.868742162502181], [(28, 0), 0.8658496751645084], [(29, 0), 0.866889726285297], [(30, 0), 0.8755146833349672]]

List of Test Accuracies: 
[[(26, 0), 0.8573696407445489], [(27, 0), 0.8567236226958057], [(28, 0), 0.8546609634567817], [(29, 0), 0.8568559190935812], [(30, 0), 0.8640555143516341]]

Sorted List of Training Scores: 
[[(30, 0), 0.8755146833349672], [(27, 0), 0.868742162502181], [(26, 0), 0.8671854927927134], [(29, 0), 0.866889726285297], [(28, 0), 0.8658496751645084]]

Sorted List of Test Accuracies: 
[[(30, 0), 0.8640555143516341], [(26, 0), 0.8573696407445489], [(29, 0), 0.8568559190935812], [(27, 0), 0.8567236226958057], [(28, 0), 0.8546609634567817]]

Elapsed Seconds: 6654.53558302
Total Time Elapsed: 01:50:54
