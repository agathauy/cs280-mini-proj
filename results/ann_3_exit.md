<!-- TOC -->

- [EXIT (make use of entry calibrations except without traffic data)](#exit-make-use-of-entry-calibrations-except-without-traffic-data)
    - [Exit 0: MRT](#exit-0-mrt)
        - [From entry: (29, 0), 0.839122565777,  0.827243923594](#from-entry-29-0-0839122565777--0827243923594)
        - [5-fold result](#5-fold-result)
    - [Exit 1: MRT + Num of trains](#exit-1-mrt--num-of-trains)
        - [(29, 0) 0.836223225525,  0.822795861096](#29-0-0836223225525--0822795861096)
        - [5-fold result](#5-fold-result-1)
    - [Exit 2: MRT + Num of Trains + Weather](#exit-2-mrt--num-of-trains--weather)
        - [From entry: (28, 0) 0.857166350247,  0.847881648828](#from-entry-28-0-0857166350247--0847881648828)
        - [5-Fold Result](#5-fold-result)
    - [Exit 3: All features except traffic data (MRT + num of trains + weather + payday)](#exit-3-all-features-except-traffic-data-mrt--num-of-trains--weather--payday)
        - [From Entry(28, 0) 0.865003251546, 0.853376644866](#from-entry28-0-0865003251546-0853376644866)
        - [5-Fold result](#5-fold-result)
    - [Exit 4: All features except day of the month and traffic data](#exit-4-all-features-except-day-of-the-month-and-traffic-data)
        - [(28, 0), 0.8746954005597891 (28, 0), 0.8883192933474454](#28-0-08746954005597891-28-0-08883192933474454)
    - [Exit #5: MRT + Weather](#exit-5-mrt--weather)
        - [From entry: (29, 0) 0.843605737389,  0.83357980356](#from-entry-29-0-0843605737389--083357980356)
        - [5-fold result](#5-fold-result-2)
    - [Exit #6: MRT + Dates](#exit-6-mrt--dates)
        - [From entry: (27, 0) 0.817055637956, 0.800150431512](#from-entry-27-0-0817055637956-0800150431512)
        - [5-fold result](#5-fold-result-3)

<!-- /TOC -->

# EXIT (make use of entry calibrations except without traffic data)

## Exit 0: MRT
### From entry: (29, 0), 0.839122565777,  0.827243923594

### 5-fold result


Accuracy: 0.815609737114
Average Training Accuracy for: (29, 0)
0.839122565777

Average Test Accuracy for: (29, 0)
0.827243923594

Elapsed Seconds: 1659.9144001
Total Time Elapsed: 00:27:39



## Exit 1: MRT + Num of trains
### (29, 0) 0.836223225525,  0.822795861096


### 5-fold result
Accuracy: 0.844505847327
Average Training Accuracy for: (29, 0)
0.836223225525

Average Test Accuracy for: (29, 0)
0.822795861096

Elapsed Seconds: 1311.9881959
Total Time Elapsed: 00:21:51



## Exit 2: MRT + Num of Trains + Weather

### From entry: (28, 0) 0.857166350247,  0.847881648828

### 5-Fold Result
Accuracy: 0.860247903329
Average Training Accuracy for: (28, 0)
0.857166350247

Average Test Accuracy for: (28, 0)
0.847881648828

Elapsed Seconds: 1574.613518
Total Time Elapsed: 00:26:14



## Exit 3: All features except traffic data (MRT + num of trains + weather + payday)

###  From Entry(28, 0) 0.865003251546, 0.853376644866

### 5-Fold result

Accuracy: 0.846549006635
Average Training Accuracy for: (28, 0)
0.865003251546

Average Test Accuracy for: (28, 0)
0.853376644866

Elapsed Seconds: 1470.6748569
Total Time Elapsed: 00:24:30



## Exit 4: All features except day of the month and traffic data


Elapsed Seconds: 2785.35537505
Total Time Elapsed: 00:46:25s
List of Training Scores: 
[[(26, 0), 0.8755244039754402], [(27, 0), 0.883118050857974], [(28, 0), 0.8883192933474454], [(29, 0), 0.8866809034468062], [(30, 0), 0.8917254957335847]]
List of Test Accuracies: 
[[(26, 0), 0.8480099151414618], [(27, 0), 0.8471069920430073], [(28, 0), 0.8746954005597891], [(29, 0), 0.8690562730697501], [(30, 0), 0.8737729973706818]]
Sorted List of Training Scores: 
[[(30, 0), 0.8917254957335847], [(28, 0), 0.8883192933474454], [(29, 0), 0.8866809034468062], [(27, 0), 0.883118050857974], [(26, 0), 0.8755244039754402]]
Sorted List of Test Accuracies: 
[[(28, 0), 0.8746954005597891], [(30, 0), 0.8737729973706818], [(29, 0), 0.8690562730697501], [(26, 0), 0.8480099151414618], [(27, 0), 0.8471069920430073]]
Elapsed Seconds: 10625.0182271
Total Time Elapsed: 02:57:05

###  (28, 0), 0.8746954005597891 (28, 0), 0.8883192933474454





## Exit #5: MRT + Weather
### From entry: (29, 0) 0.843605737389,  0.83357980356

### 5-fold result
Accuracy: 0.862135393357
Average Training Accuracy for: (29, 0)
0.843605737389

Average Test Accuracy for: (29, 0)
0.83357980356

Elapsed Seconds: 1831.64610887
Total Time Elapsed: 00:30:31


## Exit #6: MRT + Dates

### From entry: (27, 0) 0.817055637956, 0.800150431512

### 5-fold result

Accuracy: 0.813430367185
Average Training Accuracy for: (27, 0)
0.817055637956

Average Test Accuracy for: (27, 0)
0.800150431512

Elapsed Seconds: 1336.54469299
Total Time Elapsed: 00:22:16
