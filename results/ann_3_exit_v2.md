<!-- TOC -->

- [Exit 0 (from entry 0)](#exit-0-from-entry-0)
        - [5-fold](#5-fold)
        - [(28, 0) 0.858784314063, 0.849664054064](#28-0-0858784314063-0849664054064)
- [Exit 1 (from entry 1)](#exit-1-from-entry-1)
        - [(27, 0) 0.850657488153, 0.839526260504](#27-0-0850657488153-0839526260504)
        - [5-fold](#5-fold-1)
        - [(27, 0), 0.8758707672255088, 0.8654720673206391](#27-0-08758707672255088-08654720673206391)
        - [5-fold](#5-fold-2)
- [Exit 2 (from entry 2)](#exit-2-from-entry-2)
        - [(29, 0) 0.857687825871, 0.84879624167](#29-0-0857687825871-084879624167)
        - [5-fold](#5-fold-3)
- [Exit 4](#exit-4)
        - [(27, 0), 0.8663205048078035, (27, 0), 0.8774576062068877](#27-0-08663205048078035-27-0-08774576062068877)
- [Exit 5 (from entry 6)](#exit-5-from-entry-6)
        - [(29, 0) 0.850749921223, 0.843215556875](#29-0-0850749921223-0843215556875)
        - [5-fold](#5-fold-4)
        - [(29, 0), 0.870492462520183, 0.8609849073495166](#29-0-0870492462520183-08609849073495166)
        - [5-fold](#5-fold-5)
- [Exit 6 (from entry 7)](#exit-6-from-entry-7)
        - [(27, 0) 0.857846379491, 0.844885209641](#27-0-0857846379491-0844885209641)
        - [5-fold](#5-fold-6)
        - [(27, 0), 0.8821966997360271,  0.871566463089018](#27-0-08821966997360271--0871566463089018)
        - [5-fold](#5-fold-7)

<!-- /TOC -->

# Exit 0 (from entry 0)

### 5-fold
Accuracy: 0.844019380825
Average Training Accuracy for: (28, 0)
0.858784314063

Average Test Accuracy for: (28, 0)
0.849664054064

Elapsed Seconds: 1692.05470896
Total Time Elapsed: 00:28:12

### (28, 0) 0.858784314063, 0.849664054064




# Exit 1 (from entry 1)

### (27, 0) 0.850657488153, 0.839526260504
### 5-fold

Accuracy: 0.852114183417
Average Training Accuracy for: (27, 0)
0.850657488153

Average Test Accuracy for: (27, 0)
0.839526260504

Elapsed Seconds: 1316.51797795
Total Time Elapsed: 00:21:56


### (27, 0), 0.8758707672255088, 0.8654720673206391

### 5-fold
Accuracy: 0.883792882022
Average Training Accuracy for: (27, 0)
0.900662788988

Average Test Accuracy for: (27, 0)
0.89737998252

Elapsed Seconds: 912.339545965
Total Time Elapsed: 00:15:12


# Exit 2 (from entry 2)


### (29, 0) 0.857687825871, 0.84879624167

### 5-fold

Average Training Accuracy for: (29, 0)
0.857687825871

Average Test Accuracy for: (29, 0)
0.84879624167

Elapsed Seconds: 1170.70267892
Total Time Elapsed: 00:19:30



# Exit 4 

Average Training Accuracy for: (30, 0)
0.880816108859

Average Test Accuracy for: (30, 0)
0.853777500306

Elapsed Seconds: 2365.97739816
Total Time Elapsed: 00:39:25
List of Training Scores: 
[[(26, 0), 0.8771705787883697], [(27, 0), 0.8774576062068877], [(28, 0), 0.8661950677012931], [(29, 0), 0.882648150418402], [(30, 0), 0.8808161088592155]]

List of Test Accuracies: 
[[(26, 0), 0.8522246565976623], [(27, 0), 0.8663205048078035], [(28, 0), 0.8504267689442596], [(29, 0), 0.8561668642833338], [(30, 0), 0.8537775003057531]]

Sorted List of Training Scores: 
[[(29, 0), 0.882648150418402], [(30, 0), 0.8808161088592155], [(27, 0), 0.8774576062068877], [(26, 0), 0.8771705787883697], [(28, 0), 0.8661950677012931]]

Sorted List of Test Accuracies: 
[[(27, 0), 0.8663205048078035], [(29, 0), 0.8561668642833338], [(30, 0), 0.8537775003057531], [(26, 0), 0.8522246565976623], [(28, 0), 0.8504267689442596]]

Elapsed Seconds: 7782.72305608
Total Time Elapsed: 02:09:42

### (27, 0), 0.8663205048078035, (27, 0), 0.8774576062068877


# Exit 5 (from entry 6)

### (29, 0) 0.850749921223, 0.843215556875

### 5-fold
Accuracy: 0.854390846646
Average Training Accuracy for: (29, 0)
0.850749921223

Average Test Accuracy for: (29, 0)
0.843215556875

Elapsed Seconds: 1422.52007508
Total Time Elapsed: 00:23:42



### (29, 0), 0.870492462520183, 0.8609849073495166

### 5-fold
Accuracy: 0.889455352104
Average Training Accuracy for: (29, 0)
0.884234972224

Average Test Accuracy for: (29, 0)
0.880700342226

Elapsed Seconds: 710.667086124
Total Time Elapsed: 00:11:50




# Exit 6 (from entry 7)
### (27, 0) 0.857846379491, 0.844885209641


### 5-fold

Accuracy: 0.871553384834
Average Training Accuracy for: (27, 0)
0.857846379491

Average Test Accuracy for: (27, 0)
0.844885209641

Elapsed Seconds: 1281.28344703
Total Time Elapsed: 00:21:21


### (27, 0), 0.8821966997360271,  0.871566463089018

### 5-fold
Accuracy: 0.898094997178
Average Training Accuracy for: (27, 0)
0.891629175489

Average Test Accuracy for: (27, 0)
0.88703992529

Elapsed Seconds: 934.477130175
Total Time Elapsed: 00:15:34
