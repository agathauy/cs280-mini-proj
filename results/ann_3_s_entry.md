<!-- TOC -->

- [Valid Entries](#valid-entries)
    - [Try na rin si 2 to 3 :))](#try-na-rin-si-2-to-3-)
    - [Try na rin si 2 :))](#try-na-rin-si-2-)
- [ENTRY: h1: 20 to 25, h2: 20 to 25](#entry-h1-20-to-25-h2-20-to-25)
    - [Entry 0: MRT](#entry-0-mrt)
        - [(23, 23) 0.9214511657450852,  0.8943560466168672](#23-23-09214511657450852--08943560466168672)
    - [Entry 1: - MRT + Num of Trains](#entry-1---mrt--num-of-trains)
        - [(23, 23), 0.9214511657450852,  0.8943560466168672](#23-23-09214511657450852--08943560466168672)
    - [Entry 2: - MRT + Num of Trains + Weather](#entry-2---mrt--num-of-trains--weather)
    - [Entry 3: - MRT + Num of Trains + Weather + Dates](#entry-3---mrt--num-of-trains--weather--dates)
    - [Entry 4: All features](#entry-4-all-features)
    - [Entry 5: All features except day of the month](#entry-5-all-features-except-day-of-the-month)
    - [Entry #6: MRT + Weather](#entry-6-mrt--weather)
    - [Entry #7: MRT + Dates](#entry-7-mrt--dates)
    - [Entry #8: MRT + Traffic data](#entry-8-mrt--traffic-data)

<!-- /TOC -->

# Valid Entries
Valid Entry Sets
0, 1, 4, 5, 6, 7, 8

## Try na rin si 2 to 3 :))

Valid Exit Sets
0, 1, 3, 4, 5, 6

## Try na rin si 2 :))


# ENTRY: h1: 20 to 25, h2: 20 to 25

## Entry 0: MRT
Sorted List of Training Scores: 
[[(24, 24), 0.9220582665304295], [(23, 23), 0.9214511657450852], [(25, 25), 0.91683562512807], [(21, 21), 0.8998464754798825], [(22, 22), 0.8997676626672872], [(20, 20), 0.8930243408522177]]

Sorted List of Test Accuracies: 
[[(25, 25), 0.8967923331813068], [(23, 23), 0.8943560466168672], [(21, 21), 0.8919433414858172], [(24, 24), 0.8918497642321622], [(22, 22), 0.8792446659614457], [(20, 20), 0.8789684139481226]]

Elapsed Seconds: 22197.4082031
Total Time Elapsed: 06:09:57

### (23, 23) 0.9214511657450852,  0.8943560466168672


## Entry 1: - MRT + Num of Trains
Sorted List of Training Scores: 
[[(24, 24), 0.9220582665304295], [(23, 23), 0.9214511657450852], [(25, 25), 0.91683562512807], [(21, 21), 0.8998464754798825], [(22, 22), 0.8997676626672872], [(20, 20), 0.8930243408522177]]

Sorted List of Test Accuracies: 
[[(25, 25), 0.8967923331813068], [(23, 23), 0.8943560466168672], [(21, 21), 0.8919433414858172], [(24, 24), 0.8918497642321622], [(22, 22), 0.8792446659614457], [(20, 20), 0.8789684139481226]]

Elapsed Seconds: 22454.6811001
Total Time Elapsed: 06:14:14

### (23, 23), 0.9214511657450852,  0.8943560466168672


## Entry 2: - MRT + Num of Trains + Weather

Sorted List of Training Scores: 
[[(24, 24), 0.9220582665304295], [(23, 23), 0.9214511657450852], [(25, 25), 0.91683562512807], [(21, 21), 0.8998464754798825], [(22, 22), 0.8997676626672872], [(20, 20), 0.8930243408522177]]

Sorted List of Test Accuracies: 
[[(25, 25), 0.8967923331813068], [(23, 23), 0.8943560466168672], [(21, 21), 0.8919433414858172], [(24, 24), 0.8918497642321622], [(22, 22), 0.8792446659614457], [(20, 20), 0.8789684139481226]]

Elapsed Seconds: 22508.9709539
Total Time Elapsed: 06:15:08


## Entry 3: - MRT + Num of Trains + Weather + Dates


## Entry 4: All features




## Entry 5: All features except day of the month

## Entry #6: MRT + Weather




## Entry #7: MRT + Dates



## Entry #8: MRT + Traffic data

