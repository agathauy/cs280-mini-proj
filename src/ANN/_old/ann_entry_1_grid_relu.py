import time
from datetime import timedelta
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import argparse
import joblib
import os
import sys
import datetime

from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score

from sklearn.externals import joblib

#import matplotlib.pyplot as plt
from sklearn.neural_network import MLPClassifier
from collections import Counter


SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))

def ann_entry_1_grid_relu():
    time_start = time.time()

    list_features = ['station', 'month', 'day', 'day_of_the_week', 'hour']
    folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "master_standardized")
    folder_name = "ann_entry_1_relu"
    result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "results", "ann", folder_name)
    if not os.path.isdir(result_folder_path):
        os.makedirs(result_folder_path)
    file_path = os.path.join(result_folder_path, "result.txt")
    write_file = open(file_path, 'w')

    str_a = "# {}".format(folder_name)
    print str_a
    write_file.write(str_a + '\n')

    str_a = str(list_features)
    print str_a
    write_file.write(str_a + '\n')


    # Model path
    model_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "models", "ann", folder_name)
    print model_folder_path

    if not os.path.isdir(model_folder_path):
        os.makedirs(model_folder_path)



    # Find the best parameters for the first fold
    i = 0
    list_scores = []
    for j in range(5, 11, 5):
        for k in range (0, 11, 5):
            str_a =  "Num of nodes: {}, {}".format(j, k)
            print str_a
            write_file.write(str_a + '\n')

            str_a =  "# {}-kth fold --------------------------------".format(i)
            print str_a
            write_file.write(str_a + '\n')
            # Load the files and selected features
            file_name = "{}_entry_X_train.csv".format(i)
            file_path = os.path.join(folder_path, file_name)
            X_train_temp = pd.read_csv(file_path, encoding="utf-8")
            X_train = X_train_temp[list_features].values

            file_name = "{}_entry_y_train.csv".format(i)
            file_path = os.path.join(folder_path, file_name)
            y_train = np.genfromtxt(file_path, delimiter=',', skip_header=1)


            file_name = "{}_entry_X_test.csv".format(i)
            file_path = os.path.join(folder_path, file_name)
            X_test_temp = pd.read_csv(file_path, encoding="utf-8")
            X_test = X_test_temp[list_features].values

            file_name = "{}_entry_y_test.csv".format(i)
            file_path = os.path.join(folder_path, file_name)
            y_test = np.genfromtxt(file_path, delimiter=',', skip_header=1)

            # Train with classifier
            if k > 0:
                hidden_layer_sizes = (j, k)
            else:
                hidden_layer_sizes = (j, )
            params = [
                {
                    "hidden_layer_sizes": hidden_layer_sizes,
                    "activation": 'relu',
                    "solver": 'adam',
                    "alpha": 0.0001,
                    "learning_rate": 'constant',
                    'learning_rate_init': 0.001,
                    'max_iter': 300,
                    'shuffle': True,
                    'early_stopping': False,
                    'tol': 1e-4,
                    'validation_fraction': 0.1,
                    'n_iter_no_change': 10,
                }
            ]

            clf = MLPClassifier(verbose=1, random_state=0,
                                **params[0])

            # SVC with parameters
            clf.fit(X_train, y_train)
            elapsed = time.time() - time_start
            str_a = 'Training Time Elapsed: {}, {}'.format(time.strftime("%H:%M:%S", time.gmtime(elapsed)), elapsed)
            print str_a
            write_file.write(str_a)
            write_file.write("\n")

            training_score =  clf.score(X_train, y_train)
            list_scores.append((j, training_score))
            
            str_a = "Training set score: %f" % training_score
            print str_a
            write_file.write(str_a + '\n')
            str_a = "Training set loss: %f" % clf.loss_
            print str_a
            write_file.write(str_a + '\n')
            str_a = "MLP # layers: {}".format(clf.n_layers_)
            print str_a
            write_file.write(str_a + '\n')
            str_a = "MLP # outputs: {}".format(clf.n_outputs_)
            print str_a
            write_file.write(str_a + '\n')
            str_a = "MLP # iterations: {}".format(clf.n_iter_)
            print str_a
            write_file.write(str_a + '\n')
            str_a = "MLP out_activation_: {}".format(clf.out_activation_)
            print str_a
            write_file.write(str_a + '\n')
            str_a = "MLP classes: {}".format(clf.classes_)
            print str_a
            write_file.write(str_a + '\n')


            plt.figure()
            plt.plot(clf.loss_curve_)
            plt.title('Training Loss Curve')
            plt.xlabel('epochs')
            plt.ylabel('loss')
            plt.grid(True)
            plt.tight_layout()
            file_name = "training_loss_curve_{}_{}_{}.png".format(i, j, k)
            file_path = os.path.join(result_folder_path, file_name)
            plt.savefig(file_path)


            file_name = "ann_{}_{}_{}.pkl".format(i, j, k)
            file_path = os.path.join(model_folder_path, file_name)
            joblib.dump(clf, file_path)
            str_a =  "Classifier saved to {}\n".format(file_path)
            print str_a
            write_file.write(str_a + '\n')

            # Evaluate on validation set
            test_predict = clf.predict(X_test)


            len_train = y_train.shape
            print_train = (
                "Features in training dataset (80%):\n"
                "Count: {}\n"
                "{}\n"
            ).format(len_train, Counter(y_train))

            len_test = y_test.shape
            print_cv = (
                "Features in test dataset (20%):\n"
                "Count: {}\n"
                "{}\n"
            ).format(len_test, Counter(y_test))

            write_file.write(print_train)
            print print_train
            write_file.write(print_cv)
            print print_cv

            str_a = "Classification Report for Test Data (20%):\n"
            print str_a
            write_file.write(str_a)
            str_a =  (classification_report(
                y_test, test_predict))
            print str_a
            write_file.write(str_a + '\n')

            str_a = "\nAccuracy: {}".format(accuracy_score(y_test, test_predict))
            print str_a
            write_file.write(str_a + '\n')


    # Print execution time

    str_a =  list_scores
    print str_a
    write_file.write(str_a + '\n')


    elapsed = time.time() - time_start
    str_a =  "Elapsed Seconds: {}".format(elapsed)
    print str_a
    write_file.write(str_a + '\n')

    str_a = 'Total Time Elapsed: {}'.format(time.strftime("%H:%M:%S", time.gmtime(elapsed)))
    print str_a
    write_file.write(str_a + '\n')

    write_file.close()


if __name__ == '__main__':
    ann_entry_1_grid_relu()