import time
from datetime import timedelta
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import argparse
import joblib
import os
import sys
import datetime

from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score

from sklearn.externals import joblib

#import matplotlib.pyplot as plt
from sklearn.neural_network import MLPClassifier
from collections import Counter
from operator import itemgetter



SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))

def ann_exit_grid_full():
    time_start = time.time()


    list_features = [
        ['station', 'month', 'day', 'day_of_the_week', 'hour'],
        ['station','month','day','day_of_the_week','hour','num_trains'],
        ['station','month','day','day_of_the_week','hour','num_trains','avg_air_pressure'],
        ['station','month','day','day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type'],
        ['station','month','day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type']
    ]


    # Going through each experiment set or list of features
    num_sets = len(list_features)
    for m in range (num_sets):


        folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "master_standardized")
        folder_name = "ann_exit_full_{}".format(m + 1)
        result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "results", "ann", folder_name)
        if not os.path.isdir(result_folder_path):
            os.makedirs(result_folder_path)
        file_path = os.path.join(result_folder_path, "result.txt")
        write_file = open(file_path, 'w')

        str_a = "# {}".format(folder_name)
        print str_a
        write_file.write(str_a + '\n')

        str_a = str(list_features[m])
        print str_a
        write_file.write(str_a + '\n')


        # Model path
        model_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "models", "ann", folder_name)
        print model_folder_path

        if not os.path.isdir(model_folder_path):
            os.makedirs(model_folder_path)



        # Find the best parameters for the first fold division


        list_scores = []
        list_test_accuracies = []

        # Going through each # of nodes combination
        #for j in range(5, 51, 5):
        #    for k in range (0, 51, 5):
        for j in range(2, 31, 1):
            for k in range(0, 31, 1):
                # Go through each of the 5-folds
                temp_list_scores = []
                temp_list_test_accuracies = []
                str_a =  "# Num of nodes: {}, {} -----------------------------------------------*****".format(j, k)
                print str_a
                write_file.write(str_a + '\n')

                # Start plotting
                fig, ax = plt.subplots()

                for i in range(5):


                    str_a =  "## {}-kth fold --------------------------------".format(i)
                    print str_a
                    write_file.write(str_a + '\n')
                    # Load the files and selected features
                    file_name = "{}_exit_X_train.csv".format(i)
                    file_path = os.path.join(folder_path, file_name)
                    X_train_temp = pd.read_csv(file_path, encoding="utf-8")
                    X_train = X_train_temp[list_features[m]].values

                    file_name = "{}_exit_y_train.csv".format(i)
                    file_path = os.path.join(folder_path, file_name)
                    y_train = np.genfromtxt(file_path, delimiter=',', skip_header=1)


                    file_name = "{}_exit_X_test.csv".format(i)
                    file_path = os.path.join(folder_path, file_name)
                    X_test_temp = pd.read_csv(file_path, encoding="utf-8")
                    X_test = X_test_temp[list_features[m]].values

                    file_name = "{}_exit_y_test.csv".format(i)
                    file_path = os.path.join(folder_path, file_name)
                    y_test = np.genfromtxt(file_path, delimiter=',', skip_header=1)

                    # Train with classifier
                    if k > 0:
                        hidden_layer_sizes = (j, k)
                    else:
                        hidden_layer_sizes = (j, )
                    params = [
                        {
                            "hidden_layer_sizes": hidden_layer_sizes,
                            "activation": 'logistic',
                            "solver": 'adam',
                            "alpha": 0.0001,
                            "learning_rate": 'constant',
                            'learning_rate_init': 0.001,
                            'max_iter': 500,
                            'shuffle': True,
                            'early_stopping': False,
                            'tol': 1e-4,
                            'validation_fraction': 0.1,
                            'n_iter_no_change': 10,
                        }
                    ]

                    clf = MLPClassifier(verbose=1, random_state=0,
                                        **params[0])

                    # SVC with parameters
                    clf.fit(X_train, y_train)
                    elapsed = time.time() - time_start
                    str_a = 'Training Time Elapsed: {}, {}'.format(time.strftime("%H:%M:%S", time.gmtime(elapsed)), elapsed)
                    print str_a
                    write_file.write(str_a)
                    write_file.write("\n")

                    training_score =  clf.score(X_train, y_train)
                    temp = [(j,k), training_score]
                    temp_list_scores.append(temp)

                    str_a = "Training set score: %f" % training_score
                    print str_a
                    write_file.write(str_a + '\n')
                    str_a = "Training set loss: %f" % clf.loss_
                    print str_a
                    write_file.write(str_a + '\n')
                    str_a = "MLP # layers: {}".format(clf.n_layers_)
                    print str_a
                    write_file.write(str_a + '\n')
                    str_a = "MLP # outputs: {}".format(clf.n_outputs_)
                    print str_a
                    write_file.write(str_a + '\n')
                    str_a = "MLP # iterations: {}".format(clf.n_iter_)
                    print str_a
                    write_file.write(str_a + '\n')
                    str_a = "MLP out_activation_: {}".format(clf.out_activation_)
                    print str_a
                    write_file.write(str_a + '\n')
                    str_a = "MLP classes: {}".format(clf.classes_)
                    print str_a
                    write_file.write(str_a + '\n')


                    # Plot over plot
                    label = "{}-fold".format(i + 1)
                    ax.plot(clf.loss_curve_, label=label)


                    file_name = "ann_{}_{}_{}.pkl".format(i, j, k)
                    file_path = os.path.join(model_folder_path, file_name)
                    joblib.dump(clf, file_path)
                    str_a =  "Classifier saved to {}\n".format(file_path)
                    print str_a
                    write_file.write(str_a + '\n')

                    # Evaluate on validation set
                    test_predict = clf.predict(X_test)


                    len_train = y_train.shape
                    print_train = (
                        "Features in training dataset (80%):\n"
                        "Count: {}\n"
                        "{}\n"
                    ).format(len_train, Counter(y_train))

                    len_test = y_test.shape
                    print_cv = (
                        "Features in test dataset (20%):\n"
                        "Count: {}\n"
                        "{}\n"
                    ).format(len_test, Counter(y_test))

                    write_file.write(print_train)
                    print print_train
                    write_file.write(print_cv)
                    print print_cv

                    str_a = "Classification Report for Test Data (20%):\n"
                    print str_a
                    write_file.write(str_a)
                    str_a =  (classification_report(
                        y_test, test_predict))
                    print str_a
                    write_file.write(str_a + '\n')

                    test_accuracy = accuracy_score(y_test, test_predict)

                    str_a = "\nAccuracy: {}".format(test_accuracy)
                    temp = [(j, k),test_accuracy]
                    temp_list_test_accuracies.append(temp)
                    print str_a
                    write_file.write(str_a + '\n')




                # End plotting
                ax.legend(loc='upper right')
                if k == 0:
                    title = 'H1:{} Training Loss Curve'.format(j)
                else:
                    title = 'H1:{} H2: {} Training Loss Curve'.format(j, k)
                plt.title(title)
                plt.xlabel('epochs')
                plt.ylabel('loss')
                plt.grid(True)
                plt.tight_layout()
                file_name = "training_loss_curve_{}_{}.png".format(j, k)
                file_path = os.path.join(result_folder_path, file_name)
                plt.savefig(file_path)



                str_a = "Sorted List of Temp Training Scores: "
                print str_a
                write_file.write(str_a + '\n')
                str_a = str(sorted(list_scores,key=itemgetter(1), reverse=True))
                print str_a
                write_file.write(str_a + '\n\n')

                str_a = "Sorted List of Temp Test Accuracies: "
                print str_a
                write_file.write(str_a + '\n')

                str_a = str(sorted(list_test_accuracies,key=itemgetter(1), reverse=True))
                print str_a
                write_file.write(str_a + '\n\n')


                # Get average training accuracy
                str_a = "Average Training Accuracy for: ({}, {})".format(j, k)
                print str_a
                write_file.write(str_a + '\n')

                mean = 0
                num_list = len(temp_list_scores)
                for p in range(num_list):
                    mean = mean + temp_list_scores[p][1]
                mean = float(mean)/num_list

                temp = [(j, k), mean]
                list_scores.append(temp)
                
                str_a = str(mean)
                print str_a
                write_file.write(str_a + '\n\n')


                # Get average test accuracy
                str_a = "Average Test Accuracy for: ({}, {})".format(j, k)
                print str_a
                write_file.write(str_a + '\n')

                mean = 0
                num_list = len(temp_list_test_accuracies)
                for p in range(num_list):
                    mean = mean + temp_list_test_accuracies[p][1]
                mean = float(mean)/num_list

                temp = [(j, k), mean]
                list_test_accuracies.append(temp)

                str_a = str(mean)
                print str_a
                write_file.write(str_a + '\n\n')

        # Print execution time

        str_a = "List of Training Scores: "
        print str_a
        write_file.write(str_a + '\n')
        str_a = str(list_scores)
        print str_a
        write_file.write(str_a + '\n\n')

        str_a = "List of Test Accuracies: "
        print str_a
        write_file.write(str_a + '\n')

        str_a = str(list_test_accuracies)
        print str_a
        write_file.write(str_a + '\n\n')


        str_a = "Sorted List of Training Scores: "
        print str_a
        write_file.write(str_a + '\n')
        str_a = str(sorted(list_scores,key=itemgetter(1), reverse=True))
        print str_a
        write_file.write(str_a + '\n\n')

        str_a = "Sorted List of Test Accuracies: "
        print str_a
        write_file.write(str_a + '\n')

        str_a = str(sorted(list_test_accuracies,key=itemgetter(1), reverse=True))
        print str_a
        write_file.write(str_a + '\n\n')

        #sorted(data,key=itemgetter(1), reverse=True)


        elapsed = time.time() - time_start
        str_a =  "Elapsed Seconds: {}".format(elapsed)
        print str_a
        write_file.write(str_a + '\n')

        str_a = 'Total Time Elapsed: {}'.format(time.strftime("%H:%M:%S", time.gmtime(elapsed)))
        print str_a
        write_file.write(str_a + '\n')

        write_file.close()

if __name__ == '__main__':
    ann_exit_grid_full()