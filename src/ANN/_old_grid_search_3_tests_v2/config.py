



entry_master_list_features = [
    ['station', 'month', 'day_of_the_week', 'hour'],
    ['station','month','day_of_the_week','hour','num_trains'],
    ['station','month','day_of_the_week','hour','num_trains','avg_air_pressure', 'temperature'],
    ['station','month','day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type'],
    ['station','month','day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type','traffic_type'],
    ['station','month','day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type','traffic_type'],
    ['station','month','day_of_the_week','hour','avg_air_pressure', 'temperature'],
    ['station', 'month', 'day_of_the_week', 'hour', 'holiday_type', 'payday_type'],
    ['station', 'month', 'day_of_the_week', 'hour', 'traffic_type']
]

exit_master_list_features = [
    ['station', 'month',  'day_of_the_week', 'hour'],
    ['station','month','day_of_the_week','hour','num_trains'],
    ['station','month','day_of_the_week','hour','num_trains','avg_air_pressure', 'temperature'],
    ['station','month', 'day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type'],
    ['station','month','day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type'],
    ['station','month','day_of_the_week','hour','avg_air_pressure', 'temperature'],
    ['station', 'month',  'day_of_the_week', 'hour', 'holiday_type', 'payday_type']
]