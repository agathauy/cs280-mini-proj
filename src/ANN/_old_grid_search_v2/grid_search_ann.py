import time
from datetime import timedelta
import numpy as np
import pandas as pd
#import matplotlib.pyplot as plt
import argparse
import joblib
import os
import sys
import datetime

from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score

from sklearn.svm import LinearSVC
from sklearn.externals import joblib

#import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier


#import utilities
SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))

def grid_search_ann(num_set, flow_type):


    master_list_features = []

    if flow_type == "entry":


        master_list_features = [
            ['station', 'month', 'day', 'day_of_the_week', 'hour'],
            ['station','month','day','day_of_the_week','hour','num_trains'],
            ['station','month','day','day_of_the_week','hour','num_trains','avg_air_pressure'],
            ['station','month','day','day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type'],
            ['station','month','day','day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type','traffic_type'],
            ['station','month','day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type','traffic_type']

        ]
    elif flow_type == "exit":
        master_list_features = [
            ['station', 'month', 'day', 'day_of_the_week', 'hour'],
            ['station','month','day','day_of_the_week','hour','num_trains'],
            ['station','month','day','day_of_the_week','hour','num_trains','avg_air_pressure'],
            ['station','month','day','day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type'],
            ['station','month','day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type']
        ]

    list_features = master_list_features[num_set]


    time_start = time.time()
    # Load dataset
    folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "master_standardized")
    file_name = "{}_X_train.csv".format(flow_type)
    file_path = os.path.join(folder_path, file_name)
    # station,month,day,day_of_the_week,hour,num_trains,avg_air_pressure,temperature,holiday_type,payday_type,traffic_type

    #column_types = [ ('station', 'float_'), ('month', 'float_'),('day', 'float_'), ('day_of_the_week', float), ('hour', 'float_'),('num_trains', 'float_'), ('avg_air_pressure', 'float_'), ('temperature', 'float_'), ('holiday_type', 'float_'), ('payday_type', 'float_'), ('traffic_type', 'float_') ]

    #X_temp = np.genfromtxt(file_path, delimiter=',', dtype=column_types)
    X_temp = pd.read_csv(file_path, encoding="utf-8")
    X = X_temp[list_features].values



    #print X[0, :]
    #print X.shape
    
    
    #print X.head()
    file_name = "{}_y_train.csv".format(flow_type)
    file_path = os.path.join(folder_path, file_name)
    Y = np.genfromtxt(file_path, delimiter=',', skip_header=1)

    folder_name = "grid_search_ann_{}_{}".format(flow_type,num_set)
    result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "analysis", folder_name)
    if not os.path.isdir(result_folder_path):
        os.makedirs(result_folder_path)
    file_name = "result.txt"
    file_path = os.path.join(result_folder_path, file_name)
    write_file = open(file_path, 'w')
    write_file.truncate()



    str_a = "##### Flow Type: {}, Num Set: {} ####################################".format(flow_type, num_set)
    print str_a
    write_file.write(str_a + '\n')
    # For saving models

    folder_name = "grid_search_ann_{}_{}".format(flow_type, num_set)
    model_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "models", "ann", folder_name)
    if not os.path.isdir(model_folder_path):
        os.makedirs(model_folder_path)


    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=5, stratify=Y)


    metrics = ['accuracy']

    # Parameter grid for cross-validation
    hidden_layer_sizes = []
    for j in range(0, 15):
        for i in range(2, 15):
            if j == 0:

                temp = (i, )
            else:
                temp = (i, j, )
            hidden_layer_sizes.append(temp)


    parameter_grid = [
        {
            "hidden_layer_sizes": (3, ),
        }
    ]

    params = [{
        "activation": 'logistic',
        "solver": 'adam',
        "alpha": 0.0001,
        "learning_rate": 'constant',
        'learning_rate_init': 0.001,
        'max_iter': 500,
        'shuffle': True,
        'early_stopping': False,
        'tol': 1e-4,
        'validation_fraction': 0.1,
        'n_iter_no_change': 10,
    }]

    for metric in metrics:
        str_a = "\n#### Searching optimal hyperparameters for: {}".format(metric)
        print str_a
        write_file.write(str_a + '\n')

        clf = GridSearchCV(MLPClassifier(verbose=1, random_state=0, **params[0]), 
                parameter_grid, cv=5, scoring=metric, verbose=1)
        clf.fit(X_train, Y_train.ravel())


        str_a = "Best parameters set found on development set:"
        print str_a
        write_file.write(str_a + '\n')
        str_a = str(clf.best_params_)
        print str_a
        write_file.write(str_a + '\n')
        str_a = ""
        print str_a
        write_file.write(str_a + '\n')


        str_a = "Grid scores on development set:"
        print str_a
        write_file.write(str_a + '\n')

        means = clf.cv_results_['mean_test_score']
        stds = clf.cv_results_['std_test_score']
        for mean, std, params in zip(means, stds, clf.cv_results_['params']):
            str_a = "{}, {} for {}".format(mean, std * 2, params)
            print str_a
            write_file.write(str_a + '\n')

        str_a = ""
        print str_a
        write_file.write(str_a + '\n')


        file_name = "model.pkl"
        file_path = os.path.join(model_folder_path, file_name)
        joblib.dump(clf, file_path)


        Y_test, Y_pred = Y_test, clf.predict(X_test)
        str_a =  "\nFull performance report:\n"
        print str_a
        write_file.write(str_a + '\n')

        str_a = classification_report(Y_test, Y_pred)
        print str_a
        write_file.write(str_a + '\n')


    # End of function
    # Print execution time
    elapsed = time.time() - time_start
    str_a =  "Elapsed Seconds: {}".format(elapsed)
    print str_a
    write_file.write(str_a + '\n')

    str_a = 'Total Time Elapsed: {}'.format(time.strftime("%H:%M:%S", time.gmtime(elapsed)))
    print str_a
    write_file.write(str_a + '\n')

    write_file.close()

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument(
        "-n", "--set_number", help="set number from master_list_features",
        required=True
    )

    ap.add_argument(
        "-t", "--flow_type", help="entry or exit",
        required=True
    )

    args = vars(ap.parse_args())
    num_set = int(args["set_number"])
    flow_type = args["flow_type"]
    print num_set
    print flow_type


    grid_search_ann(num_set, flow_type)

