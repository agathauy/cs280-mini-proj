import time
from datetime import timedelta
import numpy as np
import pandas as pd
#import matplotlib.pyplot as plt
import argparse
import joblib
import os
import sys
import datetime

from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score

from sklearn.svm import LinearSVC
from sklearn.externals import joblib

#import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split

#import utilities
SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))

def grid_search_svm(list_features, experiment_name, flow_type):
    time_start = time.time()
    # Load dataset
    folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "master_standardized_3")
    file_name = "0_{}_X_train.csv".format(flow_type)
    file_path = os.path.join(folder_path, file_name)
    # station,month,day,day_of_the_week,hour,num_trains,avg_air_pressure,temperature,holiday_type,payday_type,traffic_type

    #column_types = [ ('station', 'float_'), ('month', 'float_'),('day', 'float_'), ('day_of_the_week', float), ('hour', 'float_'),('num_trains', 'float_'), ('avg_air_pressure', 'float_'), ('temperature', 'float_'), ('holiday_type', 'float_'), ('payday_type', 'float_'), ('traffic_type', 'float_') ]

    #X_temp = np.genfromtxt(file_path, delimiter=',', dtype=column_types)
    X_temp = pd.read_csv(file_path, encoding="utf-8")
    print X_temp.head()
    X_1 = X_temp[list_features]
    print X_1.head()
    X  = X_1.values
    print X[0, :]
    print X.shape
    
    
    #print X.head()
    file_name = "0_{}_y_train.csv".format(flow_type)
    file_path = os.path.join(folder_path, file_name)
    Y = np.genfromtxt(file_path, delimiter=',', skip_header=1)


    result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "analysis", "grid_search_svm_3", experiment_name)
    if not os.path.isdir(result_folder_path):
        os.makedirs(result_folder_path)
    file_name = "result.txt"
    file_path = os.path.join(result_folder_path, file_name)
    write_file = open(file_path, 'w')
    write_file.truncate()



    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=5, stratify=Y)


    metrics = ['accuracy', 'f1_weighted']

    # Parameter grid for cross-validation
    parameter_grid = [  
         {'kernel': ['linear'], 'C': [1, 10, 50, 600]},
        {'kernel': ['poly'], 'degree': [2, 3]},
        {'kernel': ['rbf'], 'gamma': [0.01, 0.001], 'C': [1, 10, 50, 600]}
       
    ]


    for metric in metrics:
        str_a = "\n#### Searching optimal hyperparameters for: {}".format(metric)
        print str_a
        write_file.write(str_a + '\n')

        clf = GridSearchCV(svm.SVC(C=1), 
                parameter_grid, cv=5, scoring=metric, verbose=2)
        clf.fit(X_train, Y_train.ravel())


        str_a = "Best parameters set found on development set:"
        print str_a
        write_file.write(str_a + '\n')
        str_a = str(clf.best_params_)
        print str_a
        write_file.write(str_a + '\n')
        str_a = ""
        print str_a
        write_file.write(str_a + '\n')


        str_a = "Grid scores on development set:"
        print str_a
        write_file.write(str_a + '\n')

        means = clf.cv_results_['mean_test_score']
        stds = clf.cv_results_['std_test_score']
        for mean, std, params in zip(means, stds, clf.cv_results_['params']):
            str_a = "%0.3f (+/-%0.03f) for %r".format(mean, std * 2, params)
            print str_a
            write_file.write(str_a + '\n')

        str_a = ""
        print str_a
        write_file.write(str_a + '\n')

        Y_test, Y_pred = Y_test, clf.predict(X_test)
        str_a =  "\nFull performance report:\n"
        print str_a
        write_file.write(str_a + '\n')

        str_a = classification_report(Y_test, Y_pred)
        print str_a
        write_file.write(str_a + '\n')


    # End of function
    # Print execution time
    elapsed = time.time() - time_start
    str_a =  "Elapsed Seconds: {}".format(elapsed)
    print str_a
    write_file.write(str_a + '\n')

    str_a = 'Total Time Elapsed: {}'.format(time.strftime("%H:%M:%S", time.gmtime(elapsed)))
    print str_a
    write_file.write(str_a + '\n')

    write_file.close()

if __name__ == '__main__':

    ap = argparse.ArgumentParser()
    ap.add_argument(
        '-p', "--list_features", help="list of features to be used: station,month,day,day_of_the_week,hour,num_trains,avg_air_pressure,temperature,holiday_type,payday_type,traffic_type",
        nargs="*",
        required=True
    )
    ap.add_argument(
        "-n", "--experiment_name", help="name of the experiment",
        required=True
    )
    ap.add_argument(
        "-t", "--flow_type", help="entry or exit",
        required=True
    )

    args = vars(ap.parse_args())
    experiment_name= str(args["experiment_name"])
    list_features = args["list_features"]
    flow_type = args["flow_type"]
    print experiment_name
    print list_features
    print flow_type

    grid_search_svm(list_features, experiment_name, flow_type)

