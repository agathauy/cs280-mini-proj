import time
from datetime import timedelta
import numpy as np
import pandas as pd
#import matplotlib.pyplot as plt
import argparse
import joblib
import os
import sys
import datetime

from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score

from sklearn.svm import LinearSVC
from sklearn.externals import joblib

#import matplotlib.pyplot as plt
from sklearn.svm import SVC

SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))


def svm_entry_1():
    time_start = time.time()

    list_features = ['station', 'month', 'day', 'day_of_the_week', 'hour']
    folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "master_standardized")
    folder_name = "svm_entry_1"
    result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "svm", folder_name)
    if not os.path.isdir(result_folder_path):
        os.makedirs(result_folder_path)
    file_path = os.path.join(result_folder_path, "result.txt")
    write_file = open(file_path, 'w')

    str_a = "# {}".format(folder_name)
    print str_a
    write_file.write(str_a + '\n')

    str_a = str(list_features)
    print str_a
    write_file.write(str_a + '\n')


    # Model path
    model_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "models", "svm", folder_name)
    print model_folder_path

    if not os.path.isdir(model_folder_path):
        os.makedirs(model_folder_path)


    # Go through each of the 5 folds
    for i in range(5):
        print "# {}-kth fold --------------------------------".format(i)
        # Load the files and selected features
        file_name = "{}_entry_X_train.csv".format(i)
        file_path = os.path.join(folder_path, file_name)
        X_train_temp = pd.read_csv(file_path, encoding="utf-8")
        X_train = X_train_temp[list_features].values

        file_name = "{}_entry_y_train.csv".format(i)
        file_path = os.path.join(folder_path, file_name)
        y_train = np.genfromtxt(file_path, delimiter=',', skip_header=1)


        file_name = "{}_entry_X_test.csv".format(i)
        file_path = os.path.join(folder_path, file_name)
        X_test_temp = pd.read_csv(file_path, encoding="utf-8")
        X_test = X_test_temp[list_features].values

        file_name = "{}_entry_y_test.csv".format(i)
        file_path = os.path.join(folder_path, file_name)
        y_test = np.genfromtxt(file_path, delimiter=',', skip_header=1)

        # Train with classifier

        # SVC with parameters
        clf = SVC(gamma='auto')
        clf.fit(X_train, y_train)
        elapsed = time.time() - time_start
        str_a = 'Training Time Elapsed: {}, {}'.format(time.strftime("%H:%M:%S", time.gmtime(elapsed)), elapsed)
        print str_a
        write_file.write(str_a)
        write_file.write("\n")

        file_name = "svm{}.pkl".format(i)
        file_path = os.path.join(model_folder_path, file_name)
        joblib.dump(clf, file_path)
        str_a =  "Classifier saved to {}\n".format(file_path)
        print str_a
        write_file.write(str_a)

        # Evaluate on validation set
        test_predict = clf.predict(X_test)


        len_train = y_train.shape
        print_train = (
            "Features in training dataset (80%):\n"
            "Count: {}\n"
            "{}\n"
        ).format(len_train, Counter(y_train))

        len_test = y_test.shape
        print_cv = (
            "Features in test dataset (20%):\n"
            "Count: {}\n"
            "{}\n"
        ).format(len_test, Counter(y_test))

        write_file.write(print_train)
        print print_train
        write_file.write(print_cv)
        print print_cv

        str_a = "Classification Report for Test Data (20%):\n"
        print str_a
        write_file.write(str_a)
        str_a =  (classification_report(
            y_test, test_predict))
        print str_a
        write_file.write(str_a)

        str_a = "\nAccuracy: {}".format(accuracy_score(y_test, test_predict))
        print str_a
        write_file.write(str_a + '\n')


    # Print execution time
    elapsed = time.time() - time_start
    str_a =  "Elapsed Seconds: {}".format(elapsed)
    print str_a
    write_file.write(str_a + '\n')

    str_a = 'Total Time Elapsed: {}'.format(time.strftime("%H:%M:%S", time.gmtime(elapsed)))
    print str_a
    write_file.write(str_a + '\n')

    write_file.close()





if __name__ == '__main__':
    svm_entry_1()
