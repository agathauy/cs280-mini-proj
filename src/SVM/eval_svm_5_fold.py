import time
from datetime import timedelta
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import argparse
import os
import sys
import datetime

from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score

from sklearn.externals import joblib

#import matplotlib.pyplot as plt
from sklearn.neural_network import MLPClassifier
from collections import Counter
from operator import itemgetter
from sklearn import svm


SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))

def eval_svm_5_fold(num_set, flow_type, dataset_folder):
    time_start = time.time()


    # insert paramters


    if flow_type == "entry":

        master_list_parameters = [
            {'kernel': 'linear', 'C': 1},
            {'kernel': 'linear', 'C': 1},
            {'kernel': 'linear', 'C': 1},
            {'kernel': 'linear', 'C': 1},
            {'kernel': 'linear', 'C': 1},
            {'kernel': 'linear', 'C': 1},
        ]

        master_list_features = [
            ['station', 'month', 'day', 'day_of_the_week', 'hour'],
            ['station','month','day','day_of_the_week','hour','num_trains'],
            ['station','month','day','day_of_the_week','hour','num_trains','avg_air_pressure', 'temperature'],
            ['station','month','day','day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type'],
            ['station','month','day','day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type','traffic_type'],
            ['station','month','day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type','traffic_type']

        ]
    elif flow_type == "exit":


        master_list_parameters = [
            {},
            {},
            {},
            {},
            {},
            {}
        ]

        master_list_features = [
            ['station', 'month', 'day', 'day_of_the_week', 'hour'],
            ['station','month','day','day_of_the_week','hour','num_trains'],
            ['station','month','day','day_of_the_week','hour','num_trains','avg_air_pressure', 'temperature'],
            ['station','month','day','day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type'],
            ['station','month','day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type']
        ]

    list_features = master_list_features[num_set]


    folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", dataset_folder)
    if dataset_folder == 'master_standardized_3':
        root_result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "results", "eval_svm_5_fold_3")
    elif dataset_folder == 'master_standardized':
        root_result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "results", "eval_svm_5_fold")

    folder_name = "eval_svm_{}_{}".format(flow_type, num_set)

    root_result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "results", "eval_svm_5_fold_3")
    if not os.path.isdir(root_result_folder_path):
        os.makedirs(root_result_folder_path)
    result_folder_path = os.path.join(root_result_folder_path, folder_name)
    if not os.path.isdir(result_folder_path):
        os.makedirs(result_folder_path)
    file_path = os.path.join(result_folder_path, "result.txt")
    write_file = open(file_path, 'w')



    # Model path
    if dataset_folder == 'master_standardized_3':
        root_model_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "models", "eval_svm_5_fold_3")
    elif dataset_folder == 'master_standardized':
        root_model_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "models", "eval_svm_5_fold")

    if not os.path.isdir(root_model_folder_path):
        os.makedirs(root_model_folder_path)
    model_folder_path = os.path.join(root_model_folder_path, folder_name)
    print model_folder_path

    if not os.path.isdir(model_folder_path):
        os.makedirs(model_folder_path)

    str_a = "# {}".format(folder_name)
    print str_a
    write_file.write(str_a + '\n')

    str_a = str(list_features)
    print str_a
    write_file.write(str_a + '\n')


    str_a = str(master_list_parameters[num_set])
    print str_a
    write_file.write(str_a + '\n')


    temp_list_scores = []
    temp_list_test_accuracies = []

    for i in range (5):
        str_a =  "## {}-kth fold --------------------------------".format(i)
        time_fold_start = time.time()
        print str_a
        write_file.write(str_a + '\n')
        # Load the files and selected features
        file_name = "{}_entry_X_train.csv".format(i)
        file_path = os.path.join(folder_path, file_name)
        X_train_temp = pd.read_csv(file_path, encoding="utf-8")
        X_train = X_train_temp[list_features].values

        file_name = "{}_entry_y_train.csv".format(i)
        file_path = os.path.join(folder_path, file_name)
        y_train = np.genfromtxt(file_path, delimiter=',', skip_header=1)


        file_name = "{}_entry_X_test.csv".format(i)
        file_path = os.path.join(folder_path, file_name)
        X_test_temp = pd.read_csv(file_path, encoding="utf-8")
        X_test = X_test_temp[list_features].values

        file_name = "{}_entry_y_test.csv".format(i)
        file_path = os.path.join(folder_path, file_name)
        y_test = np.genfromtxt(file_path, delimiter=',', skip_header=1)


        clf = svm.SVC(verbose=1, random_state=0, **master_list_parameters[num_set])
        clf.fit(X_train, y_train)

        elapsed = time.time() - time_start

        str_a =  "Elapsed Seconds: {}".format(elapsed)
        print str_a
        write_file.write(str_a + '\n')

        str_a = 'Training Time Elapsed: {}'.format(time.strftime("%H:%M:%S", time.gmtime(elapsed)))
        print str_a
        write_file.write(str_a)
        write_file.write("\n")


        training_score =  clf.score(X_train, y_train)
        temp = [i, training_score]
        temp_list_scores.append(temp)
        str_a = "Training set score: %f" % training_score
        print str_a
        write_file.write(str_a + '\n')


        file_name = "svm_model_{}.pkl".format(i)
        file_path = os.path.join(model_folder_path, file_name)
        joblib.dump(clf, file_path)
        str_a =  "Classifier saved to {}\n".format(file_path)
        print str_a
        write_file.write(str_a + '\n')


        elapsed = time.time() - time_fold_start
        str_a =  "Elapsed Seconds: {}".format(elapsed)
        print str_a
        write_file.write(str_a + '\n')

        str_a = 'Total Time Elapsed: {}'.format(time.strftime("%H:%M:%S", time.gmtime(elapsed)))
        print str_a
        write_file.write(str_a + '\n')


        time_predict_start =  time.time()

        # Evaluate on validation set
        test_predict = clf.predict(X_test)


        elapsed = time.time() - time_predict_start
        str_a =  "Predict Elapsed Seconds: {}".format(elapsed)
        print str_a
        write_file.write(str_a + '\n')

        str_a = 'Predict Total Time Elapsed: {}'.format(time.strftime("%H:%M:%S", time.gmtime(elapsed)))
        print str_a
        write_file.write(str_a + '\n')


        len_train = y_train.shape
        print_train = (
            "Features in training dataset (80%):\n"
            "Count: {}\n"
            "{}\n"
        ).format(len_train, Counter(y_train))

        len_test = y_test.shape
        print_cv = (
            "Features in test dataset (20%):\n"
            "Count: {}\n"
            "{}\n"
        ).format(len_test, Counter(y_test))

        write_file.write(print_train)
        print print_train
        write_file.write(print_cv)
        print print_cv

        str_a = "Classification Report for Test Data (20%):\n"
        print str_a
        write_file.write(str_a)
        str_a =  (classification_report(
            y_test, test_predict))
        print str_a
        write_file.write(str_a + '\n')

        test_accuracy = accuracy_score(y_test, test_predict)
        temp = [i,test_accuracy]
        temp_list_test_accuracies.append(temp)
        str_a = "\nAccuracy: {}".format(test_accuracy)
        print str_a
        write_file.write(str_a + '\n')


        str_a = "Average Training Accuracy for: ({})".format(i)
        print str_a
        write_file.write(str_a + '\n')

        mean = 0
        num_list = len(temp_list_scores)
        for p in range(num_list):
            mean = mean + temp_list_scores[p][1]
        mean = float(mean)/num_list

        temp = [i, mean]
        list_scores.append(temp)

        str_a = str(mean)
        print str_a
        write_file.write(str_a + '\n\n')


        # Get average test accuracy
        str_a = "Average Test Accuracy for: ({})".format(i)
        print str_a
        write_file.write(str_a + '\n')

        mean = 0
        num_list = len(temp_list_test_accuracies)
        for p in range(num_list):
            mean = mean + temp_list_test_accuracies[p][1]
        mean = float(mean)/num_list

        temp = [i, mean]
        list_test_accuracies.append(temp)

        str_a = str(mean)
        print str_a
        write_file.write(str_a + '\n\n')

    str_a = "Sorted List of Training Scores: "
    print str_a
    write_file.write(str_a + '\n')
    str_a = str(sorted(list_scores,key=itemgetter(1), reverse=True))
    print str_a
    write_file.write(str_a + '\n\n')

    str_a = "Sorted List of Test Accuracies: "
    print str_a
    write_file.write(str_a + '\n')

    str_a = str(sorted(list_test_accuracies,key=itemgetter(1), reverse=True))
    print str_a
    write_file.write(str_a + '\n\n')


    elapsed = time.time() - time_start
    str_a =  "Elapsed Seconds: {}".format(elapsed)
    print str_a
    write_file.write(str_a + '\n')

    str_a = 'Total Time Elapsed: {}'.format(time.strftime("%H:%M:%S", time.gmtime(elapsed)))
    print str_a
    write_file.write(str_a + '\n')

    write_file.close()

if __name__ == '__main__':

    ap = argparse.ArgumentParser()
    ap.add_argument(
        "-n", "--set_number", help="set number from master_list_features",
        required=True
    )

    ap.add_argument(
        "-t", "--flow_type", help="entry or exit",
        required=True
    )
    ap.add_argument(
        "-f", "--dataset_folder", help="dataset folder: master_standardized_3, master_standardized",
        required=True
    )


    args = vars(ap.parse_args())
    num_set = int(args["set_number"])
    flow_type = args["flow_type"]
    dataset_folder = args["dataset_folder"]


    print num_set, flow_type, dataset_folder

    # python eval_svm_5_fold.py -n 0 -t entry -f master_standardized_3

    eval_svm_5_fold(num_set, flow_type, dataset_folder)