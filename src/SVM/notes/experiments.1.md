# List of experiments for Entry Prediction

## 1. MRT passenger flow
To test the performance at a basic level

## 2. MRT passenger flow, num of trains

## 3. MRT passenger flow, num of trains, weather

## 4. MRT passenger flow, num of trains, weather, holiday, payday

## 5. (All features) MRT passenger flow, num of trains, weather, holiday, payday, traffic incidents
python grid_search_svm.py station month day day_of_the_week hour num_trains avg_air_pressure temperature holiday_type payday_type traffic_type


## 6. All features except day of the month
To test if the other features are already enough to describe a day of the month


# List of experiments for Exit Prediction

## 1. MRT passenger flow
To test the performance at a basic level

## 2. MRT passenger flow, num of trains

## 3. MRT passenger flow, num of trains, weather

## 4. MRT passenger flow, num of trains, weather, holiday, payday

## 5. MRT passenger flow, num of trains, weather, holiday, payday, traffic incidents
Should we include this just for the sake of completion?
Traffic incidents shouldn't affect the exit prediction.
Expectation for this is that this would mess up the results.

## 6. All features except day of the month
To test if the other features are already enough to describe a day of the month

