# List of experiments for Entry Prediction
'''
station,month,day,day_of_the_week,hour,num_trains,avg_air_pressure,temperature,holiday_type,payday_type,traffic_type
'''
## 1. MRT passenger flow
To test the performance at a basic level
'''
python grid_search_svm.py -n entry_1 -t entry -p station month day day_of_the_week hour 
'''
list_features = ['station','month','day','day_of_the_week','hour']

## 2. MRT passenger flow, num of trains
'''
python grid_search_svm.py  -n entry_2 -t entry -p station month day day_of_the_week hour num_trains
'''


list_features = ['station','month','day','day_of_the_week','hour','num_trains']

## 3. MRT passenger flow, num of trains, weather
'''
python grid_search_svm.py  -n entry_3 -t entry -p station month day day_of_the_week hour num_trains avg_air_pressure temperature
'''

list_features = ['station','month','day','day_of_the_week','hour','num_trains','avg_air_pressure']

## 4. MRT passenger flow, num of trains, weather, holiday, payday
'''
python grid_search_svm.py -n entry_4 -t entry -p station month day day_of_the_week hour num_trains avg_air_pressure temperature holiday_type payday_type
'''

list_features = ['station','month','day','day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type']

## 5. (All features) MRT passenger flow, num of trains, weather, holiday, payday, traffic incidents
'''
python grid_search_svm.py -n entry_5 -t entry -p station month day day_of_the_week hour num_trains avg_air_pressure temperature holiday_type payday_type traffic_type
'''

list_features = ['station','month','day','day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type','traffic_type']

## 6. All features except day of the month
To test if the other features are already enough to describe a day of the month
python grid_search_svm.py -n entry_6  -t entry -p station month day_of_the_week hour num_trains avg_air_pressure temperature holiday_type payday_type traffic_type

list_features = ['station','month','day_of_the_week','hour','num_trains','avg_air_pressure','temperature','holiday_type','payday_type','traffic_type']

# List of experiments for Exit Prediction

## 1. MRT passenger flow
To test the performance at a basic level
'''
python grid_search_svm.py -n exit_1 -t exit -p station month day day_of_the_week hour
'''

## 2. MRT passenger flow, num of trains
'''
python grid_search_svm.py  -n exit_2 -t exit -p station month day day_of_the_week hour num_trains
'''


## 3. MRT passenger flow, num of trains, weather
'''
python grid_search_svm.py  -n exit_3 -t exit -p station month day day_of_the_week hour num_trains avg_air_pressure temperature
'''


## 4. MRT passenger flow, num of trains, weather, holiday, payday
'''
python grid_search_svm.py  -n exit_4 -t exit -p station month day day_of_the_week hour num_trains avg_air_pressure temperature holiday_type payday_type
'''

## 5. All features except: day of the month and traffic incidents
To test if the other features are already enough to describe a day of the month

python grid_search_svm.py  -n exit_6 -t exit -p station month day_of_the_week hour num_trains avg_air_pressure temperature holiday_type payday_type 



### Scrapped
## 5. MRT passenger flow, num of trains, weather, holiday, payday, traffic incidents
Should we include this just for the sake of completion?
Traffic incidents shouldn't affect the exit prediction.
Expectation for this is that this would mess up the results.

'''
python grid_search_svm.py  -n exit_5 -t exit-p station month day_of_the_week hour num_trains avg_air_pressure temperature holiday_type payday_type traffic_type
'''