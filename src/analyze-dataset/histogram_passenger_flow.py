'''
    Histogram of passenger flow for all days
    Class 0 = Entry
    Class 1 = Exit
'''

import time
from datetime import timedelta
import numpy as np
import os
import joblib
import argparse

import matplotlib
import matplotlib.pyplot as plt


SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))


def histogram_passenger_flow():
    list_years = [2016, 2017]

    j = 0
    for m in range(len(list_years)):
        folder_name = "hourly_{}".format(list_years[m])
        folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "MRT_passenger_flow_entry_exit", folder_name)
        result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "analysis", "histogram_passenger_flow")

        file_path = os.path.join(folder_path, "all_0_ENTRY.csv")
        list_entry = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=int)

        file_path = os.path.join(folder_path, "all_1_EXIT.csv")
        list_exit = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=int)

        for n in range(2):
            if n == 0:
                plt.figure(j+1)
                for i, binwidth in enumerate([1, 5, 10, 15]):

                    # Set up the plot
                    ax = plt.subplot(2, 2, i + 1)

                    # Draw the plot
                    ax.hist(list_entry[:, 7], bins = int(180/binwidth),
                            color = 'blue', edgecolor = 'black')

                    # Title and labels
                    plot_title = "Entry Histogram with Binwidth = {} ({})".format(binwidth, list_years[m])
                    '''
                    ax.set_title(plot_title, size = 30)
                    ax.set_xlabel('Passenger Flow', size = 22)
                    ax.set_ylabel('Number of instances', size= 22)
                    '''
                    ax.set_title(plot_title)
                    ax.set_xlabel('Passenger Flow')
                    ax.set_ylabel('Number of instances')
                file_name = "entry_{}.png".format(list_years[m])
                file_path = os.path.join(result_folder_path, file_name)
                plt.savefig(file_path)
                j += 1

            else:
                plt.figure(j+1)

                # Set up the plot
                # Show 4 different binwidths

                for i, binwidth in enumerate([1, 5, 10, 15]):
                    ax = plt.subplot(2, 2, i + 1)

                    # Draw the plot
                    ax.hist(list_exit[:, 7], bins = int(180/binwidth),
                            color = 'blue', edgecolor = 'black')

                    # Title and labels
                    plot_title = "Exit Histogram with Binwidth = {} ({})".format(binwidth, list_years[m])
                    '''
                    ax.set_title(plot_title, size = 30)
                    ax.set_xlabel('Passenger Flow', size = 22)
                    ax.set_ylabel('Number of instances', size= 22)
                    '''
                    ax.set_title(plot_title)
                    ax.set_xlabel('Passenger Flow')
                    ax.set_ylabel('Number of instances')
                file_name = "exit_{}.png".format(list_years[m])
                file_path = os.path.join(result_folder_path, file_name)
                plt.savefig(file_path)

                j += 1


    plt.tight_layout()
    plt.show()

if __name__ == '__main__':
    histogram_passenger_flow()

