'''
    Graph for each station for just one day for all hours (regardless of what day of the week)
'''

import time
from datetime import timedelta
import numpy as np
import os
import joblib
import argparse

import matplotlib
import matplotlib.pyplot as plt


def plot_per_station_single_day():
    list_years = [2016, 2017]



    for m in range(len(list_years)):

        folder_name = "hourly_{}".format(list_years[m])
        folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "MRT_passenger_flow_entry_exit", folder_name)
        result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "analysis", "histogram_passenger_flow")

        file_path = os.path.join(folder_path, "all_0_ENTRY.csv")
        list_entry = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=int)

        file_path = os.path.join(folder_path, "all_1_EXIT.csv")
        list_exit = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=int)

        num_entries = list_exit.shape[0]
        new_day = 0


        for n in range(num_entries):



        plt.figure(m+1)

        for i in range(2):
            ax = plt.subplot(2, 1, i + 1)



        # Show 4 different binwidths
        for i, binwidth in enumerate([1, 5, 10, 15]):

            # Set up the plot
            ax = plt.subplot(2, 2, i + 1)

            # Draw the plot
            ax.hist(list_entry[:, 7], bins = int(180/binwidth),
                    color = 'blue', edgecolor = 'black')

            # Title and labels
            plot_title = "Histogram with Binwidth = {} ({})".format(binwidth, list_years[m])
            '''
            ax.set_title(plot_title, size = 30)
            ax.set_xlabel('Passenger Flow', size = 22)
            ax.set_ylabel('Number of instances', size= 22)
            '''
            ax.set_title(plot_title)
            ax.set_xlabel('Passenger Flow')
            ax.set_ylabel('Number of instances')
    plt.tight_layout()
    plt.show()




if __name__ == '__main__':
    plot_per_station_single_day()
