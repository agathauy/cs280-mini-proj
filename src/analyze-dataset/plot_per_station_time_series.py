'''
    Graph for each station for all hours on entry, and then for exit for all hours (time series for the days of the week)
'''

import time
from datetime import timedelta
import numpy as np
import os
import joblib
import argparse

import matplotlib
import matplotlib.pyplot as plt

def plot_per_station_time_series.py():
    list_years = [2016, 2017]
    list_months = ["JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"]

    list_days = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY']
    #print list_days.index("MONDAY")
    list_stations = ['NorthAve', 'QuezonAve', 'GMA', 'Cubao', 
        'Santolan', 'Ortigas', 'ShawBlvd', 'BoniAve', 'Guadalupe',
        'Buendia', 'Ayala', 'Magallanes', 'Taft']
    list_type = ["ENTRY", "EXIT"]
    print list_stations
    print len(list_stations)

    num_stations = 13
    num_flows = 26


    for n in range(len(list_years)):
        folder_name = "hourly_{}".format(list_years[n])
        folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "MRT_passenger_flow_per_station", folder_name)

        iter_x = []
        iter_y = []

        for m in range(len(list_stations)):

            file_name = "{} - {} {} Summary Hourly.csv".format(m, list_months[m], list_years[n])
            file_path = os.path.join(folder_path,file_name)
            print file_path
            #df = pd.read_csv(file_path, encoding="utf-8")
            dataset = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=int)







if __name__ == '__main__':
    plot_per_station_single_day()
