'''
    Statistics of passenger flow
    Class 0 = Entry
    Class 1 = Exit
'''

import time
from datetime import timedelta
import numpy as np
import os
import joblib
import argparse

import matplotlib
import matplotlib.pyplot as plt


SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))



def statistics_passenger_flow():
    list_years = [2016, 2017]

    folder_path = os.path.join(SCRIPT_DIR, "..", "..", "analysis", "statistics_passenger_flow")

    file_path = os.path.join(folder_path, "results_combined.txt")
    write_file = open(file_path, 'w')
    write_file.truncate()

    combined_entry = np.array([])
    combined_exit = np.array([])

    for m in range(len(list_years)):
        folder_name = "hourly_{}".format(list_years[m])
        folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "MRT_passenger_flow_entry_exit", folder_name)

        file_path = os.path.join(folder_path, "all_0_ENTRY.csv")
        list_entry = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=int)

        combined_entry = np.concatenate((combined_entry, list_entry[:,7].T))


        file_path = os.path.join(folder_path, "all_1_EXIT.csv")
        list_exit = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=int)

        combined_exit = np.concatenate((combined_exit, list_exit[:,7].T))

        #str_a = "# {}".format(list_years[m])
        #print str_a
        #write_file.write(str_a + '\n')


    for n in range(2):
        if n == 0:
            str_a = "## Entry"
            print str_a
            write_file.write(str_a + '\n')
            # For entry
            str_a = "Mean: {}".format(np.mean(combined_entry))
            print str_a
            write_file.write(str_a + '\n')

            str_a = "std: {}".format(np.std(combined_entry))
            print str_a
            write_file.write(str_a + '\n')

            str_a = "var: {}".format(np.var(combined_entry))
            print str_a
            write_file.write(str_a + '\n')

            str_a = "min: {}".format(np.amin(combined_entry))
            print str_a
            write_file.write(str_a + '\n')

            str_a = "max: {}".format(np.amax(combined_entry))
            print str_a
            write_file.write(str_a + '\n')
            for j, q in enumerate([0, 20, 30, 40, 60, 80, 100]):
                print  q
                str_a = "{} percentile: {}".format(q, np.percentile(combined_entry, q))
                print str_a
                write_file.write(str_a + '\n')

            hist, bin_edges = np.histogram(combined_entry, bins=5, range=None, normed=None, weights=None, density=None)
            str_a = "5 bin unweighted histogram: {}".format(str(bin_edges))
            print str_a
            write_file.write(str_a + '\n')
        else:
            # For exit
            str_a = "## Exit"
            print str_a
            write_file.write(str_a + '\n')

            str_a = "Mean: {}".format(np.mean(combined_exit))
            print str_a
            write_file.write(str_a + '\n')

            str_a = "std: {}".format(np.std(combined_exit))
            print str_a
            write_file.write(str_a + '\n')

            str_a = "var: {}".format(np.var(combined_exit))
            print str_a
            write_file.write(str_a + '\n')

            str_a = "min: {}".format(np.amin(combined_exit))
            print str_a
            write_file.write(str_a + '\n')

            str_a = "max: {}".format(np.amax(combined_exit))
            print str_a
            write_file.write(str_a + '\n')

            for j, q in enumerate([0, 20, 30, 40, 60, 80, 100]):

                str_a = "{} percentile: {}".format(q, np.percentile(combined_exit, q))
                print str_a
                write_file.write(str_a + '\n')

            hist, bin_edges = np.histogram(combined_exit, bins=5, range=None, normed=None, weights=None, density=None)
            str_a = "5 bin unweighted histogram: {}".format(str(bin_edges))
            print str_a
            write_file.write(str_a + '\n')
    str_a = "\n"
    print str_a
    write_file.write(str_a + '\n')

    write_file.close()
    print "write file close"


if __name__ == '__main__':
    statistics_passenger_flow()
