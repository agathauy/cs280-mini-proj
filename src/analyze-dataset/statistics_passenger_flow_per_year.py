'''
    Statistics of passenger flow
    Class 0 = Entry
    Class 1 = Exit
'''

import time
from datetime import timedelta
import numpy as np
import os
import joblib
import argparse

import matplotlib
import matplotlib.pyplot as plt


SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))



def statistics_passenger_flow():
    list_years = [2016, 2017]

    folder_path = os.path.join(SCRIPT_DIR, "..", "..", "analysis", "statistics_passenger_flow")

    file_path = os.path.join(folder_path, "results_per_year.txt")
    write_file = open(file_path, 'w')
    write_file.truncate()


    for m in range(len(list_years)):
        folder_name = "hourly_{}".format(list_years[m])
        folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "MRT_passenger_flow_entry_exit", folder_name)

        file_path = os.path.join(folder_path, "all_0_ENTRY.csv")
        list_entry = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=int)

        file_path = os.path.join(folder_path, "all_1_EXIT.csv")
        list_exit = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=int)


        str_a = "# {}".format(list_years[m])
        print str_a
        write_file.write(str_a + '\n')

        for n in range(2):
            if n == 0:
                str_a = "## Entry"
                print str_a
                write_file.write(str_a + '\n')
                # For entry
                str_a = "Mean: {}".format(np.mean(list_entry[:, 7]))
                print str_a
                write_file.write(str_a + '\n')

                str_a = "std: {}".format(np.std(list_entry[:, 7]))
                print str_a
                write_file.write(str_a + '\n')

                str_a = "var: {}".format(np.var(list_entry[:, 7]))
                print str_a
                write_file.write(str_a + '\n')

                str_a = "min: {}".format(np.amin(list_entry[:, 7]))
                print str_a
                write_file.write(str_a + '\n')

                str_a = "max: {}".format(np.amax(list_entry[:, 7]))
                print str_a
                write_file.write(str_a + '\n')
                for j, q in enumerate([0, 20, 40, 60, 80, 100]):
                    print  q
                    str_a = "{} percentile: {}".format(q, np.percentile(list_entry[:, 7], q))
                    print str_a
                    write_file.write(str_a + '\n')
            else:
                # For exit
                str_a = "## Exit"
                print str_a
                write_file.write(str_a + '\n')

                str_a = "Mean: {}".format(np.mean(list_exit[:, 7]))
                print str_a
                write_file.write(str_a + '\n')

                str_a = "std: {}".format(np.std(list_exit[:, 7]))
                print str_a
                write_file.write(str_a + '\n')

                str_a = "var: {}".format(np.var(list_exit[:, 7]))
                print str_a
                write_file.write(str_a + '\n')

                str_a = "min: {}".format(np.amin(list_exit[:, 7]))
                print str_a
                write_file.write(str_a + '\n')

                str_a = "max: {}".format(np.amax(list_exit[:, 7]))
                print str_a
                write_file.write(str_a + '\n')

                for j, q in enumerate([0, 20, 40, 60, 80, 100]):

                    str_a = "{} percentile: {}".format(q, np.percentile(list_exit[:, 7], q))
                    print str_a
                    write_file.write(str_a + '\n')
        str_a = "\n"
        print str_a
        write_file.write(str_a + '\n')

    write_file.close()
    print "write file close"


if __name__ == '__main__':
    statistics_passenger_flow()
