'''
    Parses text copy pasted from MMDA's Twitter account on traffic incidents at EDSA.

    Direct parsing from the dataset only for this stage.
'''

import os
import re

SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))


def mmda_traffic_incidents():
    list_years = [2016, 2017]
    folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "raw", "MMDA_traffic_incidents")
    result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "MMDA_traffic_incidents")

    for m in range (len(list_years)):
        file_name = "{}_mmda_twitter_direct_copy_cropped_header.txt".format(list_years[m])
        file_path = os.path.join(folder_path, file_name)
        print file_path

        # Write parsing to another file
        file_name =  "{}_mmda_twitter.csv".format(list_years[m])
        result_file_path = os.path.join(result_folder_path, file_name)
        write_file = open(result_file_path, 'w')
        write_file.truncate()

        # CSV header
        # Example:
        # 2016,Dec,23,7:37 PM,Magallanes,SB
        str_a = "year,month,day,time,place,direction"
        write_file.write(str_a + '\n')


        file_content = [line for line in open(file_path)]
        print len(file_content)

        entry = {
            "year": list_years[m],
            "month": "",
            "day": "",
            "time": "",
            "place": "",
            "direction": ""
        }

        new_entry = False
        new_entry_ctr = 0

        for i in range(len(file_content)):

            print file_content[i]
            print len(file_content[i])
            # print word_tokenize(file_content[i])
            # print len(word_tokenize(file_content[i]))

            list_tokenized = file_content[i].split("\n")[0].split(" ")
            #list_tokenized = [word.rstrip() for word in init_list_tokenized]
            print list_tokenized

            # Not a blank line
            if len(list_tokenized) > 0:
                if list_tokenized[0] == "@MMDA":
                    new_entry = True
                    print "-----------------------------------------------"

                elif new_entry == True:
                    if new_entry_ctr == 0:
                        print "Here in date"
                        # Get date
                        entry["day"] = list_tokenized[1]
                        entry["month"] = list_tokenized[2]
                    elif list_tokenized[0] == "Replying":
                        continue
                    elif new_entry_ctr == 2:
                        new_list = [word.lower() for word in list_tokenized if word.isalpha()]
                        print new_list

                        # Not an announcement about cleared accident
                        if "cleared" not in new_list:
                            print "CLERED NOT IN NEW LIST ------------------"
                            start_of_place = False
                            for j in range(len(list_tokenized)):
                                list_tokenized[j] = list_tokenized[j].rstrip().lstrip()
                                if list_tokenized[j].upper() == "EDSA":
                                    start_of_place = True
                                elif start_of_place == True:
                                    temp_word = re.sub('[^0-9a-zA-Z]+', '', list_tokenized[j])
                                    if temp_word.upper() != "NB" and temp_word.upper() != "SB" and temp_word.upper() != "EB" and temp_word.upper() != "WB":

                                        print entry["place"]
                                        # Get the name of the place
                                        if len(entry["place"]) == 0:
                                            entry["place"] = list_tokenized[j]
                                        else:
                                            entry["place"] = entry["place"] + " " + list_tokenized[j]
                                    else:
                                        # Acquired direction
                                        if temp_word == "EB":
                                            temp_word = "NB"
                                        elif temp_word == "WB":
                                            temp_word = "SB"

                                        entry["direction"] = temp_word
                                        start_of_place = False
                                        break
                            if entry["direction"] == "":
                                #list_temp_place = entry["place"].split(" ")
                                #temp_place = " ".join(entry["place"][0:2])
                                #entry["place"] = temp_place
                                entry["direction"] = "BLANK_DIRECTION"

                            print "before get time"
                            # Get time
                            time = ""
                            suffix = ""
                            start_of_time = False
                            for j in range(len(list_tokenized)):


                                temp_list = list_tokenized[j].split(":")
                                if len(temp_list) == 2 and temp_list[0].isdigit() and  temp_list[1].isdigit():
                                    print "in time tokenized"
                                    print temp_list
                                    start_of_time = True
                                    time = list_tokenized[j]
                                    print time
                                elif start_of_time == True:
                                    #print list_tokenized[j]
                                    print "in start of time"

                                    temp_word = re.sub('[^0-9a-zA-Z]+', '', list_tokenized[j])
                                    print temp_word
                                    if temp_word.upper() == "PM" or temp_word.upper() == "AM":
                                        suffix = temp_word.upper()
                                        entry["time"] = time + " " + suffix
                                        break
                            print entry
                            # Write data to csv
                            #  str_a = "year,month,day,time,place,direction"

                            # Remove commas from "place"
                            entry["place"] = entry["place"].replace(",", "")
                            entry["place"] = entry["place"].replace("#", "")

                            str_a = "{},{},{},{},{},{}".format(entry["year"], entry["month"], entry["day"], entry["time"], entry["place"], entry["direction"])
                            print str_a
                            write_file.write(str_a + '\n')

                        # Clear data in entry
                        new_entry = False
                        new_entry_ctr = 0
                        entry["month"] = ""
                        entry["day"] = ""
                        entry["time"] = ""
                        entry["place"] = ""
                        entry["direction"] = ""

                    if new_entry == True:
                        new_entry_ctr += 1

            #if (i == 24):
            #    return



        # End of for loop through file contents
        print "close file"
        write_file.close()










if __name__ == '__main__':
    mmda_traffic_incidents()