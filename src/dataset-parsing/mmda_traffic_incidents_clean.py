'''
    Cleans the data parsed with mmda_traffic_incidents

'''

import os
import re
import csv
import numpy as np
import pandas as pd
import time
import math

SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))


def mmda_traffic_incidents_clean():
    # Timer function
    time_start = time.time()

    folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "MMDA_traffic_incidents")
    list_years = [2016, 2017]
    list_months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    list_hours = {
        "AM": {
            12: 0,
            1: 1,
            2: 2,
            3: 3,
            4: 4,
            5: 5,
            6: 6,
            7: 7,
            8: 8,
            9: 9,
            10: 10,
            11: 11
        },
        "PM": {
            12: 12,
            1: 13,
            2: 14,
            3: 15,
            4: 16,
            5: 17,
            6: 18,
            7: 19,
            8: 20,
            9: 21,
            10: 22,
            11: 23
        }
    }

    list_stations = {
        0: {
            "station": "NorthAve",
            "places": ['north', 'annex', 'balintawak', 'walter', 'trinoma', 'landmark']
        },
        1: { 
            "station": "QuezonAve",
            "places": ['quezon', 'centris', 'q.']
        },
        2: {
            "station": "GMA",
            "places": ['kamuning', 'gma', 'timog', 'tuazon']
        },
        3: {
            "station": "Cubao",
            "places": ['cubao', 'farmers', 'aurora', 'ignacia', 'ramon', 'main ave', 'p.' ]
        },
        4: {
            "station": "Santolan",
            "places": ['santolan', 'gate', 'white', 'soliven']
        },
        5: {
            "station": "Ortigas",
            "places": ['ortigas', 'robinsons', 'guadix']
        },
        6: {
            "station": "Shaw",
            "places": ['shaw', 'megamall', 'starmall', 'reliance']
        },
        7: {
            "station": "Boni",
            "places": ['boni', 'pioneer', 'forum']
        },
        8: {
            "station": "Guadalupe",
            "places": ['guadalupe', 'orense', 'estrella']
        },
        9: {
            "station": "Buendia",
            "places": ['buendia', 'kalayaan', 'rockwell']
        },
        10: {
            "station": "Ayala",
            "places": ['ayala', 'mckinley', 'pasay']
        },
        11: {
            "station": "Magallanes",
            "places": ['magallanes']
        },
        12: {
            "station": "Taft",
            "places": ['taft', 'malibay', 'tramo', 'roxas']
        }
    }



    list_valid_incidents = []
    list_to_parse_incidents = []
    list_invalid_incidents = []


    column_types = [('year', int), ('month', int), ('day', int), ('time', str), ('place', str), ('direction', str)]
    # Parse all that is parsable
    for i, year in enumerate(list_years):
        file_name = "{}_mmda_twitter.csv".format(year)
        file_path = os.path.join(folder_path, file_name)
        #df_traffic = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=column_types)
        df_traffic = pd.read_csv(file_path, encoding='utf-8')

        num_entries = len(df_traffic['year'])
        #num_entries = 6
        for j in range(num_entries):
            print "------------------- ENTRY"
            print j
            print df_traffic['time'][j], df_traffic['place'][j], df_traffic['direction'][j]
            temp = []
            # Put in to parse the entries with blank fields
            if (pd.isnull(df_traffic['time'][j])  or pd.isnull(df_traffic['place'][j]) or pd.isnull(df_traffic['direction'][j])):
                temp = [df_traffic['year'][j], df_traffic['month'][j], df_traffic['day'][j], df_traffic['time'][j], df_traffic['place'][j], df_traffic['direction'][j]]
                list_to_parse_incidents.append(temp)
                print "with a blank field"
                continue

            if (len(df_traffic['time'][j]) == 0 or len(df_traffic['place'][j]) == 0 or len(df_traffic['direction'][j]) == 0):
                temp = [df_traffic['year'][j], df_traffic['month'][j], df_traffic['day'][j], df_traffic['time'][j], df_traffic['place'][j], df_traffic['direction'][j]]
                list_to_parse_incidents.append(temp)
                print "with a blank field"
                continue

            # Meaning complete entries
            # Parse time
            list_time = df_traffic['time'][j].split(" ")
            print "=========list_time"
            print list_time
            temp_hour = int(list_time[0].split(":")[0])
            suffix = list_time[1]
            hour = list_hours[suffix][int(temp_hour)]

            # Parse direction
            direction = 0
            if (df_traffic['direction'][j] == 'NB'):
                direction = 1
            elif (df_traffic['direction'][j] == 'SB'):
                direction = 2
            else: # blank direction
                direction = 3
            


            # Parse place
            df_traffic['place'][j] = df_traffic['place'][j].lower()
            temp_place = df_traffic['place'][j].lower()
            print temp_place
            #list_place = df_traffic['place'][j].split(" ")
            actual_place = -1
            for k in range(len(list_stations)):
                for m in range(len(list_stations[k]['places'])):
                    if list_stations[k]['places'][m] in temp_place:
                        actual_place = k
                        break
                if actual_place != -1:
                    break

            '''
            for k in range(len(list_place)):
                word = list_place[k]
                for m in range(13):
                    if word in list_stations[m]['places']:
                        actual_place = m
                        break
                if actual_place == -1:
                    break
            '''

            # Place not found in dictionary
            if actual_place == -1: 
                print "place not in dictionary"
                temp = [df_traffic['year'][j], df_traffic['month'][j], df_traffic['day'][j], df_traffic['time'][j], df_traffic['place'][j], df_traffic['direction'][j]]
                list_to_parse_incidents.append(temp)
                continue



            # Valid entry
            print "valid entry"
            temp = [df_traffic['year'][j], list_months.index(df_traffic['month'][j].encode('utf-8')), df_traffic['day'][j], hour, actual_place, direction]
            print temp
            list_valid_incidents.append(temp)




    # Save valid entries to a csv
    result_header = "year,month,day,time,place,direction"
    result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "MMDA_traffic_incidents_cleaned")
    file_name = "partial_cleaned.csv"
    result_file_path = os.path.join(result_folder_path, file_name)
    print "----------------- LIST OF VALID INCIDENTS"
    print list_valid_incidents[0:2]
    print len(list_valid_incidents)
    df_valid = np.array(list_valid_incidents)
    print "df_valid.shape"
    print df_valid.shape
    #print df_valid
    np.savetxt(result_file_path, df_valid, delimiter=",", fmt='%d', header=result_header)



    original_column_types = [('year', int), ('month', str), ('day', int), ('time', str), ('place', str), ('direction', str)]

    file_name = "to_parse.csv"
    result_file_path = os.path.join(result_folder_path, file_name)
    print "----------------- LIST OF TO PARSE INCIDENTS"
    print list_to_parse_incidents[0:2]
    print len(list_to_parse_incidents)
    #df_to_parse = np.array(list_to_parse_incidents, dtype=original_column_types)
    original_labels = ['year', 'month', 'day', 'time', 'place', 'direction']
    df_to_parse = pd.DataFrame.from_records(list_to_parse_incidents, columns=original_labels)
    #print df_to_parse.shape
    df_to_parse.to_csv(result_file_path, index=False, quoting=csv.QUOTE_NONE, quotechar="", escapechar="\\", encoding='utf-8')
    '''
    df_vocab.to_csv("dictionary_merged_2.csv", index=False,  quoting=csv.QUOTE_NONE, quotechar="",  escapechar="\\")

    sales = [('Jones LLC', 150, 200, 50),
            ('Alpha Co', 200, 210, 90),
            ('Blue Inc', 140, 215, 95)]
    labels = ['account', 'Jan', 'Feb', 'Mar']
    df = pd.DataFrame.from_records(sales, columns=labels)

    ''' 
    #np.savetxt(result_file_path, df_to_parse, delimiter=",", header=result_header)



    # End of timer
    elapsed = time.time() - time_start
    str_a = 'Total Time Elapsed: {}'.format(time.strftime("%H:%M:%S", time.gmtime(elapsed)))
    print str_a


if __name__ == '__main__':
    mmda_traffic_incidents_clean()