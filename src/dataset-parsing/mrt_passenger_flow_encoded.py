'''
    Parses the generated csvs of hourly data for MRT passenger flow

    For blank passenger flow, -1 is substituted

    CSV output of this parser but with numerical values:
    year, month, day, day_of_the_week, Time, Station Name, Type, Flow Number

'''
import os
import numpy as np


SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))


def mrt_passenger_flow_encoded(year):
    list_months = ["JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"]
    if year % 4 == 0:
        list_months_length = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    else:
        list_months_length = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    list_days = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY']
    #print list_days.index("MONDAY")
    list_stations = ['NorthAve', 'QuezonAve', 'GMA', 'Cubao', 
        'Santolan', 'Ortigas', 'ShawBlvd', 'BoniAve', 'Guadalupe',
        'Buendia', 'Ayala', 'Magallanes', 'Taft']
    list_type = ["ENTRY", "EXIT"]

    list_time = ["00:00 - 00:59", "01:00 - 01:59", "02:00 - 02:59", "03:00 - 03:59", "04:00 - 04:59", "05:00 - 05:59", "06:00 - 06:59", "07:00 - 07:59", "08:00 - 08:59",
        "09:00 - 09:59", "10:00 - 10:59", "11:00 - 11:59", "12:00 - 12:59", "13:00 - 13:59", "14:00 - 14:59",
        "15:00 - 15:59", "16:00 - 16:59", "17:00 - 17:59", "18:00 - 18:59", "19:00 - 19:59", "20:00 - 20:59", 
        "21:00 - 21:59", "22:00 - 22:59", "23:00 - 23:59"]
    print list_stations
    print len(list_stations)

    num_stations = 13
    num_flows = 26

    folder_name = "mrt{}".format(year)
    folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "interim", folder_name)
    folder_name = "hourly_{}".format(year)
    result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "MRT_passenger_flow_encoded", folder_name)



    for m in range(12):
        print m
        file_name = "{} - {} {} Summary Hourly.csv".format(m, list_months[m], year)
        file_path = os.path.join(folder_path,file_name)
        print file_path

        # Write parsing to another file
        result_file_path = os.path.join(result_folder_path, file_name)
        write_file = open(result_file_path, 'w')
        write_file.truncate()
        str_a = "year,month,day,day_of_the_week,time,station,flow_type,flow_num"
        write_file.write(str_a + '\n')

        file_content = [line for line in open(file_path)]
        file_list = file_content[0].split('\r')
        print len(file_list)


        j = 0
        day_counter = 0

        # Go through each day in the csv
        for i in range(len(file_list)):
            if j == 0:
                # Break out of loop to move on to next month
                if (day_counter == list_months_length[m]):
                    print "in break"
                    break

            # For ignoring the first line of the csv
            if i >= 2:
                j = j + 1



            # Get Date
            if j == 2:
                #print "PRINT DATE_LIST"
                date_list = file_list[i].split('"')[1].split(" ")
                day_of_the_week = list_days.index(str(date_list[0].split(",")[0]).upper())
                #date = "{}_{}_{}".format(date_list[3], date_list[2].upper(), date_list[1])
                year = date_list[3]
                month = list_months.index(date_list[2].upper())
                day = date_list[1]
                #print day, date

            # Get the values for each line
            # 13 stations
            # 26 flows, entry and exit
            # Date (YYMMDD), Weekday, Time, Station Name, Type, Flow Number

            if j >= 6 and j <= 29:
                print date_list
                entry_list = file_list[i].split(",")
                print entry_list
                station_num = 0
                entry_type = 0
                time_string = str(entry_list[0])
                if len(time_string) < 13:
                    print time_string
                    time_string = time_string.replace(" ", "")
                    print time_string
                    entry_time_list = time_string.split("-")
                    print entry_time_list
                    print entry_time_list[0][0]
                    print entry_time_list[0][1]

                    if entry_time_list[0][0] != '0' or entry_time_list[1][0] != '0':
                        a = "0" + entry_time_list[0]
                        b = "0" + entry_time_list[1]
                    else:
                        a = entry_time_list[0]
                        b = entry_time_list[1]

                    time_string = a + " - " + b
                    print time_string
                time = list_time.index(time_string)
                



                for k in range(num_flows):
                    flow = entry_list[k + 1]
                    if flow == "" or not flow.isdigit():
                        flow = 0
                    #flow_type = list_type[entry_type]
                    #station_name = list_stations[station_num]
                    flow_type = entry_type
                    station_name = station_num

                    # Do not include times that the mrt is not open
                    if (time >= 5 and time <= 22):
                        str_a = "{},{},{},{},{},{},{},{}".format(year, month, day, day_of_the_week, time, station_name, flow_type, flow)
                        #print str_a
                        write_file.write(str_a + '\n')


                    if entry_type == 1:
                        entry_type = 0
                        station_num = station_num + 1
                    else:
                        entry_type = 1

            # Reset counter for new day at the 31st repeating line
            if j == 31:
                j = 0
                day = ""
                date = ""
                day_counter = day_counter + 1

        print "end of month for loop"
        print "close file"
        write_file.close()


    return


if __name__ == '__main__':
    year = 2016
    mrt_passenger_flow_encoded(year)