'''
    Parses the generated csvs of hourly data for MRT passenger flow

    For blank passenger flow, -1 is substituted

    CSV output of this parser:
    year, month, day, day_of_the_week, Time, Station Name, Type, Flow Number

'''
import os
import numpy as np
#import pandas as pd



SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))


def mrt_passenger_flow_per_station():
    list_years = [2016, 2017]
    list_months = ["JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"]

    list_days = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY']
    #print list_days.index("MONDAY")
    list_stations = ['NorthAve', 'QuezonAve', 'GMA', 'Cubao', 
        'Santolan', 'Ortigas', 'ShawBlvd', 'BoniAve', 'Guadalupe',
        'Buendia', 'Ayala', 'Magallanes', 'Taft']
    list_type = ["ENTRY", "EXIT"]
    print list_stations
    print len(list_stations)

    num_stations = 13
    num_flows = 26

    entries_entry = [
        [],[],[],[],
        [],[],[],[],
        [],[],[],[],
        []
    ]
    entries_exit = [
        [],[],[],[],
        [],[],[],[],
        [],[],[],[],
        []
    ]


    for n in range(len(list_years)):
        if list_years[n] % 4 == 0:
            list_months_length = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        else:
            list_months_length = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]


        folder_name = "hourly_{}".format(list_years[n])
        folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "MRT_passenger_flow_encoded", folder_name)
        folder_name = "hourly_{}".format(list_years[n])
        result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "MRT_passenger_flow_per_station", folder_name)

        for m in range(12):
            print m
            file_name = "{} - {} {} Summary Hourly.csv".format(m, list_months[m], list_years[n])
            file_path = os.path.join(folder_path,file_name)
            print file_path
            #df = pd.read_csv(file_path, encoding="utf-8")
            dataset = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=int)

            num_entries = dataset.shape[0]

            # Go through each entry
            # Put in station list
            for i in range(num_entries):
                print dataset[i, :]
                #print dataset[i, :]
                print dataset[i,5]

                if dataset[i, 6] == 0:
                    entries_entry[dataset[i, 5]].append(dataset[i])
                else:
                    entries_exit[dataset[i, 5]].append(dataset[i])



        # Write parsing to another file
        #result_file_path = os.path.join(result_folder_path, file_name)
        #write_file = open(result_file_path, 'w')
        #write_file.truncate()
        #str_a = "year,month,day,day_of_the_week,time,station,flow_type,flow_num"
        #write_file.write(str_a + '\n')

        # Create a separate file for each station and each entry/exit
        # e.g. 0_NorthAve_0_ENTRY.csv 0_NorthAve_1_EXIT.csv

        for i in range(13):
            file_name = "{}_{}_{}_{}.csv".format(i, list_stations[i], 0, list_type[0])
            file_path = os.path.join(result_folder_path, file_name)
            print file_path
            temp = np.array(entries_entry[i])
            np.savetxt(file_path, temp, delimiter=",", fmt='%d', header="year,month,day,day_of_the_week,time,station,flow_type,flow_num")

            file_name = "{}_{}_{}_{}.csv".format(i, list_stations[i], 1, list_type[1])
            file_path = os.path.join(result_folder_path, file_name)
            print file_path
            temp = np.array(entries_exit[i])
            np.savetxt(file_path, temp, delimiter=",", fmt='%d', header="year,month,day,day_of_the_week,time,station,flow_type,flow_num")


    return


if __name__ == '__main__':
    mrt_passenger_flow_per_station()