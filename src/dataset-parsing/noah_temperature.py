'''
    Parses for air pressure from /data/interim/noah_rainfall

    Output csv
    year, month, day, hour, avg air pressure
'''

import os

SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))




def noah_temperature():
    list_years = [2016, 2017]
    list_files = ['HYBRID_250_DOSTBICUTAN_TAGUIGCITY_METROMANILA_5.csv']
    #list_files = ['HYBRID_190_SCIENCEGARDENPAGASA-IDP_QUEZONCITY_METROMANILA_2.csv', 'HYDROMET_968_NATIONALCENTERFORMENTALHEALTH_MANDALUYONG_METROMANILA_2.csv', 'HYBRID_144_USUSAN_TAGUIGCITY_METROMANILA_2.csv']
    folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "interim", "noah_temperature")
    result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "noah_temperature")

    for m in range(len(list_files)):
        file_name = list_files[m]
        file_path = os.path.join(folder_path, file_name)
        print file_path

        # Write parsing to another file
        result_file_path = os.path.join(result_folder_path, file_name)
        write_file = open(result_file_path, 'w')
        write_file.truncate()
        str_a = "year,month,day,hour,avg_air_temperature"
        write_file.write(str_a + '\n')

        file_content = [line for line in open(file_path)]
        print len(file_content)

        entry = {
            "year": "",
            "month": "",
            "day": "",
            "hour": "",
            "total_temperature": 0,
            "num_readings": 0
        }

        for i in range(6, len(file_content)):
            print file_content[i]
            entry_list = file_content[i].split(";")
            date_entry = entry_list[0].split(" ")[0].split("-")
            current_year = int(date_entry[0])

            # Check if year is part of to be acquired
            if current_year in list_years:
                current_month = int(date_entry[1])
                print current_month
                current_day = int(date_entry[2])
                print current_day

                current_hour = int(entry_list[0].split(" ")[1].split(":")[0])
                print current_hour


                if entry["year"] == current_year and entry["month"] == current_month and entry["day"] == current_day and entry["hour"] == current_hour:
                    print "same hour"

                    temperature = entry_list[4]
                    print temperature

                    # Check if air pressure is a float
                    try:
                        float(temperature)
                        entry["total_temperature"] += float(temperature)
                        entry["num_readings"] += 1
                    except ValueError:
                        print "Not a float"


                else:
                    if i != 6 and entry["year"] != "":
                        # Log the final reading
                        # str_a = "year,month,day,hour,avg_temperature"
                        # write_file.write(str_a + '\n')s
                        print entry
                        avg_temperature = float(entry["total_temperature"])/entry["num_readings"]
                        str_a = "{},{},{},{},{}".format(entry["year"], entry["month"], entry["day"], entry["hour"], avg_temperature)
                        print str_a
                        write_file.write(str_a + '\n')


                    temperature = entry_list[4]
                    print temperature

                    # New Reading if valid air pressure reading
                    # Check if air pressure is a float
                    try:
                        float(temperature)
                        entry["year"] = current_year
                        entry["month"] = current_month
                        entry["day"] = current_day
                        entry["hour"] = current_hour
                        entry["total_temperature"] = float(temperature)
                        entry["num_readings"] = 1
                    except ValueError:
                        print "Not a float"

        print "close file"
        write_file.close()



if __name__ == '__main__':
    noah_temperature()
