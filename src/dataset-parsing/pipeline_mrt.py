from mrt_passenger_flow_encoded import mrt_passenger_flow_encoded
from mrt_passenger_flow_entry_exit import mrt_passenger_flow_entry_exit

def pipeline_mrt():

    # Parses the interim csv files
    mrt_passenger_flow_encoded(2016)
    mrt_passenger_flow_encoded(2017)

    # Separates entry and exit
    mrt_passenger_flow_entry_exit()




if __name__ == '__main__':
    pipeline_mrt()