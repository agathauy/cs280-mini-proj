'''
    Parses time and date data from timeanddate.com
'''

import os
import time
import json
import numpy as np
from pprint import pprint

SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))

def time_and_date():
    # Timer function
    time_start = time.time()


    # Result path
    result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "time_and_date", "makati")
    file_name = "all_makati.csv"
    result_file_path = os.path.join(result_folder_path, file_name)
    write_file = open(result_file_path, 'w')
    write_file.truncate()


    # Create csv file that follows this format
    # year, month, day, hour, temperature, air_pressure
    str_a = "year,month,day,hour,temperature,avg_air_pressure"
    print str_a
    write_file.write(str_a + '\n')

    # Source path
    folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "raw", "time_and_date", "makati_json")
    list_years = [2016, 2017]
    list_months = ['Enero', 'Pebrero', 'Marso', 'Abril', 'Mayo', 'Hunyo', 'Hulyo', 'Agosto', 'Setyembre', 'Oktubre', 'Nobyembre', 'Disyembre']
    list_days = ['Linggo', 'Lunes', 'Martes', 'Miyerkules', 'Huwebes', 'Biyernes', 'Sabado']
    list_hours = {
        "00:00": [0, 1, 2, 3, 4, 5],
        "06:00": [6, 7, 8, 9, 10, 11],
        "12:00": [12, 13, 14, 15, 16, 17],
        "18:00": [18, 19, 20, 21, 22, 23]
    }

    for i, year in enumerate(list_years):
        for j in range(1, 13):
            month = j
            file_name = "{}_{}.json".format(year, month)
            file_path = os.path.join(folder_path, file_name)
            print file_path
            with open(file_path) as data_file:
                data = json.load(data_file)
            print len(data["detail"])
            for k, entry in enumerate(data["detail"]):
                list_date = entry["ds"].split(", ")
                day_of_the_week = list_days.index(list_date[0])
                entry_day = list_date[1].split(" ")[0]
                entry_month = list_months.index(list_date[1].split(" ")[1])

                for m, hour in enumerate(list_hours[entry["ts"]]):
                    str_a = "{},{},{},{},{},{}".format(year, entry_month, entry_day, hour, entry["temp"], entry["baro"])
                    write_file.write(str_a + '\n')

            #pprint(data)
            # Test print data
            '''
            print data["detail"][0]
            print data["detail"][0]["ts"]
            print data["detail"][0]["ds"]
            print data["detail"][0]["temp"]
            print data["detail"][0]["baro"]
            '''






    print "close file"
    print result_file_path
    write_file.close()

    # End of timer
    elapsed = time.time() - time_start
    str_a = 'Total Time Elapsed: {}'.format(time.strftime("%H:%M:%S", time.gmtime(elapsed)))
    print str_a

if __name__ == '__main__':
    time_and_date()