'''
    Combine the following datasets for entry and exit
        - MRT
            - flow num (separate for entry and exit versions)
            - station
        - Date and time
            - month
            - day
            - day of the week
            - time
        - Weather
            - air pressure (nearest weather station)
            - temperature (DOST Bicutan)

        flow_level,station,month,day,day_of_the_week,time,avg_air_pressure,temperature
'''
import os
import numpy as np
import pandas as pd
import time


SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))


def master_combine():
    # Timer function
    time_start = time.time()

    # Result path
    result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "master_combined")

    # MRT data 
    # "# year,month,day,day_of_the_week,time,station,flow_type,flow_num"

    folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "MRT_passenger_flow_entry_exit_w_output")
    file_path = os.path.join(folder_path, "merged_0_ENTRY.csv")
    df_entry = pd.read_csv(file_path, encoding='utf-8')
    print df_entry.head()
    #mrt_entry = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=int)
    file_path = os.path.join(folder_path, "merged_1_EXIT.csv")
    #mrt_exit = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=int)
    df_exit = pd.read_csv(file_path, encoding='utf-8')
    print df_exit.head()

    # Weather data
    # Timeanddate.com: Temperature and air pressure from Makati
    folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "time_and_date", "makati")
    file_path = os.path.join(folder_path, "all_makati.csv")
    # year,month,day,hour,temperature,avg_air_pressure
    df_makati = pd.read_csv(file_path)
    print df_makati.head()
    #temp_ap_makati = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=[('year', int), ('month', int), ('day', int), ('hour', int), ('temp', int) ('avg_air_pressure', float)])


    # Noah Dataset: Air pressure from Mandaluyong and Quezon City
    # year,month,day,hour,avg_air_pressure
    folder_path = os.path.join(SCRIPT_DIR, '..', "..", "data", "processed", "noah_air_pressure")
    file_path = os.path.join(folder_path, "HYBRID_190_SCIENCEGARDENPAGASA-IDP_QUEZONCITY_METROMANILA_2.csv")
    #ap_qc = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=[('year', int), ('month', int), ('day', int), ('hour', int), ('avg_air_pressure', float)])
    df_qc = pd.read_csv(file_path)
    print df_qc.head()

    file_path = os.path.join(folder_path, "HYDROMET_968_NATIONALCENTERFORMENTALHEALTH_MANDALUYONG_METROMANILA_2.csv")
    #p_mandaluyong = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=[('year', int), ('month', int), ('day', int), ('hour', int), ('avg_air_pressure', float)])
    df_mandaluyong = pd.read_csv(file_path)
    print df_mandaluyong.head()

    # Form structure for dataset_mrt_entry
    # flow_level,station,month,day,day_of_the_week,hour,avg_air_pressure,temperature
    #dataset_entry = np.zeros((mrt_entry.shape[0], 8))
    #dataset_exit = np.zeros((mrt_exit.shape[0], 8))
    column_names = ['flow_level','year','station','month','day','day_of_the_week','hour','avg_air_pressure','temperature']
    dataset_entry = pd.DataFrame(columns=column_names)
    dataset_entry['year'] = df_entry['# year']
    dataset_entry['flow_level'] = df_entry["flow_num"]
    dataset_entry['station'] = df_entry["station"]
    dataset_entry['month'] = df_entry['month']
    dataset_entry['day'] = df_entry['day']
    dataset_entry['day_of_the_week'] = df_entry['day_of_the_week']
    dataset_entry['hour'] = df_entry['time']

    dataset_exit = pd.DataFrame(columns=column_names)
    dataset_exit['year'] = df_exit['# year']
    dataset_exit['flow_level'] = df_exit["flow_num"]
    dataset_exit['station'] = df_exit["station"]
    dataset_exit['month'] = df_exit['month']
    dataset_exit['day'] = df_exit['day']
    dataset_exit['day_of_the_week'] = df_exit['day_of_the_week']
    dataset_exit['hour'] = df_exit['time']

    print dataset_entry.head()
    num_rows = len(dataset_entry['flow_level'])


    # Go through each entry and attach remaining variables
    for i in range(0, num_rows):
        # For adding air pressure
        year = dataset_entry['year'][i]
        month = dataset_entry['month'][i]
        day = dataset_entry['day'][i]
        hour = dataset_entry['hour'][i]
        station = dataset_entry['station'][i]
        print year, month, day, hour


        temp_makati = df_makati[(df_makati['year'] == year) & (df_makati['month'] == month) & (df_makati['day'] == day) & (df_makati['hour'] == hour)]

        if (station >= 0 and station <= 3):
            # Quezon city data
            #print "qc"
            temp = df_qc[(df_qc['year'] == year) & (df_qc['month'] == month) & (df_qc['day'] == day) & (df_qc['hour'] == hour)]
            # print df_qc.head()
            if len(temp['year']) == 0:
                air_pressure = temp_makati['avg_air_pressure'][temp_makati.index[0]]
        elif (station >= 4 and station <= 7):
            # Mandaluyong data
            #print 'mandaluyong'
            temp = df_mandaluyong[(df_mandaluyong['year'] == year) & (df_mandaluyong['month'] == month) & (df_mandaluyong['day'] == day) & (df_mandaluyong['hour'] == hour)]
            if len(temp['year']) == 0:
                air_pressure = temp_makati['avg_air_pressure'][temp_makati.index[0]]
        else:
            # Makati data
            #print 'makati'
            air_pressure = temp_makati['avg_air_pressure'][temp_makati.index[0]]
        print "i = {}".format(i)
        temperature = temp_makati['temperature'][temp_makati.index[0]]
        print temperature, air_pressure
        dataset_entry['avg_air_pressure'][i] = air_pressure
        dataset_entry['temperature'][i] = temperature

        # For adding air pressure
        year = dataset_exit['year'][i]
        month = dataset_exit['month'][i]
        day = dataset_exit['day'][i]
        hour = dataset_exit['hour'][i]
        station = dataset_exit['station'][i]
        print year, month, day, hour


        temp_makati = df_makati[(df_makati['year'] == year) & (df_makati['month'] == month) & (df_makati['day'] == day) & (df_makati['hour'] == hour)]

        if (station >= 0 and station <= 3):
            # Quezon city data
            #print "qc"
            temp = df_qc[(df_qc['year'] == year) & (df_qc['month'] == month) & (df_qc['day'] == day) & (df_qc['hour'] == hour)]
            # print df_qc.head()
            if len(temp['year']) == 0:
                air_pressure = temp_makati['avg_air_pressure'][temp_makati.index[0]]
        elif (station >= 4 and station <= 7):
            # Mandaluyong data
            #print 'mandaluyong'
            temp = df_mandaluyong[(df_mandaluyong['year'] == year) & (df_mandaluyong['month'] == month) & (df_mandaluyong['day'] == day) & (df_mandaluyong['hour'] == hour)]
            if len(temp['year']) == 0:
                air_pressure = temp_makati['avg_air_pressure'][temp_makati.index[0]]
        else:
            # Makati data
            #print 'makati'
            air_pressure = temp_makati['avg_air_pressure'][temp_makati.index[0]]
        print "i = {}".format(i)
        temperature = temp_makati['temperature'][temp_makati.index[0]]
        print temperature, air_pressure
        dataset_exit['avg_air_pressure'][i] = air_pressure
        dataset_exit['temperature'][i] = temperature


    result_file_path = os.path.join(result_folder_path, "all_0_ENTRY.csv")
    print result_file_path
    dataset_entry.to_csv(result_file_path, index=False,  quoting=csv.QUOTE_NONE, quotechar="",  escapechar="\\")

    result_file_path = os.path.join(result_folder_path, "all_0_EXIT.csv")
    print result_file_path
    dataset_exit.to_csv(result_file_path, index=False,  quoting=csv.QUOTE_NONE, quotechar="",  escapechar="\\")




    # End of timer
    elapsed = time.time() - time_start
    str_a = 'Total Time Elapsed: {}'.format(time.strftime("%H:%M:%S", time.gmtime(elapsed)))
    print str_a

if __name__ == '__main__':
    master_combine()