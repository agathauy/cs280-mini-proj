'''
    Combine the following datasets for entry and exit


    column_names = ['flow_level','year','station','month','day','day_of_the_week','hour','avg_air_pressure','temperature', 'holiday_type', 'payday_type', 'traffic_type']
'''
import os
import numpy as np
import pandas as pd
import time


SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))


def master_combine():
    # Timer function
    time_start = time.time()

    # Result path
    result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "master_combined")

    # MRT data 
    # "# year,month,day,day_of_the_week,time,station,flow_type,flow_num"

    folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "MRT_passenger_flow_entry_exit_w_output")
    file_path = os.path.join(folder_path, "merged_0_ENTRY.csv")
    print file_path
    #df_entry = pd.read_csv(file_path, encoding='utf-8')
    #print df_entry.head()
    column_types = [('year', int), ('month', int),('day', int), ('day_of_the_week', int), ('time', int), ('station', int), ('flow_type', int), ('flow_num', int)]
    df_entry = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=column_types)
    file_path = os.path.join(folder_path, "merged_1_EXIT.csv")
    print file_path



    df_exit = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=column_types)
    #df_exit = pd.read_csv(file_path, encoding='utf-8')
    #print df_exit.head()

    # Weather data
    # Timeanddate.com: Temperature and air pressure from Makati
    folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "time_and_date", "makati")
    file_path = os.path.join(folder_path, "all_makati.csv")
    print file_path

    # year,month,day,hour,temperature,avg_air_pressure
    #df_makati = pd.read_csv(file_path)
    #print df_makati.head()
    df_makati = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=[('year', int), ('month', int), ('day', int), ('hour', int), ('temperature', int), ('avg_air_pressure', float)])


    # Noah Dataset: Air pressure from Mandaluyong and Quezon City
    # year,month,day,hour,avg_air_pressure
    folder_path = os.path.join(SCRIPT_DIR, '..', "..", "data", "processed", "noah_air_pressure")
    file_path = os.path.join(folder_path, "HYBRID_190_SCIENCEGARDENPAGASA-IDP_QUEZONCITY_METROMANILA_2.csv")
    print file_path

    df_qc = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=[('year', int), ('month', int), ('day', int), ('hour', int), ('avg_air_pressure', float)])
    #df_qc = pd.read_csv(file_path)
    #print df_qc.head()

    file_path = os.path.join(folder_path, "HYDROMET_968_NATIONALCENTERFORMENTALHEALTH_MANDALUYONG_METROMANILA_2.csv")
    print file_path

    df_mandaluyong = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=[('year', int), ('month', int), ('day', int), ('hour', int), ('avg_air_pressure', float)])
    #df_mandaluyong = pd.read_csv(file_path)
    #print df_mandaluyong.head()


    # Load Holiday dataset

    folder_path = os.path.join(SCRIPT_DIR, '..', "..", "data", "raw", "holiday_dates")
    file_path = os.path.join(folder_path, 'holiday_dates_w_variants.csv')
    df_holiday = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=[('year', int), ('month', int), ('day', int), ('holiday_type', int)])


    # Load Payday dataset

    folder_path = os.path.join(SCRIPT_DIR, '..', "..", "data", "raw", "payday_dates")
    file_path = os.path.join(folder_path, 'payday_dates.csv')
    df_payday = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=[('year', int), ('month', int), ('day', int), ('payday_type', int)])


    # Load traffic incident dataset
    folder_path = os.path.join(SCRIPT_DIR, '..', "..", "data", "processed", "MMDA_traffic_incidents_cleaned")
    file_path = os.path.join(folder_path, "partial_cleaned.csv")
    df_traffic = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=[('year', int), ('month', int), ('day', int), ('time', int), ('place', int), ('direction', int)])

    # Form structure for dataset_mrt_entry
    # flow_level,station,month,day,day_of_the_week,hour,avg_air_pressure,temperature
    #dataset_entry = np.zeros((mrt_entry.shape[0], 8))
    #dataset_exit = np.zeros((mrt_exit.shape[0], 8))
    column_names = ['flow_level','year','station','month','day','day_of_the_week','hour','avg_air_pressure','temperature', 'holiday_type', 'payday_type', 'traffic_type']
    column_types= [('flow_level', int),('year', int),('station', int),('month', int),('day', int),('day_of_the_week', int),('hour', int),('avg_air_pressure', float),('temperature', int), ('holiday_type', float),('payday_type', int), ('traffic_type', int)]
    print "here"
    #print len(df_entry['year'])
    # Find number of occurences of valid stations
    '''
    >>> unique, counts = numpy.unique(a, return_counts=True)
    >>> dict(zip(unique, counts))
    {0: 7, 1: 4, 2: 1, 3: 2, 4: 1}
    '''
    unique, counts = np.unique(df_entry['time'], return_counts=True)
    counter = dict(zip(unique, counts))
    num_entries = 0
    # Valid hours, 5 to 10
    # Invalid, 11, to 4
    for i in range(len(counter)):
        if (i >= 5 and i <=10):
            num_entries+= counter[i]



    dataset_entry = np.zeros((num_entries,), dtype=column_types)
    #dataset_entry = pd.DataFrame(columns=column_names)
    '''
    dataset_entry['year'] = df_entry['year']
    dataset_entry['flow_level'] = df_entry["flow_num"]
    dataset_entry['station'] = df_entry["station"]
    dataset_entry['month'] = df_entry['month']
    dataset_entry['day'] = df_entry['day']
    dataset_entry['day_of_the_week'] = df_entry['day_of_the_week']
    dataset_entry['hour'] = df_entry['time']
    '''
    #dataset_exit = pd.DataFrame(columns=column_names)

    dataset_exit = np.zeros((num_entries,), dtype=column_types)
    '''
    dataset_exit['year'] = df_exit['year']
    dataset_exit['flow_level'] = df_exit["flow_num"]
    dataset_exit['station'] = df_exit["station"]
    dataset_exit['month'] = df_exit['month']
    dataset_exit['day'] = df_exit['day']
    dataset_exit['day_of_the_week'] = df_exit['day_of_the_week']
    dataset_exit['hour'] = df_exit['time']
    '''
    #print dataset_entry.head()
    #num_rows = len(dataset_entry['flow_level'])
    num_rows = len(df_entry['year'])

    # Go through each entry and attach remaining variables
    for i in range(0, num_rows):
        # For adding air pressure
        '''
        year = dataset_entry['year'][i]
        month = dataset_entry['month'][i]
        day = dataset_entry['day'][i]
        hour = dataset_entry['hour'][i]
        station = dataset_entry['station'][i]
        '''
        hour = df_entry['time'][i]
        if not (hour >= 5 and hour <=10):
            continue


        year = df_entry['year'][i]
        month = df_entry['month'][i]
        day = df_entry['day'][i]
        station = df_entry['station'][i]

        dataset_entry['hour'] = hour
        dataset_entry['year'] = year
        dataset_entry['month'] = month
        dataset_entry['day'] = day
        dataset_entry['station'] = station



        print year, month, day, hour


        temp_makati = df_makati[(df_makati['year'] == year) & (df_makati['month'] == month) & (df_makati['day'] == day) & (df_makati['hour'] == hour)]
        temp_holiday =  df_holiday[(df_holiday['year'] == year) & (df_holiday['month'] == month) & (df_holiday['day'] == day)]
        temp_payday =  df_payday[(df_payday['year'] == year) & (df_payday['month'] == month) & (df_payday['day'] == day)]

        temp_traffic = df_traffic[(df_traffic['year'] == year) & (df_traffic['month'] == month) & (df_traffic['day'] == day) & (df_traffic['time'] == hour) & (df_traffic['place'] == station)]


        print temp_makati
        if (station >= 0 and station <= 3):
            # Quezon city data
            #print "qc"
            temp = df_qc[(df_qc['year'] == year) & (df_qc['month'] == month) & (df_qc['day'] == day) & (df_qc['hour'] == hour)]
            # print df_qc.head()
            print temp
            if len(temp) == 0:
                air_pressure = temp_makati[0][-1]
            else:
                air_pressure = temp[0][-1]

        elif (station >= 4 and station <= 7):
            # Mandaluyong data
            #print 'mandaluyong'
            temp = df_mandaluyong[(df_mandaluyong['year'] == year) & (df_mandaluyong['month'] == month) & (df_mandaluyong['day'] == day) & (df_mandaluyong['hour'] == hour)]

            if len(temp) == 0:
                air_pressure = temp_makati[0][-1]
            else:
                air_pressure = temp[0][-1]

        else:
            # Makati data
            #print 'makati'
            air_pressure = temp_makati[0][-1]
        print "i = {}".format(i)
        temperature = temp_makati[0][-2]
        print temperature, air_pressure
        if len(temp_payday) == 0:
            payday_type = 0
        else:
            payday_type = temp_payday[0][-1]
        if len(temp_holiday) == 0:
            holiday_type = 0
        else:
            holiday_type = temp_holiday[0][-1]
        if len(temp_traffic) == 0:
            traffic_type = 0
        else:
            traffic_type = temp_traffic[0][-1]

        print payday_type, holiday_type, traffic_type
        dataset_entry['avg_air_pressure'][i] = air_pressure
        dataset_entry['temperature'][i] = temperature
        dataset_entry['payday_type'][i] = payday_type
        dataset_entry['holiday_type'][i] = holiday_type
        dataset_entry['traffic_type'][i] = traffic_type

        # For adding air pressure

        hour = df_exit['time'][i]
        year = df_exit['year'][i]
        month = df_exit['month'][i]
        day = df_exit['day'][i]
        station = df_exit['station'][i]

        dataset_exit['hour'] = hour
        dataset_exit['year'] = year
        dataset_exit['month'] = month
        dataset_exit['day'] = day
        dataset_exit['station'] = station
        print year, month, day, hour



        temp_makati = df_makati[(df_makati['year'] == year) & (df_makati['month'] == month) & (df_makati['day'] == day) & (df_makati['hour'] == hour)]
        temp_holiday =  df_holiday[(df_holiday['year'] == year) & (df_holiday['month'] == month) & (df_holiday['day'] == day)]
        temp_payday =  df_payday[(df_payday['year'] == year) & (df_payday['month'] == month) & (df_payday['day'] == day)]
        temp_traffic = df_traffic[(df_traffic['year'] == year) & (df_traffic['month'] == month) & (df_traffic['day'] == day) & (df_traffic['time'] == hour) & (df_traffic['place'] == station)]

        print temp_makati
        if (station >= 0 and station <= 3):
            # Quezon city data
            #print "qc"
            temp = df_qc[(df_qc['year'] == year) & (df_qc['month'] == month) & (df_qc['day'] == day) & (df_qc['hour'] == hour)]
            # print df_qc.head()
            if len(temp['year']) == 0:
                air_pressure = temp_makati[0][-1]
            else:
                air_pressure = temp[0][-1]
        elif (station >= 4 and station <= 7):
            # Mandaluyong data
            #print 'mandaluyong'
            temp = df_mandaluyong[(df_mandaluyong['year'] == year) & (df_mandaluyong['month'] == month) & (df_mandaluyong['day'] == day) & (df_mandaluyong['hour'] == hour)]

            if len(temp['year']) == 0:
                air_pressure = temp_makati[0][-1]
            else:
                air_pressure = temp[0][-1]
        else:
            # Makati data
            #print 'makati'
            air_pressure = temp_makati[0][-1]
        print "i = {}".format(i)
        temperature = temp_makati[0][-2]
        print temperature, air_pressure
        if len(temp_payday) == 0:
            payday_type = 0
        else:
            payday_type = temp_payday[0][-1]
        if len(temp_holiday) == 0:
            holiday_type = 0
        else:
            holiday_type = temp_holiday[0][-1]

        if len(temp_traffic) == 0:
            traffic_type = 0
        else:
            traffic_type = temp_traffic[0][-1]

        print payday_type, holiday_type, traffic_type
        dataset_entry['avg_air_pressure'][i] = air_pressure
        dataset_entry['temperature'][i] = temperature
        dataset_entry['payday_type'][i] = payday_type
        dataset_entry['holiday_type'][i] = holiday_type
        dataset_entry['traffic_type'][i] = traffic_type

        print payday_type, holiday_type
        dataset_entry['avg_air_pressure'][i] = air_pressure
        dataset_entry['temperature'][i] = temperature
        dataset_entry['payday_type'][i] = payday_type
        dataset_entry['holiday_type'][i] = holiday_type

    result_header = "flow_level,year,station,month,day,day_of_the_week,hour,avg_air_pressure,temperature,holiday_type,payday_type,traffic_type"




    result_file_path = os.path.join(result_folder_path, "all_0_ENTRY.csv")
    print result_file_path
    np.savetxt(result_file_path, dataset_entry, delimiter=",", fmt='%d', header=result_header)

    result_file_path = os.path.join(result_folder_path, "all_1_EXIT.csv")
    print result_file_path
    np.savetxt(result_file_path, dataset_exit, delimiter=",", fmt='%d', header=result_header)




    # End of timer
    elapsed = time.time() - time_start
    str_a = 'Total Time Elapsed: {}'.format(time.strftime("%H:%M:%S", time.gmtime(elapsed)))
    print str_a

if __name__ == '__main__':
    master_combine()