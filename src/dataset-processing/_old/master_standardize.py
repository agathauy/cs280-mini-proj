import os
import numpy as np
import pandas as pd
import time
from sklearn.externals import joblib
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split


SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))


def master_standardize():
    # Timer function
    time_start = time.time()
    result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "master_standardized")
    result_file_path = os.path.join(result_folder_path, "notes_dataset.txt")
    write_file = open(result_file_path, 'w')
    write_file.truncate()


    folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "master_combined")

    #column_types= [('flow_level', int),('year', int),('station', int),('month', int),('day', int),('day_of_the_week', int),('hour', int),('avg_air_pressure', float),('temperature', int)]

    file_path = os.path.join(folder_path, "all_0_ENTRY.csv")
    str_a =  file_path
    write_file.write(str_a + '\n')

    df_entry = np.genfromtxt(file_path, delimiter=',', skip_header=1)

    unique, counts = np.unique( df_entry[:, 0], return_counts=True)
    str_a = "Entry Dataset Shape: {}".format(df_entry.shape)
    write_file.write(str_a + '\n')
    str_a = "Dataset Distribution:"
    write_file.write(str_a + '\n')
    str_a =  str(np.asarray((unique, counts)).T)
    write_file.write(str_a + '\n')

    file_path = os.path.join(folder_path, "all_1_EXIT.csv")
    str_a =  file_path
    write_file.write(str_a + '\n')

    df_exit = np.genfromtxt(file_path, delimiter=',', skip_header=1)
    unique, counts = np.unique( df_exit[:, 0], return_counts=True)
    str_a = "Exit Dataset Shape: {}".format(df_exit.shape)
    write_file.write(str_a + '\n')
    str_a = "Dataset Distribution:"
    write_file.write(str_a + '\n')
    str_a = str(np.asarray((unique, counts)).T)
    write_file.write(str_a + '\n')

    x_train_entry, x_test_entry, y_train_entry, y_test_entry = train_test_split(df_entry[:, 2:], df_entry[:, 0], test_size=0.2, random_state=5, stratify=df_entry[:, 0])
    unique, counts = np.unique( y_train_entry, return_counts=True)
    str_a = "Entry Training Dataset Shape: {}".format(x_train_entry.shape)
    write_file.write(str_a + '\n')
    str_a = "Entry Training Dataset Distribution:"
    write_file.write(str_a + '\n')
    str_a =  str(np.asarray((unique, counts)).T)
    write_file.write(str_a + '\n')

    unique, counts = np.unique( y_test_entry, return_counts=True)
    str_a = "Entry Test Dataset Shape: {}".format(x_test_entry.shape)
    write_file.write(str_a + '\n')
    str_a = "Entry Test Dataset Distribution:"
    write_file.write(str_a + '\n')
    str_a =  str(np.asarray((unique, counts)).T)
    write_file.write(str_a + '\n')


    x_train_exit, x_test_exit, y_train_exit, y_test_exit = train_test_split(df_exit[:, 2:], df_exit[:, 0], test_size=0.2, random_state=5, stratify=df_exit[:, 0])
    unique, counts = np.unique( y_train_exit, return_counts=True)
    str_a = "Exit Training Dataset Shape: {}".format(x_train_exit.shape)
    write_file.write(str_a + '\n')
    str_a = "Exit Training Dataset Distribution:"
    write_file.write(str_a + '\n')
    str_a =  str(np.asarray((unique, counts)).T)
    write_file.write(str_a + '\n')

    unique, counts = np.unique( y_test_exit, return_counts=True)
    str_a = "Exit Test Dataset Shape: {}".format(x_test_exit.shape)
    write_file.write(str_a + '\n')
    str_a = "Exit Test Dataset Distribution:"
    write_file.write(str_a + '\n')
    str_a =  str(np.asarray((unique, counts)).T)
    write_file.write(str_a + '\n')

    '''
    >>> from sklearn.preprocessing import StandardScaler
    >>> data = [[0, 0], [0, 0], [1, 1], [1, 1]]
    >>> scaler = StandardScaler()
    >>> print(scaler.fit(data))
    StandardScaler(copy=True, with_mean=True, with_std=True)
    >>> print(scaler.mean_)
    [0.5 0.5]
    >>> print(scaler.transform(data))
    [[-1. -1.]
    [-1. -1.]
    [ 1.  1.]
    [ 1.  1.]]
    >>> print(scaler.transform([[2, 2]]))
    [[3. 3.]]

    new_scaler = StandardScaler()
    # ...fit it... do something ...

    joblib.dump(new_scaler , 'my_scaler.pkl')     # save to disk

    loaded_scaler = joblib.load('my_scaler.pkl')  # load from disk

    '''

    # For entry
    scaler_entry = StandardScaler()
    scaler_entry.fit(x_train_entry)
    x_train_entry_s = scaler_entry.transform(x_train_entry)
    x_test_entry_s = scaler_entry.transform(x_test_entry)



    file_path = os.path.join(result_folder_path, 'scaler_entry.pkl')
    str_a =  file_path
    write_file.write(str_a + '\n')
    joblib.dump(scaler_entry, file_path)

    result_header = "station,month,day,day_of_the_week,hour,avg_air_pressure,temperature"
    y_result_header = "flow_level"
    result_file_path =  os.path.join(result_folder_path, "entry_train_x.csv")
    str_a =  result_file_path
    write_file.write(str_a + '\n')
    np.savetxt(result_file_path, x_train_entry_s, delimiter=",", header=result_header)
    result_file_path =  os.path.join(result_folder_path, "entry_train_y.csv")
    str_a =  result_file_path
    write_file.write(str_a + '\n')
    np.savetxt(result_file_path, y_train_entry, delimiter=",", fmt="%d", header=y_result_header)

    result_file_path =  os.path.join(result_folder_path, "entry_test_x.csv")
    str_a =  result_file_path
    write_file.write(str_a + '\n')
    np.savetxt(result_file_path, x_test_entry_s, delimiter=",", header=result_header)
    result_file_path =  os.path.join(result_folder_path, "entry_test_y.csv")
    str_a =  result_file_path
    write_file.write(str_a + '\n')
    np.savetxt(result_file_path, y_test_entry, delimiter=",", fmt="%d", header=y_result_header)


    # For exit
    scaler_exit = StandardScaler()
    scaler_exit.fit(x_train_exit)
    x_train_exit_s = scaler_exit.transform(x_train_exit)
    #print x_test_exit[0:5, :]
    x_test_exit_s = scaler_exit.transform(x_test_exit)
    #print x_test_exit_s[0:5, :]
    #print scaler_exit.inverse_transform(x_test_exit_s[0:5, :])




    result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "master_standardized")
    file_path = os.path.join(result_folder_path, 'scaler_exit.pkl')
    str_a =  result_file_path
    write_file.write(str_a + '\n')
    joblib.dump(scaler_exit, file_path)

    result_header = "station,month,day,day_of_the_week,hour,avg_air_pressure,temperature"
    y_result_header = "flow_level"
    result_file_path =  os.path.join(result_folder_path, "exit_train_x.csv")
    str_a =  result_file_path
    write_file.write(str_a + '\n')

    np.savetxt(result_file_path, x_train_exit_s, delimiter=",", header=result_header)
    result_file_path =  os.path.join(result_folder_path, "exit_train_y.csv")
    str_a =  result_file_path
    write_file.write(str_a + '\n')
    np.savetxt(result_file_path, y_train_exit, delimiter=",", fmt="%d", header=y_result_header)

    result_file_path =  os.path.join(result_folder_path, "exit_test_x.csv")
    str_a =  result_file_path
    write_file.write(str_a + '\n')
    np.savetxt(result_file_path, x_test_exit_s, delimiter=",", header=result_header)
    result_file_path =  os.path.join(result_folder_path, "exit_test_y.csv")
    str_a =  result_file_path
    write_file.write(str_a + '\n')
    np.savetxt(result_file_path, y_test_exit, delimiter=",", fmt="%d", header=y_result_header)

    
    # End of timer
    elapsed = time.time() - time_start
    str_a = 'Total Time Elapsed: {}'.format(time.strftime("%H:%M:%S", time.gmtime(elapsed)))
    print str_a
    write_file.write(str_a + '\n')
    write_file.close()

if __name__ == '__main__':
    master_standardize()