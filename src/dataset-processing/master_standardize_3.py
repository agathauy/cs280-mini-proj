import os
import numpy as np
import pandas as pd
import time
from sklearn.externals import joblib
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import MinMaxScaler

SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))

def master_standardize_3():
    # Timer function
    time_start = time.time()

    result_header = "station,month,day,day_of_the_week,hour,num_trains,avg_air_pressure,temperature,holiday_type,payday_type,traffic_type"
    y_result_header = "flow_level"
    result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "master_standardized_3")
    if not os.path.isdir(result_folder_path):
        os.makedirs(result_folder_path)
    result_file_path = os.path.join(result_folder_path, "notes_dataset.txt")
    write_file = open(result_file_path, 'w')
    write_file.truncate()


    folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "master_combined_3")

    file_path = os.path.join(folder_path, "all_0_ENTRY.csv")
    str_a =  file_path
    print str_a
    write_file.write(str_a + '\n')

    df_entry = np.genfromtxt(file_path, delimiter=',', skip_header=1)

    unique, counts = np.unique( df_entry[:, 0], return_counts=True)
    str_a = "Entry Dataset Shape: {}".format(df_entry.shape)
    print str_a
    write_file.write(str_a + '\n')
    str_a = "Dataset Distribution:"
    print str_a
    write_file.write(str_a + '\n')
    str_a =  str(np.asarray((unique, counts)).T)
    print str_a
    write_file.write(str_a + '\n')

    file_path = os.path.join(folder_path, "all_1_EXIT.csv")
    str_a =  file_path
    print str_a
    write_file.write(str_a + '\n')

    df_exit = np.genfromtxt(file_path, delimiter=',', skip_header=1)
    unique, counts = np.unique( df_exit[:, 0], return_counts=True)
    str_a = "Exit Dataset Shape: {}".format(df_exit.shape)
    print str_a
    write_file.write(str_a + '\n')
    str_a = "Dataset Distribution:"
    print str_a
    write_file.write(str_a + '\n')
    str_a = str(np.asarray((unique, counts)).T)
    print str_a
    write_file.write(str_a + '\n')



    # ########################## ENTRY 

    flow_type = "entry"


    X_train, X_test = df_entry[:, 2:], df_entry[:, 2:]
    y_train, y_test = df_entry[:, 0], df_entry[:, 0]

    str_a = "# ENTIRE TRAINING Dataset ----------------------------"
    print str_a
    write_file.write(str_a + '\n')
    # Print details about the distribution of the datasets
    unique, counts = np.unique( y_train, return_counts=True)
    str_a = "Entry Training Dataset Shape: {}".format(X_train.shape)
    print str_a
    write_file.write(str_a + '\n')
    str_a = "Entry Training Dataset Distribution:"
    print str_a
    write_file.write(str_a + '\n')
    str_a =  str(np.asarray((unique, counts)).T)
    print str_a
    write_file.write(str_a + '\n')

    unique, counts = np.unique( y_test, return_counts=True)
    str_a = "Entry Test Dataset Shape: {}".format(X_test.shape)
    print str_a
    write_file.write(str_a + '\n')
    str_a = "Entry Test Dataset Distribution:"
    print str_a
    write_file.write(str_a + '\n')
    str_a =  str(np.asarray((unique, counts)).T)
    print str_a
    write_file.write(str_a + '\n')

    # Standardize Data
    scaler = MinMaxScaler(feature_range=(-1,1))
    scaler.fit(X_train)
    X_train_s = scaler.transform(X_train)
    X_test_s = scaler.transform(X_test)

    # Save the scaler
    file_name = 'scaler_{}.pkl'.format(flow_type)
    file_path = os.path.join(result_folder_path, file_name)
    str_a =  file_path
    print str_a
    write_file.write(str_a + '\n')
    joblib.dump(scaler, file_path)

    # Save the data

    file_name = "{}_X_train.csv".format(flow_type)
    result_file_path = os.path.join(result_folder_path, file_name)
    str_a =  result_file_path
    print str_a

    write_file.write(str_a + '\n')
    np.savetxt(result_file_path, X_train_s, delimiter=",", comments='', header=result_header)
    file_name = "{}_y_train.csv".format(flow_type)
    result_file_path = os.path.join(result_folder_path, file_name)
    str_a =  result_file_path
    print str_a
    write_file.write(str_a + '\n')
    np.savetxt(result_file_path, y_train, delimiter=",",  fmt="%d",  comments='',  header=y_result_header)

    file_name = "{}_X_test.csv".format(flow_type)
    result_file_path = os.path.join(result_folder_path, file_name)
    str_a =  result_file_path
    print str_a
    write_file.write(str_a + '\n')
    np.savetxt(result_file_path, X_test_s, delimiter=",", comments='',  header=result_header)
    file_name = "{}_y_test.csv".format(flow_type)
    result_file_path = os.path.join(result_folder_path, file_name)
    str_a =  result_file_path
    print str_a
    write_file.write(str_a + '\n')
    np.savetxt(result_file_path, y_test, delimiter=",", fmt="%d", comments='',  header=y_result_header)



    # Doing 5-Fold divsion on the datasets
    # Do 5-fold divisions
    skf_entry = StratifiedKFold(n_splits=5, random_state=5)
    skf_entry.get_n_splits(df_entry[:, 2:],  df_entry[:, 0])
    num_fold = 0
    flow_type = "entry"
    str_a = "# ENTRY DATASETS 5-FOLD -----------------------"
    print str_a
    write_file.write(str_a + '\n')
    for train_index, test_index in skf_entry.split(df_entry[:, 2:], df_entry[:, 0]):
        X_train, X_test = df_entry[train_index, 2:], df_entry[test_index, 2:]
        y_train, y_test = df_entry[train_index, 0], df_entry[test_index, 0]

        str_a = "# 0th Fold Dataset ----------------------------"
        print str_a
        write_file.write(str_a + '\n')
        # Print details about the distribution of the datasets
        unique, counts = np.unique( y_train, return_counts=True)
        str_a = "Entry Training Dataset Shape: {}".format(X_train.shape)
        print str_a
        write_file.write(str_a + '\n')
        str_a = "Entry Training Dataset Distribution:"
        print str_a
        write_file.write(str_a + '\n')
        str_a =  str(np.asarray((unique, counts)).T)
        print str_a
        write_file.write(str_a + '\n')

        unique, counts = np.unique( y_test, return_counts=True)
        str_a = "Entry Test Dataset Shape: {}".format(X_test.shape)
        print str_a
        write_file.write(str_a + '\n')
        str_a = "Entry Test Dataset Distribution:"
        print str_a
        write_file.write(str_a + '\n')
        str_a =  str(np.asarray((unique, counts)).T)
        print str_a
        write_file.write(str_a + '\n')

        # Standardize Data
        scaler = MinMaxScaler(feature_range=(-1,1))
        scaler.fit(X_train)
        X_train_s = scaler.transform(X_train)
        X_test_s = scaler.transform(X_test)

        # Save the scaler
        file_name = 'scaler_{}_{}.pkl'.format(flow_type,num_fold)
        file_path = os.path.join(result_folder_path, file_name)
        str_a =  file_path
        print str_a
        write_file.write(str_a + '\n')
        joblib.dump(scaler, file_path)

        # Save the data

        file_name = "{}_{}_X_train.csv".format(num_fold, flow_type)
        result_file_path = os.path.join(result_folder_path, file_name)
        str_a =  result_file_path
        print str_a

        write_file.write(str_a + '\n')
        np.savetxt(result_file_path, X_train_s, delimiter=",", comments='', header=result_header)
        file_name = "{}_{}_y_train.csv".format(num_fold, flow_type)
        result_file_path = os.path.join(result_folder_path, file_name)
        str_a =  result_file_path
        print str_a
        write_file.write(str_a + '\n')
        np.savetxt(result_file_path, y_train, delimiter=",",  fmt="%d",  comments='',  header=y_result_header)

        file_name = "{}_{}_X_test.csv".format(num_fold, flow_type)
        result_file_path = os.path.join(result_folder_path, file_name)
        str_a =  result_file_path
        print str_a
        write_file.write(str_a + '\n')
        np.savetxt(result_file_path, X_test_s, delimiter=",", comments='',  header=result_header)
        file_name = "{}_{}_y_test.csv".format(num_fold, flow_type)
        result_file_path = os.path.join(result_folder_path, file_name)
        str_a =  result_file_path
        print str_a
        write_file.write(str_a + '\n')
        np.savetxt(result_file_path, y_test, delimiter=",", fmt="%d", comments='',  header=y_result_header)

        num_fold = num_fold + 1


    # ########################## EXIT 

    flow_type = "exit"


    X_train, X_test = df_exit[:, 2:], df_exit[:, 2:]
    y_train, y_test = df_exit[:, 0], df_exit[:, 0]

    str_a = "# ENTIRE TRAINING Dataset ----------------------------"
    print str_a
    write_file.write(str_a + '\n')
    # Print details about the distribution of the datasets
    unique, counts = np.unique( y_train, return_counts=True)
    str_a = "Exit Training Dataset Shape: {}".format(X_train.shape)
    print str_a
    write_file.write(str_a + '\n')
    str_a = "Exit Training Dataset Distribution:"
    print str_a
    write_file.write(str_a + '\n')
    str_a =  str(np.asarray((unique, counts)).T)
    print str_a
    write_file.write(str_a + '\n')

    unique, counts = np.unique( y_test, return_counts=True)
    str_a = "Exit Test Dataset Shape: {}".format(X_test.shape)
    print str_a
    write_file.write(str_a + '\n')
    str_a = "Exit Test Dataset Distribution:"
    print str_a
    write_file.write(str_a + '\n')
    str_a =  str(np.asarray((unique, counts)).T)
    print str_a
    write_file.write(str_a + '\n')

    # Standardize Data
    scaler = MinMaxScaler(feature_range=(-1,1))
    scaler.fit(X_train)
    X_train_s = scaler.transform(X_train)
    X_test_s = scaler.transform(X_test)

    # Save the scaler
    file_name = 'scaler_{}.pkl'.format(flow_type)
    file_path = os.path.join(result_folder_path, file_name)
    str_a =  file_path
    print str_a
    write_file.write(str_a + '\n')
    joblib.dump(scaler, file_path)

    # Save the data

    file_name = "{}_X_train.csv".format(flow_type)
    result_file_path = os.path.join(result_folder_path, file_name)
    str_a =  result_file_path
    print str_a

    write_file.write(str_a + '\n')
    np.savetxt(result_file_path, X_train_s, delimiter=",", comments='', header=result_header)
    file_name = "{}_y_train.csv".format(flow_type)
    result_file_path = os.path.join(result_folder_path, file_name)
    str_a =  result_file_path
    print str_a
    write_file.write(str_a + '\n')
    np.savetxt(result_file_path, y_train, delimiter=",",  fmt="%d",  comments='',  header=y_result_header)

    file_name = "{}_X_test.csv".format(flow_type)
    result_file_path = os.path.join(result_folder_path, file_name)
    str_a =  result_file_path
    print str_a
    write_file.write(str_a + '\n')
    np.savetxt(result_file_path, X_test_s, delimiter=",", comments='',  header=result_header)
    file_name = "{}_y_test.csv".format(flow_type)
    result_file_path = os.path.join(result_folder_path, file_name)
    str_a =  result_file_path
    print str_a
    write_file.write(str_a + '\n')
    np.savetxt(result_file_path, y_test, delimiter=",", fmt="%d", comments='',  header=y_result_header)




    # Exit dataset
    skf_exit = StratifiedKFold(n_splits=5, random_state=5)
    skf_exit.get_n_splits(df_exit[:, 2:],  df_exit[:, 0])
    num_fold = 0
    str_a = "# EXIT DATASETS 5-FOLD -----------------------"
    print str_a
    write_file.write(str_a + '\n')
    for train_index, test_index in skf_exit.split(df_exit[:, 2:], df_exit[:, 0]):
        X_train, X_test = df_exit[train_index, 2:], df_exit[test_index, 2:]
        y_train, y_test = df_exit[train_index, 0], df_exit[test_index, 0]

        str_a = "# 0th Fold Dataset ----------------------------"
        print str_a
        write_file.write(str_a + '\n')
        # Print details about the distribution of the datasets
        unique, counts = np.unique( y_train, return_counts=True)
        str_a = "Entry Training Dataset Shape: {}".format(X_train.shape)
        print str_a
        write_file.write(str_a + '\n')
        str_a = "Entry Training Dataset Distribution:"
        print str_a
        write_file.write(str_a + '\n')
        str_a =  str(np.asarray((unique, counts)).T)
        print str_a
        write_file.write(str_a + '\n')

        unique, counts = np.unique( y_test, return_counts=True)
        str_a = "Entry Test Dataset Shape: {}".format(X_test.shape)
        print str_a
        write_file.write(str_a + '\n')
        str_a = "Entry Test Dataset Distribution:"
        print str_a
        write_file.write(str_a + '\n')
        str_a =  str(np.asarray((unique, counts)).T)
        print str_a
        write_file.write(str_a + '\n')

        # Standardize Data
        '''
        scaler = StandardScaler()
        scaler.fit(X_train)
        X_train_s = scaler.transform(X_train)
        X_test_s = scaler.transform(X_test)
        '''
        scaler = MinMaxScaler(feature_range=(-1,1))
        scaler.fit(X_train)
        X_train_s = scaler.transform(X_train)
        X_test_s = scaler.transform(X_test)

        # Save the scaler
        file_name = 'scaler_{}_{}.pkl'.format(flow_type,num_fold)
        file_path = os.path.join(result_folder_path, file_name)
        str_a =  file_path
        print str_a
        write_file.write(str_a + '\n')
        joblib.dump(scaler, file_path)

        # Save the data

        file_name = "{}_{}_X_train.csv".format(num_fold, flow_type)
        result_file_path = os.path.join(result_folder_path, file_name)
        str_a =  result_file_path
        print str_a
        write_file.write(str_a + '\n')
        np.savetxt(result_file_path, X_train_s, delimiter=",", comments='',  header=result_header)
        file_name = "{}_{}_y_train.csv".format(num_fold, flow_type)
        result_file_path = os.path.join(result_folder_path, file_name)
        str_a =  result_file_path
        print str_a
        write_file.write(str_a + '\n')
        np.savetxt(result_file_path, y_train, delimiter=",",  fmt="%d", comments='', header=y_result_header)

        file_name = "{}_{}_X_test.csv".format(num_fold, flow_type)
        result_file_path = os.path.join(result_folder_path, file_name)
        str_a =  result_file_path
        print str_a
        write_file.write(str_a + '\n')
        np.savetxt(result_file_path, X_test_s, delimiter=",",  comments='', header=result_header)
        file_name = "{}_{}_y_test.csv".format(num_fold, flow_type)
        result_file_path = os.path.join(result_folder_path, file_name)
        str_a =  result_file_path
        print str_a
        write_file.write(str_a + '\n')
        np.savetxt(result_file_path, y_test, delimiter=",",  fmt="%d",  comments='', header=y_result_header)

        num_fold = num_fold + 1




    # End of timer
    elapsed = time.time() - time_start
    str_a = 'Total Time Elapsed: {}'.format(time.strftime("%H:%M:%S", time.gmtime(elapsed)))
    print str_a
    write_file.write(str_a + '\n')
    write_file.close()



if __name__ == '__main__':
    master_standardize_3()