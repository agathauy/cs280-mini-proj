'''
    Parses the generated csvs of hourly data for MRT passenger flow

    For blank passenger flow, -1 is substituted

    CSV output of this parser:
    year, month, day, day_of_the_week, Time, Station Name, Type, Flow Number

'''
import os
import numpy as np
#import pandas as pd



SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))


def mrt_passenger_flow_entry_exit_set_output_3():
    list_years = [2016, 2017]
    list_months = ["JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"]

    list_days = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY']
    #print list_days.index("MONDAY")
    list_stations = ['NorthAve', 'QuezonAve', 'GMA', 'Cubao', 
        'Santolan', 'Ortigas', 'ShawBlvd', 'BoniAve', 'Guadalupe',
        'Buendia', 'Ayala', 'Magallanes', 'Taft']
    list_type = ["ENTRY", "EXIT"]
    print list_stations
    print len(list_stations)

    num_stations = 13
    num_flows = 26

    entries = [
        [], []
    ]

    result_folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "MRT_passenger_flow_entry_exit_w_output_3")

    if not os.path.isdir(result_folder_path):
        os.makedirs(result_folder_path)
    # For entry
    for n in range(len(list_years)):
        if list_years[n] % 4 == 0:
            list_months_length = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        else:
            list_months_length = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

        #bins = [0, 2207.8,  4415.6,  6623.4,  8831.2, 11039]

        # Bins based on removed closed hours, and on 5 percentiles
        bins = [673.0, 2412.0]

        folder_name = "hourly_{}".format(list_years[n])
        folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "MRT_passenger_flow_entry_exit", folder_name)

        file_name = "all_0_ENTRY.csv"
        file_path = os.path.join(folder_path, file_name)
        print file_path
        dataset = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=int)

        num_entries = dataset.shape[0]

        for i in range(num_entries):
            temp = dataset[i]
            print temp
            print temp
            if temp[7] <= bins[0]:
                temp[7] = 0
            elif temp[7] >= bins[0] and temp[7] < bins[1]:
                temp[7] = 1
            else:
                temp[7] = 2

            entries[temp[6]].append(temp)


    # Save merged data
    file_name = "merged_{}_{}.csv".format(0, list_type[0])
    file_path = os.path.join(result_folder_path, file_name)
    print file_path
    temp = np.array(entries[0])
    np.savetxt(file_path, temp, delimiter=",", fmt='%d', header="year,month,day,day_of_the_week,time,station,flow_type,flow_num")



    # For exit
    for n in range(len(list_years)):
        if list_years[n] % 4 == 0:
            list_months_length = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        else:
            list_months_length = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]


        #bins = [0,  1707.4, 3414.8, 5122.2, 6829.6 , 8537]
        # Bins based on removed closed hours, and on 5 percentiles
        bins = [727.0, 2496.0]

        folder_name = "hourly_{}".format(list_years[n])
        folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "MRT_passenger_flow_entry_exit", folder_name)

        file_name = "all_1_EXIT.csv"
        file_path = os.path.join(folder_path, file_name)
        print file_path
        dataset = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=int)

        num_entries = dataset.shape[0]

        for i in range(num_entries):
            temp = dataset[i]
            print temp
            if temp[7] <= bins[0]:
                temp[7] = 0
            elif temp[7] >= bins[0] and temp[7] < bins[1]:
                temp[7] = 1
            else:
                temp[7] = 2


            entries[temp[6]].append(temp)


    # Save merged data
    file_name = "merged_{}_{}.csv".format(1, list_type[1])
    file_path = os.path.join(result_folder_path, file_name)
    print file_path
    temp = np.array(entries[1])
    np.savetxt(file_path, temp, delimiter=",", fmt='%d', header="year,month,day,day_of_the_week,time,station,flow_type,flow_num")





if __name__ == '__main__':
    mrt_passenger_flow_entry_exit_set_output_3()