from master_combine import master_combine
from master_standardize import master_standardize
from mrt_passenger_flow_entry_exit_set_output import mrt_passenger_flow_entry_exit_set_output

def pipeline_dataset_processing():
    # Set output for the mrt passenger flow entry
    mrt_passenger_flow_entry_exit_set_output()

    # Combine datasets
    master_combine()

    # Standardized datasets
    master_standardize()


if __name__ == '__main__':
    pipeline_dataset_processing()