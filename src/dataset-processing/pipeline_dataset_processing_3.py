from master_combine_3 import master_combine_3
from master_standardize_3 import master_standardize_3
from mrt_passenger_flow_entry_exit_set_output_3 import mrt_passenger_flow_entry_exit_set_output_3

def pipeline_dataset_processing_3():
    # Set output for the mrt passenger flow entry
    mrt_passenger_flow_entry_exit_set_output_3()

    # Combine datasets
    master_combine_3()

    # Standardized datasets
    master_standardize_3()


if __name__ == '__main__':
    pipeline_dataset_processing_3()