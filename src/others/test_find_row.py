import numpy as np 
import os

SCRIPT_DIR = os.path.dirname(os.path.realpath('__file__'))


test = np.array([[0, 1, 2,], [1, 2, 3], [4, 5, 6]])
print test[test[:, 1] == 3]
folder_path = os.path.join(SCRIPT_DIR, "..", "..", "data", "processed", "time_and_date", "makati")
file_path = os.path.join(folder_path, "all_makati.csv")
df_makati = np.genfromtxt(file_path, delimiter=',', skip_header=1, dtype=[('year', int), ('month', int), ('day', int), ('hour', int), ('temperature', int), ('avg_air_pressure', float)])

year = 2016
month = 0
day = 2
hour = 0
temp_makati = df_makati[(df_makati['year'] == year) & (df_makati['month'] == month) & (df_makati['day'] == day) & (df_makati['hour'] == hour)]
print temp_makati
print test[0, -2]